#!/bin/sh

set -e
cd $(git rev-parse --show-toplevel)/tests

set -x
../build/weave-convert -v -o md-to-html/actual-output.html md-to-html/input.md
#../build/weave-convert -v -o rewrite-md/actual-output.md rewrite-md/input.md

