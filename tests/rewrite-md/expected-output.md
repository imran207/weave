### template.html

This is an example template file

```
<html weave-data="input.md" weave-ns="h1">
	<body>
		<div> {{ . }} </div>
		<br>
		
		<div>Description: </div>
		<div weave-ns="h2['Description']">
			{{ text }}
		</div>
		<br>
		
		<div weave-ns="h2['Elements']">
			<div weave-ns="table[0]">
				<div> {{ 'Symbol' }} </div>
				<div> {{ 'Atomic number' }} </div>
				<div> {{ 'Atomic mass' }} </div>
			</div>
		</div>
	</body>
</html>
```

### one-off.md

This is a sample input file containing the data

```
Alkali metals
===

Description
---
Alkali metals have 1 valence electron

Elements
---

| Symbol     | Atomic number | Atomic mass |
| ---------- | ------------- | ----------- |
| Li         | 3             | 7           |
| Na         | 11            | 23          |


Halogens
===

Description
---
Halogens have 7 valence electrons

Elements
---

| Symbol     | Atomic number | Atomic mass |
| ---------- | ------------- | ----------- |
| F          | 9             | 19          |
| Cl         | 17            | 35.5        |

```


### one-off-Alkali-metals.html

Since the `weave-ns` attribute of the template HTML file has the value `h1`, the number of
H1 headers in the `one-off.md` file will determine the number of output HTML files produced. In the
example input markdown file, there are two H1 headers, one for `Alkali metals` and one for
`Halogens`. First let's observe the output for the `Alkali metals`.

```
<html>
	<body>
		<div> Alkali metals </div>
		<br>
		
		<div>Description: </div>
		<div>
			Alkali metals have 1 valence electron
		</div>
		<br>
		
		<div>
			<div>
				<div> Li </div>
				<div> 3 </div>
				<div> 7 </div>
			</div>
			<div>
				<div> Na </div>
				<div> 11 </div>
				<div> 23 </div>
			</div>
		</div>
	</body>
</html>
```

### one-off-Halogens.html

Here is the output for the `Halogens` H1 header.

```
<html>
	<body>
		<div> Halogens </div>
		<br>
		
		<div>Description: </div>
		<div>
			Halogens have 7 valence electrons
		</div>
		<br>
		
		<div>
			<div>
				<div> F </div>
				<div> 9 </div>
				<div> 19 </div>
			</div>
			<div>
				<div> Cl </div>
				<div> 17 </div>
				<div> 35.5 </div>
			</div>
		</div>
	</body>
</html>
```

Henceforth, the root element refers to the `<html>` tag in the template HTML document.

Rules about referencing markdown documents:

- The `weave-data` attribute in the root element specifies a space-separated list of markdown documents that can be used with this template HTML.

Rules about namespaces:

- The `weave-ns` attribute is used to enter a namespace, which is a markdown header style name.
- For the root element, the `weave-ns` attribute specifies the root header of the markdown document. For all other elements, it enters a sub-namespace relative to the already entered namespace by the closest parent element.
- If a namespace value is specified in the format `<header_tag> == 'value'`, the header with the matching text will be used and it will enter the namespace of this header.
- If a namespace value matches multiple headers in the markdown document, all the child elements under the HTML DOM element will be repeated for each matching header from the markdown document.

Rules about text substitution:

- To replace the inner text of an HTML document with one or more paragraphs from the markdown document, a text reference in the format `{{ text }}` can be specified.
- To replace the inner text with the current namespace, the text reference `{{ . }}` can be specified.
- The paragraphs from the currently entered namespace are used.

Rules about tables:

- The `weave-ns` attribute should be set to a value with the format `table[<table_index>]`, where `<table_index>` is the index of the table within the previously entered namespace.
- The element with this `weave-ns` attribute will be repeated once for each row in the table
- For each row, a text substitution can be specified in the format `{{ 'Column title' }}`, where `'Column title'` is the title of the table column


## Build instructions
This project requires the [meson](https://mesonbuild.com/) build system. Run the following commands to build all targets:

```
meson src build
ninja -C build
```

[weave](weave): <https://gitea.cubicsignal.com/imran/static-web>[github](github): <https://github.com>[gitlab](gitlab): <https://gitlab.com>

