Weave
===

What is it?
---
[weave] is a tool that applies a set of HTML templates to a web of markdown documents.

Motivation
---
Most, if not all, server-based web application frameworks are very dynamic. They require server
side scripting code to run and require running databases or services for even the most basic
functionality. The dynamic nature of these frameworks give it a lot of flexibility but it also
introduces fragility to the system and also makes it a bit more challenging to work with. Here is
a list of disadvantages of a dynamic web site:

 - Multiple database and service calls for every site visit, even when the output is the same
 - Strains the web server because of repeated computation for every site visit
 - Testing a web site requires the use of a real web server
 - Requires transactional database copy to backup the site contents
 - No way for end users to *easily* read information from the web site if the web application is
   down

For web sites that are frequently read from but infrequently written to, the dynamic web frameworks
provide very little benefit but still introduces all the drawbacks mentioned above. This is often
the case with blogging web sites.

Design details
---
Suppose you have a set of markdown documents in a git repo. Each markdown document is fully
readable on a git front-end website (ie. [github] or [gitlab]). The user can open a document in one
of these websites and he/she can navigate across all the pages by following the markdown links.

Normally there is a standard way a markdown document is rendered into an HTML document before it
is displayed in a web browser. Instead of using the standard way of rendering, [weave] allows the
developer to specify a template HTML document to be used for one or more markdown documents. This
allows for custom rendering of the markdown documents.

[weave] is a tool, not a framework. This allows us to use existing JavaScript frameworks without
any interoperability issues. This however requires some additional work for us:
 - The transformation step is not automated. This means that we need an external repo watcher to
   detect changes in the source markdown or HTML documents and trigger a transformation.
 - No new JavaScript code on the client. We could've apply DOM manipulation on the client with
   some JavaScript code and reduced a bulk of the work on the server. This, however, would
   potentially introduce a clash with any other JavaScript framework that is being used in the
   source HTML template.

Project status
---
This project is still in its infancy stage. It currently supports reading and writing UTF-8
encoded HTML documents. It also supports reading and writing UTF-8 encoded markdown documents
but it's quite buggy.

How to use
---
Here are the set of command line options for [weave]

 - *--output* - Specify the output directory name.
 - *--include* - Add an *include* directory path to search for markdown files.
 - *--verbose* - Enable verbose output.
 - *--rewrite* - Read and then write the specified HTML or markdown document. This is meant to
   be used to exercise the parsers

When the input is an HTML file, the tool operates in the `compiler` mode. In this mode, the input
file is read as the template HTML file. Based on the `weave-data` attribute of the `html` element,
the compiler will know the list of markdown files that is compatible with this template HTML file.
It will then render the output HTML files for each one of the markdown documents into a separate
HTML document in the output directory. If a markdown contains multiple instances of the top-level
header, the compiler will generate multiple HTML documents for that markdown document, one per
top-level header. The hyperlinks in the generated HTML documents will contain the same URLs that
was in the original markdown document (which would be a reference to another markdown document). A
`link.dat` text file will be created in the output directory that contains a mapping between the
source markdown document and the generated HTML document.

When the input is a set of directories, the tool operates in the `linker` mode. In this mode, the
hyperlink in each HTML document will be updated so that it points to the generated HTML document
instead of the associated source markdown document. The link will also contain a
`?weave-ver=SHA1` suffix that reflects the `SHA-1` of the target rendered HTML document (before
this hyperlink transformation). This will serve as a cache buster whenever a source markdown or its
template HTML file is modified.

Implementation design goal
---
Everything must be in C. It's just a personal preference of mine. I have a strong dislike for C++
or most of the "modern" languages because they are opinionated in favor of object oriented
programming practices. I don't like OOP style programming because it tries to couple how humans
think about the things they interact with (objects) and how machines think about the things they
interact with (data sets). I believe data-oriented design is a much better way to develop software.

Here is a more complete list of design requirements for this project:
 - All code must be written in C.
 - All data are stored in tables that are piped through transformations (aka. functions).
 - No use of pointers between nodes in a list or a tree. All references are based on indicies
   within the tables.

Template syntax
---
The template syntax is non-intrusive such that a template HTML document should be viewable in a
web browser. Let's first present an example and then we can discuss the rules for the syntax.

### template.html

This is an example template file

```
<html weave-data="input.md" weave-ns="h1">
	<body>
		<div> {{ . }} </div>
		<br>
		
		<div>Description: </div>
		<div weave-ns="h2['Description']">
			{{ text }}
		</div>
		<br>
		
		<div weave-ns="h2['Elements']">
			<div weave-ns="table[0]">
				<div> {{ 'Symbol' }} </div>
				<div> {{ 'Atomic number' }} </div>
				<div> {{ 'Atomic mass' }} </div>
			</div>
		</div>
	</body>
</html>
```

### one-off.md

This is a sample input file containing the data

```
Alkali metals
===

Description
---
Alkali metals have 1 valence electron

Elements
---

| Symbol     | Atomic number | Atomic mass |
| ---------- | ------------- | ----------- |
| Li         | 3             | 7           |
| Na         | 11            | 23          |


Halogens
===

Description
---
Halogens have 7 valence electrons

Elements
---

| Symbol     | Atomic number | Atomic mass |
| ---------- | ------------- | ----------- |
| F          | 9             | 19          |
| Cl         | 17            | 35.5        |

```


### one-off-Alkali-metals.html

Since the `weave-ns` attribute of the template HTML file has the value `h1`, the number of
H1 headers in the `one-off.md` file will determine the number of output HTML files produced. In the
example input markdown file, there are two H1 headers, one for `Alkali metals` and one for
`Halogens`. First let's observe the output for the `Alkali metals`.

```
<html>
	<body>
		<div> Alkali metals </div>
		<br>
		
		<div>Description: </div>
		<div>
			Alkali metals have 1 valence electron
		</div>
		<br>
		
		<div>
			<div>
				<div> Li </div>
				<div> 3 </div>
				<div> 7 </div>
			</div>
			<div>
				<div> Na </div>
				<div> 11 </div>
				<div> 23 </div>
			</div>
		</div>
	</body>
</html>
```

### one-off-Halogens.html

Here is the output for the `Halogens` H1 header.

```
<html>
	<body>
		<div> Halogens </div>
		<br>
		
		<div>Description: </div>
		<div>
			Halogens have 7 valence electrons
		</div>
		<br>
		
		<div>
			<div>
				<div> F </div>
				<div> 9 </div>
				<div> 19 </div>
			</div>
			<div>
				<div> Cl </div>
				<div> 17 </div>
				<div> 35.5 </div>
			</div>
		</div>
	</body>
</html>
```

Henceforth, the root element refers to the `<html>` tag in the template HTML document.

Rules about referencing markdown documents:

 - The `weave-data` attribute in the root element specifies a space-separated list of markdown
   documents that can be used with this template HTML.

Rules about namespaces:

 - The `weave-ns` attribute is used to enter a namespace, which is a markdown header style name.
 - For the root element, the `weave-ns` attribute specifies the root header of the markdown
   document. For all other elements, it enters a sub-namespace relative to the already entered
   namespace by the closest parent element.
 - If a namespace value is specified in the format `<header_tag> == 'value'`, the header with the
   matching text will be used and it will enter the namespace of this header.
 - If a namespace value matches multiple headers in the markdown document, all the child elements
   under the HTML DOM element will be repeated for each matching header from the markdown document.

Rules about text substitution:

 - To replace the inner text of an HTML document with one or more paragraphs from the markdown
   document, a text reference in the format `{{ text }}` can be specified.
 - To replace the inner text with the current namespace, the text reference `{{ . }}` can be
   specified.
 - The paragraphs from the currently entered namespace are used.

Rules about tables:

 - The `weave-ns` attribute should be set to a value with the format `table[<table_index>]`, where
   `<table_index>` is the index of the table within the previously entered namespace.
 - The element with this `weave-ns` attribute will be repeated once for each row in the table
 - For each row, a text substitution can be specified in the format
   `{{ 'Column title' }}`, where `'Column title'` is the title of the table column


Build instructions
---
This project requires the [meson](https://mesonbuild.com/) build system. Run the following commands to build all targets:

```
meson src build
ninja -C build
```

[weave]: <https://gitea.cubicsignal.com/imran/static-web>
[github]: <https://github.com>
[gitlab]: <https://gitlab.com>


