/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * weave_rewrite.h
 *
 * Copyright (C) 2022  Imran Haider
 */

#ifndef WEAVE_REWRITE_H
#define WEAVE_REWRITE_H

int rewrite_html(
		int cwd_fd,
		const char *restrict input_fn,
		const char *restrict output_fn,
		unsigned char verbose);

int rewrite_md(
		int cwd_fd,
		const char *restrict input_fn,
		const char *restrict output_fn,
		unsigned char verbose);

#endif

