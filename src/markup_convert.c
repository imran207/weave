/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * markup_convert.c
 *
 * Copyright (C) 2023  Imran Haider
 */

#include "markup_convert.h"
#include "html_builder.h"
#include "html_tree.h"
#include "md_tree.h"

#include <string.h>
#include <stdio.h>

struct markup_convert_t {
	struct html_builder_t builder;

	html_token_idx_t active_parent;

	size_t link_idx;
	size_t tooltip_idx;

	/* parsing error handling */
	const char *restrict exception_msg;

	/* flags */
	uint8_t exception_pending :1;
};

static size_t append_blockquote(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx);

static size_t append_codeblock(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx);

static size_t append_label(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx);

static size_t append_label_text(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx);

static html_token_idx_t create_node_token(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		const struct md_node_t *restrict node);

static html_token_idx_t create_node_token_escaped(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		const struct md_node_t *restrict node);

static size_t append_image(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx);

int markup_convert_md_html(
		struct html_tree_t *restrict dest,
		struct md_tree_t *restrict src,
		utf32_t *buffer_data,
		size_t buffer_size)
{
	html_token_idx_t table_parent = 0;
	html_token_idx_t table_row_parent = 0;
	html_token_idx_t table_cell_parent = 0;

	html_token_idx_t list_parent = 0;
	html_token_idx_t list_item_parent = 0;

	size_t node_idx;

	struct markup_convert_t state = { 0 };
	struct html_builder_t *restrict builder = &state.builder;

	html_builder_init(builder, dest, buffer_data, buffer_size);

	html_token_idx_t root_parent = html_builder_add_div(builder, 0);
	state.active_parent = root_parent;

	for (node_idx = 0; node_idx < src->node_count; ++node_idx) {
		struct md_node_t node = src->node[node_idx];
		enum md_tag_id_t tag = src->tag[node_idx];

		if (tag == MD_TAG_HEADING1) {
			state.active_parent = html_builder_add_header(builder, root_parent, 1);
		}
		else if (tag == MD_TAG_HEADING2) {
			state.active_parent = html_builder_add_header(builder, root_parent, 2);
		}
		else if (tag == MD_TAG_HEADING3) {
			state.active_parent = html_builder_add_header(builder, root_parent, 3);
		}
		else if (tag == MD_TAG_HEADING4) {
			state.active_parent = html_builder_add_header(builder, root_parent, 4);
		}
		else if (tag == MD_TAG_HEADING5) {
			state.active_parent = html_builder_add_header(builder, root_parent, 5);
		}
		else if (tag == MD_TAG_HEADING6) {
			state.active_parent = html_builder_add_header(builder, root_parent, 6);
		}
		else if (tag == MD_TAG_NORMAL_TEXT) {
			html_token_idx_t html_token = create_node_token(&state, src, &node);
			html_builder_add_text(builder, state.active_parent, html_token);
		}
		else if (tag == MD_TAG_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(&state, src, &node);
			html_builder_add_italic_text(builder, state.active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_TEXT) {
			html_token_idx_t html_token = create_node_token(&state, src, &node);
			html_builder_add_bold_text(builder, state.active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(&state, src, &node);
			html_builder_add_bold_italic_text(builder, state.active_parent, html_token);
		}
		else if (tag == MD_TAG_CODE_TEXT) {
			html_token_idx_t html_token = create_node_token(&state, src, &node);
			html_builder_add_code_text(builder, state.active_parent, html_token);

		}
		else if (tag == MD_TAG_LINEBREAK) {
			if (table_parent) {
				/* reset table mode */
				state.active_parent = table_parent;
				table_parent = 0;
				table_row_parent = 0;
				table_cell_parent = 0;
			}
			else if (list_parent) {
				/* reset list mode */
				state.active_parent = list_parent;
				list_parent = 0;
				list_item_parent = 0;
			}
			else {
				/* headers, list items, and table cells don't allow new lines. for these
				 * cases, we will switch the active parent to the root level parent
				 */
				state.active_parent = root_parent;
			}

			html_builder_add_linebreak(builder, state.active_parent);
		}
		else if (tag == MD_TAG_ORDERED_LIST) {
			list_parent = state.active_parent;
			list_item_parent = html_builder_add_ordered_list(builder, state.active_parent);
		}
		else if (tag == MD_TAG_UNORDERED_LIST) {
			list_parent = state.active_parent;
			list_item_parent = html_builder_add_unordered_list(builder, state.active_parent);
		}
		else if (tag == MD_TAG_LIST_ITEM) {
			state.active_parent = html_builder_add_list_item(builder, list_item_parent);
		}
		else if (tag == MD_TAG_BLOCKQUOTE) {
			node_idx = append_blockquote(&state, src, node_idx);
		}
		else if (tag == MD_TAG_CODE_BLOCK) {
			node_idx = append_codeblock(&state, src, node_idx);
		}
		else if (tag == MD_TAG_LABEL) {
			node_idx = append_label(&state, src, node_idx);
		}
		else if (tag == MD_TAG_IMG_ALTTEXT) {
			node_idx = append_image(&state, src, node_idx);
		}
		else if (tag == MD_TAG_HORIZONTAL_RULE) {
			html_builder_add_horizontalrule(builder, state.active_parent);
		}
		else if (tag == MD_TAG_TABLE) {
			table_parent = state.active_parent;
			table_row_parent = html_builder_add_table(builder, state.active_parent);
		}
		else if (tag == MD_TAG_TABLE_ROW) {
			table_cell_parent = html_builder_add_tablerow(builder, table_row_parent);
		}
		else if (tag == MD_TAG_TABLE_CELL) {
			state.active_parent = html_builder_add_tablecell(builder, table_cell_parent);
		}
	}

	int rc = 0;
	if (__builtin_expect(state.exception_pending == 1, 0)) {
		fprintf(stderr, "markup_convert_md_html: '%s'\n", state.exception_msg);
		rc = -1;
	}

	return rc;
}

static size_t append_blockquote(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx)
{
	struct html_builder_t *restrict builder = &state->builder;

	html_token_idx_t active_parent = html_builder_add_blockquote(builder, state->active_parent);

	/* node stack */
	md_token_idx_t node_stack[MD_TREE_MAX_STACK_SIZE];
	size_t node_stack_size = 1;

	/* add the BLOCKQUOTE node as the root element in the node stack. once the stack is emptied,
	 * we'll know that all children nodes within the BLOCKQUOTE node has been processed.
	 */
	node_stack[0] = src->node[node_idx].begin;
	++node_idx;

	while (node_idx < src->node_count) {
		struct md_node_t node = src->node[node_idx];
		enum md_tag_id_t tag = src->tag[node_idx];
		md_token_idx_t parent = src->parent[node_idx];

		/* pop from the stack until we find the current node's parent */
		while (node_stack_size > 0 && parent != node_stack[node_stack_size-1]) {
			--node_stack_size;
		}

		if (node_stack_size == 0)
			/* We're done with all the children nodes within the BLOCKQUOTE node */
			break;

		if (tag == MD_TAG_NORMAL_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_italic_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_bold_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_bold_italic_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_CODE_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_code_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_LINEBREAK) {
			html_builder_add_linebreak(builder, active_parent);
		}
		else if (tag == MD_TAG_LABEL) {
			node_idx = append_label(state, src, node_idx);
		}
		else if (tag == MD_TAG_IMG_ALTTEXT) {
			node_idx = append_image(state, src, node_idx);
		}
		else {
			/* Something else that cannot be part of a blockquote */
			break;
		}

		node_stack[node_stack_size] = node.begin;
		if (node_stack_size < MD_TREE_MAX_STACK_SIZE) {
			++node_stack_size;
		}
		else {
			state->exception_pending = 1;
			state->exception_msg =
				"Not enough space for node stack. Increase MD_TREE_MAX_STACK_SIZE";
			break;
		}

		++node_idx;
	}

	/* Return the index of the last node processed, not the index of the next node to be processed.
	 * This is expected by the caller
	 */
	return node_idx - 1;
}

static size_t append_codeblock(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx)
{
	struct html_builder_t *restrict builder = &state->builder;

	html_token_idx_t active_parent = html_builder_add_codeblock(builder, state->active_parent);

	/* node stack */
	md_token_idx_t node_stack[MD_TREE_MAX_STACK_SIZE];
	size_t node_stack_size = 1;

	/* add the CODE_BLOCK node as the root element in the node stack. once the stack is emptied,
	 * we'll know that all children nodes within the CODE_BLOCK node has been processed.
	 */
	node_stack[0] = src->node[node_idx].begin;
	++node_idx;

	while (node_idx < src->node_count) {
		struct md_node_t node = src->node[node_idx];
		enum md_tag_id_t tag = src->tag[node_idx];
		md_token_idx_t parent = src->parent[node_idx];

		/* pop from the stack until we find the current node's parent */
		while (node_stack_size > 0 && parent != node_stack[node_stack_size-1]) {
			--node_stack_size;
		}

		if (node_stack_size == 0)
			/* We're done with all the children nodes within the CODE_BLOCK node */
			break;

		if (tag == MD_TAG_NORMAL_TEXT) {
			html_token_idx_t html_token = create_node_token_escaped(state, src, &node);
			html_builder_add_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_italic_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_bold_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_bold_italic_text(builder, active_parent, html_token);
		}
		else if (tag == MD_TAG_LINEBREAK) {
			html_builder_add_linebreak_escaped(builder, active_parent);
		}
		else if (tag == MD_TAG_LABEL) {
			node_idx = append_label(state, src, node_idx);
		}
		else if (tag == MD_TAG_IMG_ALTTEXT) {
			node_idx = append_image(state, src, node_idx);
		}
		else {
			/* Something else that cannot be part of a code block */
			break;
		}

		node_stack[node_stack_size] = node.begin;
		if (node_stack_size < MD_TREE_MAX_STACK_SIZE) {
			++node_stack_size;
		}
		else {
			state->exception_pending = 1;
			state->exception_msg =
				"Not enough space for node stack. Increase MD_TREE_MAX_STACK_SIZE";
			break;
		}

		++node_idx;
	}

	/* Return the index of the last node processed, not the index of the next node to be processed.
	 * This is expected by the caller
	 */
	return node_idx - 1;
}

static size_t append_label(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx)
{
	struct html_builder_t *restrict builder = &state->builder;
	md_token_off_t node_begin = src->node[node_idx].begin;

	if (state->link_idx < src->link_count) {
		if (src->link[state->link_idx].begin != node_begin) {
			/* long-form link with text */

			if (src->link_parent[state->link_idx] == src->node[node_idx].begin) {
				html_token_idx_t html_link_token;
				html_token_idx_t url_token =
					create_node_token(state, src, src->link + state->link_idx);

				++state->link_idx;

				if (
					state->tooltip_idx < src->tooltip_count &&
					src->tooltip_parent[state->tooltip_idx] == node_begin) {

					html_link_token = html_builder_add_link_with_tooltip(
							builder,
							state->active_parent,
							url_token,
							create_node_token(state, src, src->tooltip + state->tooltip_idx));

					++state->tooltip_idx;
				}
				else {
					html_link_token = html_builder_add_link(
							builder,
							state->active_parent,
							url_token);
				}

				/* dive into the `a` tag before adding the inner html nodes */
				html_token_idx_t original_parent = state->active_parent;
				state->active_parent = html_link_token;

				node_idx = append_label_text(state, src, node_idx);

				/* reset parent to the one original, which would be the parent of the `a` tag */
				state->active_parent = original_parent;
			}
			else {
				state->exception_pending = 1;
				state->exception_msg = "Mismatch between label and link";
			}
		}
		else {
			/* short-form link with no text */

			const struct md_node_t *restrict link_node = src->link + state->link_idx;
			++state->link_idx;

			html_token_idx_t url_token = create_node_token(state, src, link_node);
			html_token_idx_t html_link_token = html_builder_add_link(
					builder,
					state->active_parent,
					url_token);

			html_builder_add_text(builder, html_link_token, url_token);

			/* For a quick link, the node table and the link table will contain
			 * references to the same tokens. Since we only need one reference, we
			 * used the references from the link table and we'll be skipping over the
			 * references in the node table */
			while (
					node_idx < src->node_count &&
					src->node[node_idx].begin < link_node->end)
				++node_idx;
		}
	}

	return node_idx;
}

static size_t append_label_text(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx)
{
	struct html_builder_t *restrict builder = &state->builder;

	/* node stack */
	md_token_idx_t node_stack[MD_TREE_MAX_STACK_SIZE];
	size_t node_stack_size = 1;

	/* add the LABEL node as the root element in the node stack. once the stack is emptied, we'll
	 * know that all children nodes within the LABEL node has been processed.
	 */
	node_stack[0] = src->node[node_idx].begin;
	++node_idx;

	while (node_idx < src->node_count) {
		struct md_node_t node = src->node[node_idx];
		enum md_tag_id_t tag = src->tag[node_idx];
		md_token_idx_t parent = src->parent[node_idx];

		/* pop from the stack until we find the current node's parent */
		while (node_stack_size > 0 && parent != node_stack[node_stack_size-1]) {
			--node_stack_size;
		}

		if (node_stack_size == 0)
			/* We're done with all the children nodes within the LABEL node */
			break;

		if (tag == MD_TAG_NORMAL_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_text(builder, state->active_parent, html_token);
		}
		else if (tag == MD_TAG_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_italic_text(builder, state->active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_bold_text(builder, state->active_parent, html_token);
		}
		else if (tag == MD_TAG_BOLD_ITALIC_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_bold_italic_text(builder, state->active_parent, html_token);
		}
		else if (tag == MD_TAG_CODE_TEXT) {
			html_token_idx_t html_token = create_node_token(state, src, &node);
			html_builder_add_code_text(builder, state->active_parent, html_token);
		}
		else if (tag == MD_TAG_IMG_ALTTEXT) {
			node_idx = append_image(state, src, node_idx);
		}
		else {
			/* Something else that cannot be part of a label */
			break;
		}

		node_stack[node_stack_size] = node.begin;
		if (node_stack_size < MD_TREE_MAX_STACK_SIZE) {
			++node_stack_size;
		}
		else {
			state->exception_pending = 1;
			state->exception_msg =
				"Not enough space for node stack. Increase MD_TREE_MAX_STACK_SIZE";
			break;
		}

		++node_idx;
	}

	/* Return the index of the last node processed, not the index of the next node to be processed.
	 * This is expected by the caller
	 */
	return node_idx - 1;
}

static html_token_idx_t create_node_token(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		const struct md_node_t *restrict node)
{
	struct html_builder_t *restrict builder = &state->builder;
	const struct md_tokens_t *restrict tokens = &src->tokens;

	html_token_idx_t token = html_builder_allocate_token(builder);
	md_token_idx_t i;

	for (i = node->begin; i < node->end; ++i) {
		html_builder_append_last_token(
				builder,
				tokens->base + tokens->begin[i],
				tokens->size[i]);
	}

	return token;
}

static html_token_idx_t create_node_token_escaped(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		const struct md_node_t *restrict node)
{
	struct html_builder_t *restrict builder = &state->builder;
	const struct md_tokens_t *restrict tokens = &src->tokens;

	html_token_idx_t token = html_builder_allocate_token(builder);
	md_token_idx_t i;

	for (i = node->begin; i < node->end; ++i) {
		// FIXME: escape the text

		html_builder_append_last_token(
				builder,
				tokens->base + tokens->begin[i],
				tokens->size[i]);
	}

	return token;
}


static size_t append_image(
		struct markup_convert_t *restrict state,
		const struct md_tree_t *restrict src,
		size_t node_idx)
{
	struct html_builder_t *restrict builder = &state->builder;
	const struct md_node_t *restrict node = src->node + node_idx;

	if (state->link_idx < src->link_count) {
		if (src->link_parent[state->link_idx] == node->begin) {

			html_token_idx_t url_token =
				create_node_token(state, src, src->link + state->link_idx);

			++state->link_idx;

			if (
				state->tooltip_idx < src->tooltip_count &&
				src->tooltip_parent[state->tooltip_idx] == node->begin) {

				html_builder_add_image_with_tooltip(
						builder,
						state->active_parent,
						url_token,
						create_node_token(state, src, src->tooltip + state->tooltip_idx),
						create_node_token(state, src, node));

				++state->tooltip_idx;
			}
			else {
				html_builder_add_image(
						builder,
						state->active_parent,
						url_token,
						create_node_token(state, src, node));
			}
		}
		else {
			state->exception_pending = 1;
			state->exception_msg = "Mismatch between label and link";
		}
	}

	return node_idx;
}

