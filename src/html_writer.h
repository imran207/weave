/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * html_writer.h
 *
 * Copyright (C) 2023  Imran Haider
 */

#ifndef HTML_WRITER_H
#define HTML_WRITER_H

#include "html_tree.h"

int html_write(
		utf32_t *restrict *restrict out_data, size_t *restrict out_size,
		const struct html_tree_t *restrict tree);

#endif

