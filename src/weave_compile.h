/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * weave_compile.h
 *
 * Copyright (C) 2022  Imran Haider
 */

#ifndef WEAVE_COMPILE_H
#define WEAVE_COMPILE_H

int compile_main(
		int cwd_fd,
		const char *restrict input,
		const char *restrict output,
		const char *restrict *restrict include_dirs,
		unsigned short n_include_dirs,
		unsigned char verbose);


#endif

