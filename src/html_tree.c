/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * html_tree.c
 *
 * Copyright (C) 2023  Imran Haider
 */

#include "html_tree.h"

int html_tree_append(
		struct html_tree_t *restrict dest,
		struct html_tree_t *restrict src)
{
#if 0
	struct html_tokens_t tokens;

	/* the node and a reference to its parent node */
	html_token_idx_t node_parent[HTML_TREE_MAX_NODES];
	html_token_idx_t node_tag_name[HTML_TREE_MAX_NODES];

	/* the attribute name:value and a reference to the owner node */
	html_token_idx_t attrib_parent[HTML_TREE_MAX_ATTRIBUTES];
	struct html_token_range_t attrib_name[HTML_TREE_MAX_ATTRIBUTES];
	html_token_idx_t attrib_value[HTML_TREE_MAX_ATTRIBUTES];

	/* the text and a reference to the owner node and the previous sibling node */
	html_token_idx_t text_parent[HTML_TREE_MAX_TEXTS];
	html_token_idx_t text_prev[HTML_TREE_MAX_TEXTS];
	struct html_token_range_t text[HTML_TREE_MAX_TEXTS];

	size_t attrib_count;
	size_t node_count;
	size_t text_count;

	struct html_token_range_t node_tokens[HTML_TREE_MAX_NODES];
#endif


	return 0;
}


