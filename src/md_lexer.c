// SPDX-License-Identifier: GPL-2.0-only
/*
 * md_lexer.c
 *
 * Copyright (C) 2021  Imran Haider
 */

#include "md_tree.h"
#include "md_lexer.h"

#include <stdio.h>
#include <string.h>

enum {
	CHAR_INFO_IDENTIFIER = 1 << 0,
	CHAR_INFO_NUMBER     = 1 << 1,
	CHAR_INFO_WHITESPACE = 1 << 2,
	CHAR_INFO_NEWLINE    = 1 << 3
};

struct md_lexer_t {
	const utf32_t *restrict current;
	const utf32_t *restrict end;

	/* tokens */
	struct md_tokens_t *restrict tokens;

	/* lookup table for common characters */
	uint8_t char_info[128];

	/* parsing error handling */
	const char *restrict exception_msg;
	const utf32_t *restrict exception_location;

	/* flags */
	unsigned int exception_pending :1;
};

static int read_token(struct md_lexer_t *restrict lexer);
static int read_one_token_class(struct md_lexer_t *restrict lexer, int flag, enum md_token_id_t id);
static int read_token_class(struct md_lexer_t *restrict lexer, int flag, enum md_token_id_t id);
static int read_token_char(struct md_lexer_t *restrict lexer, char ch, enum md_token_id_t token_id);
static int read_token_two_chars(
	struct md_lexer_t *restrict lexer, const char *str, enum md_token_id_t token_id);
static int read_token_nonascii(struct md_lexer_t *restrict lexer);
static void add_token(
		struct md_lexer_t *restrict lexer, enum md_token_id_t id,
		const utf32_t *begin, const utf32_t *end);

static inline int char_type_check(struct md_lexer_t *restrict lexer, utf32_t ch, int flags)
{
	// TODO: make this bound check branchless
	if (ch > 127)
		return 0;

	return lexer->char_info[ch] & flags;
}

int md_lex(const utf32_t *restrict in_data, size_t in_size, struct md_tokens_t *restrict tokens)
{
	struct md_lexer_t lexer = {0};
	int processed, rc = 0, i;

	lexer.tokens = tokens;
	tokens->base = in_data;

	/* prepare character info table */
	memset(&lexer.char_info, 0, sizeof(lexer.char_info));
	lexer.char_info['\n'] = CHAR_INFO_NEWLINE;
	lexer.char_info['\r'] = CHAR_INFO_NEWLINE;
	lexer.char_info[' '] = CHAR_INFO_WHITESPACE;
	lexer.char_info['\t'] = CHAR_INFO_WHITESPACE;

	for (i='A'; i<='Z'; ++i) {
		lexer.char_info[i] = CHAR_INFO_IDENTIFIER;
	}

	for (i='a'; i<='z'; ++i) {
		lexer.char_info[i] = CHAR_INFO_IDENTIFIER;
	}

	for (i='0'; i<='9'; ++i) {
		lexer.char_info[i] = CHAR_INFO_NUMBER;
	}

	lexer.current = in_data;
	lexer.end = in_data + in_size;

	/* process all tokens */
	while (lexer.current < lexer.end) {
		processed = read_token(&lexer);

		if (processed) {
			if (__builtin_expect(lexer.exception_pending, 0))
				break;
		}
		else {
			lexer.exception_pending = 1;
			lexer.exception_msg = "Unrecognized token";
			lexer.exception_location = lexer.current;
			break;
		}
	}

	if (lexer.exception_pending == 0) {
		add_token(&lexer, MD_TOKEN_EOF, lexer.current, lexer.current);
	}
	else {
		const utf32_t *p;
		int column_num = 0;
		int line_num = 0;

		for (p = in_data; p < lexer.exception_location; ++p) {
			if (*p != '\n') {
				++column_num;
			}
			else {
				++line_num;
				column_num = 0;
			}
		}

		fprintf(stderr, "md_lex: '%s' on line %d, column %d\n",
				lexer.exception_msg, line_num+1, column_num+1);
		rc = -1;
	}

	return rc;
}

static int read_token(struct md_lexer_t *restrict lexer)
{
	int text_flags = CHAR_INFO_IDENTIFIER;
	int number_flags = CHAR_INFO_NUMBER;

	return
		   read_token_char(lexer, '#', MD_TOKEN_HASH)
		|| read_token_char(lexer, '*', MD_TOKEN_ASTERISK)
		|| read_token_char(lexer, '[', MD_TOKEN_OPENBRACKET)
		|| read_token_char(lexer, ']', MD_TOKEN_CLOSEBRACKET)
		|| read_token_char(lexer, '(', MD_TOKEN_OPENPAREN)
		|| read_token_char(lexer, ')', MD_TOKEN_CLOSEPAREN)
		|| read_token_char(lexer, '{', MD_TOKEN_OPENBRACE)
		|| read_token_char(lexer, '}', MD_TOKEN_CLOSEBRACE)
		|| read_token_char(lexer, '-', MD_TOKEN_HYPHEN)
		|| read_token_char(lexer, '+', MD_TOKEN_PLUS)
		|| read_token_char(lexer, '_', MD_TOKEN_UNDERSCORE)
		|| read_token_char(lexer, ' ', MD_TOKEN_SPACE)
		|| read_token_char(lexer, '\t', MD_TOKEN_TAB)

		/* for CRLF line endings */
		|| read_token_two_chars(lexer, "\r\n", MD_TOKEN_NEWLINE)

		/* LF or CR line endings */
		|| read_one_token_class(lexer, CHAR_INFO_NEWLINE, MD_TOKEN_NEWLINE)

		|| read_token_char(lexer, '<', MD_TOKEN_LESSTHAN)
		|| read_token_char(lexer, '>', MD_TOKEN_GREATERTHAN)
		|| read_token_char(lexer, '=', MD_TOKEN_EQUAL)
		|| read_token_char(lexer, '~', MD_TOKEN_TILDE)
		|| read_token_char(lexer, '!', MD_TOKEN_EXCLAMATIONMARK)
		|| read_token_char(lexer, '`', MD_TOKEN_BACKTICK)
		|| read_token_char(lexer, '|', MD_TOKEN_VERTICALPIPE)
		|| read_token_char(lexer, '.', MD_TOKEN_PERIOD)
		|| read_token_char(lexer, '\'', MD_TOKEN_SINGLEQUOTE)
		|| read_token_char(lexer, '"', MD_TOKEN_DOUBLEQUOTE)
		|| read_token_char(lexer, '\\', MD_TOKEN_BACKSLASH)
		|| read_token_char(lexer, '/', MD_TOKEN_FORWARDSLASH)
		|| read_token_char(lexer, ':', MD_TOKEN_COLON)
		|| read_token_char(lexer, ',', MD_TOKEN_COMMA)
		|| read_token_char(lexer, '?', MD_TOKEN_QUESTIONMARK)
		|| read_token_char(lexer, '@', MD_TOKEN_AT)
		|| read_token_char(lexer, '$', MD_TOKEN_DOLLAR)
		|| read_token_class(lexer, text_flags, MD_TOKEN_TEXT)
		|| read_token_class(lexer, number_flags, MD_TOKEN_NUMBER)
		|| read_token_nonascii(lexer);
}

static int read_one_token_class(struct md_lexer_t *restrict lexer, int flag, enum md_token_id_t id)
{
	const utf32_t *restrict p = lexer->current;
	const utf32_t *restrict end = lexer->end;

	if (p != end && char_type_check(lexer, *p, flag))
		++p;

	if (p - lexer->current) {
		add_token(lexer, id, lexer->current, p);
		lexer->current = p;
		return 1;
	}

	return 0;
}

static int read_token_class(struct md_lexer_t *restrict lexer, int flag, enum md_token_id_t id)
{
	const utf32_t *restrict p = lexer->current;
	const utf32_t *restrict end = lexer->end;

	while (p != end && char_type_check(lexer, *p, flag))
		++p;

	if (p - lexer->current) {
		add_token(lexer, id, lexer->current, p);
		lexer->current = p;
		return 1;
	}

	return 0;
}

static int read_token_char(struct md_lexer_t *restrict lexer, char ch, enum md_token_id_t token_id)
{
	const utf32_t *restrict p = lexer->current;

	if (*lexer->current == ch) {
		++p;
		add_token(lexer, token_id, lexer->current, p);
		lexer->current = p;
		return 1;
	}

	return 0;
}

int read_token_two_chars(
	struct md_lexer_t *restrict lexer, const char *chs, enum md_token_id_t token_id)
{
	const utf32_t *restrict p = lexer->current;

	if (*p == chs[0]) {
		++p;

		if (*p == chs[1]) {
			++p;
			add_token(lexer, token_id, lexer->current, p);
			lexer->current = p;
			return 1;
		}
	}

	return 0;
}

static int read_token_nonascii(struct md_lexer_t *restrict lexer)
{
	const utf32_t *restrict p = lexer->current;
	const utf32_t *restrict end = lexer->end;

	while (p != end && *p > 127)
		++p;

	if (p - lexer->current) {
		add_token(lexer, MD_TOKEN_NONASCII, lexer->current, p);
		lexer->current = p;
		return 1;
	}

	return 0;
}

static void add_token(
		struct md_lexer_t *restrict lexer, enum md_token_id_t id,
		const utf32_t *begin, const utf32_t *end)
{
	struct md_tokens_t *restrict tokens = lexer->tokens;
	size_t i = tokens->count;

	if (__builtin_expect(i >= MD_TREE_MAX_TOKENS, 0)) {
		lexer->exception_pending = 1;
		lexer->exception_msg = "Not enough space for tokens";
		lexer->exception_location = begin;
		return;
	}

	size_t offset = begin - tokens->base;
	if (__builtin_expect(offset > MD_TREE_MAX_FILE_SIZE, 0)) {
		lexer->exception_pending = 1;
		lexer->exception_msg = "File content is too large. Increase MD_TREE_MAX_FILE_SIZE";
		lexer->exception_location = begin;
		return;
	}

	size_t size = end - begin;
	if (__builtin_expect(size > MD_TREE_MAX_TOKEN_SIZE, 0)) {
		lexer->exception_pending = 1;
		lexer->exception_msg = "Token size is too large. Increase MD_TREE_MAX_TOKEN_SIZE";
		lexer->exception_location = begin;
		return;
	}
	
	tokens->begin[i] = offset;
	tokens->size[i] = size;
	tokens->id[i] = id;
	tokens->count = i+1;
}


