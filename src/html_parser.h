/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * html_parser.h
 *
 * Copyright (C) 2021  Imran Haider
 */

#ifndef HTML_PARSER_H
#define HTML_PARSER_H

#include "html_tree.h"

int html_trace_all_tokens(
		utf32_t *restrict in_data,
		size_t in_size,
		struct html_tree_t *restrict tree);

void html_dump_parse_table(
		const struct html_tree_t *restrict tree,
		const struct html_token_range_t *restrict node_tokens);

int html_parse(
		utf32_t *restrict in_data,
		size_t in_size,
		struct html_tree_t *restrict tree,
		unsigned char debug_mode);

#endif

