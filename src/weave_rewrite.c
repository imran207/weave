/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * weave_rewrite.c
 *
 * Copyright (C) 2022  Imran Haider
 */

#include "weave_rewrite.h"
#include "unicode.h"
#include "md_parser.h"
#include "md_writer.h"
#include "html_parser.h"
#include "html_writer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

static int write_test_data(int cwd_fd, const char *filename, const int *data, size_t size);

int rewrite_md(
		int cwd_fd,
		const char *restrict input_fn,
		const char *restrict output_fn,
		unsigned char verbose)
{
	utf32_t *input_data;
	size_t input_size = 0;

	/* read input file as utf-8 and store the data in 'input_data' as utf-32 */
	int rc = unicode_read_utf8_file(cwd_fd, input_fn, &input_data, &input_size);
	if (__builtin_expect(rc != 0, 0))
		return -1;

	struct md_tree_t *tree;
	if ((tree = malloc(sizeof(struct md_tree_t))) == 0) {
		fprintf(stderr, "not enough memory\n");
		return -1;
	}

	if (verbose) {
		printf("\ntoken table:\n");
		md_trace_token_table(input_data, input_size);
	}

	/* parse the input data into `tree` */
	memset(tree, 0, sizeof(struct md_tree_t));
	md_parse(input_data, input_size, tree);

	if (verbose) {
		printf("\nnode table:\n");
		md_trace_node_table(tree);

		printf("\nlink table:\n");
		md_trace_link_table(tree);
	}

	/* dump the `tree` into a new output string */
	utf32_t *output_data;
	size_t output_size = 0;
	md_write(&output_data, &output_size, tree);

	/* FIXME: for debugging only */
	write_test_data(cwd_fd, output_fn, output_data, output_size);

	free(output_data);
	free(tree);

	return 0;
}

static int write_test_data(int cwd_fd, const char *filename, const int *data, size_t size)
{
	int out_fd = openat(cwd_fd, ".", 0);
	int rc = unicode_write_utf8_file(out_fd, filename, data, size);
	close(out_fd);

	return rc;
}

int rewrite_html(
		int cwd_fd,
		const char *restrict input_fn,
		const char *restrict output_fn,
		unsigned char verbose)
{
	utf32_t *input_data;
	size_t input_size = 0;

	/* read input file as utf-8 and store the data in 'input_data' as utf-32 */
	int rc = unicode_read_utf8_file(cwd_fd, input_fn, &input_data, &input_size);
	if (__builtin_expect(rc != 0, 0))
		return -1;

	struct html_tree_t *tree;
	if ((tree = malloc(sizeof(struct html_tree_t))) == 0) {
		fprintf(stderr, "not enough memory\n");
		return -1;
	}

	if (verbose) {
		printf("\ntoken table:\n");
		html_trace_all_tokens(input_data, input_size, tree);
		printf("\n");
	}

	/* parse the input data into `tree` */
	memset(tree, 0, sizeof(struct html_tree_t));
	html_parse(input_data, input_size, tree, verbose);

	/* dump the `tree` into a new output string */
	utf32_t *output_data;
	size_t output_size = 0;
	html_write(&output_data, &output_size, tree);

	/* FIXME: for debugging only */
	write_test_data(cwd_fd, output_fn, output_data, output_size);

	free(output_data);
	free(tree);

	return 0;

}

