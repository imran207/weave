/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * md_writer.h
 *
 * Copyright (C) 2023  Imran Haider
 */

#ifndef MD_WRITER_H
#define MD_WRITER_H

#include "md_tree.h"
#include "unicode.h"

int md_write(
		utf32_t *restrict *restrict out_data, size_t *restrict out_size,
		const struct md_tree_t *restrict tree);

#endif

