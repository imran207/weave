/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * markup_convert.h
 *
 * Copyright (C) 2023  Imran Haider
 */

#ifndef MARKUP_CONVERT_H
#define MARKUP_CONVERT_H

#include "html_tree.h"
#include "md_tree.h"

int markup_convert_md_html(
		struct html_tree_t *restrict dest,
		struct md_tree_t *restrict src,
		utf32_t *buffer_data,
		size_t buffer_size);

#endif

