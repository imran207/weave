// SPDX-License-Identifier: GPL-2.0-only
/*
 * weave.c
 *
 * Copyright (C) 2021  Imran Haider
 */

#include "unicode.h"
#include "weave_rewrite.h"
#include "weave_compile.h"
#include "weave_link.h"

#include <getopt.h>
#include <ctype.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_INPUTS 256
#define MAX_INCLUDE_DIRS 256

static int is_directory_file(const char *file);
static int open_cwd(int *restrict fd);

int main(int argc, char **argv)
{
	const char *output = 0;
	const char *inputs[MAX_INPUTS];
	const char *include_dirs[MAX_INCLUDE_DIRS];
	unsigned short n_inputs = 0;
	unsigned short n_include_dirs = 0;
	unsigned char is_verbose = 0;
	unsigned char is_help_mode = 0;

	int i, c, cwd_fd, rc = 0;

	/* get command line options */
	static struct option long_options[] = {
		{ "output",  required_argument, NULL, 'o' },	/* output directory */
		{ "include", required_argument, NULL, 'I' },	/* include path for .md files */
		{ "verbose", no_argument,       NULL, 'v' },	/* enable verbosity */
		{ "help",    no_argument,       NULL, 'h' },	/* help */
		{ NULL,      0,                 NULL, 0   }
	};

	int option_index = 0;
	while ((c = getopt_long(argc, argv, "vho:I:", long_options, &option_index)) != -1) {
		switch (c) {
		case 'o':
			output = optarg;
			break;
		case 'I':
			if (n_include_dirs < MAX_INCLUDE_DIRS-1) {
				include_dirs[n_include_dirs] = optarg;
				++n_include_dirs;
			}
			else {
				fputs("Too many include dirs.\n", stderr);
				return -1;
			}
			break;
		case 'v':
			is_verbose = 1;
			break;
		case 'h':
			is_help_mode = 1;
			break;
		case '?':
			if (optopt == 'o')
				fputs("Option -o requires an argument.\n", stderr);

			else if (optopt == 'I')
				fputs("Option -I requires an argument.\n", stderr);

			else if (isprint(optopt))
				fprintf(stderr, "Unknown option '-%c'.\n", optopt);

			return -1;
		default:
			return -1;
		}
	}

	/* print the help information if --help is specified */
	if (is_help_mode) {
		fputs(
			"Usage: weave [options] file...\n"
			"Options:\n"
			"  -o        The output directory to be created\n"
			"  -v        Enable verbose mode\n"
			"  -h        Show this help page\n"
			"  -r        Rewrite mode. This reads the input .md or .html file and writes it\n"
			"            out again. This exercises the parser code\n"
			"  -i        Add an include directory for where to search for markdown files\n"
			"\n", stdout);

		return 0;
	}

	/* make sure the correct number of input and output files were specified */
	if (__builtin_expect(argc - optind < 1, 0)) {
		fputs("no input file\n", stderr);
		return -1;
	}

	if (__builtin_expect(argc - optind > MAX_INPUTS, 0)) {
		fputs("too many input files\n", stderr);
		return -1;
	}

	if (__builtin_expect(output == 0, 0)) {
		fputs("output file not specified\n", stderr);
		return -1;
	}

	/* collect the input filenames into `input` */
	for (i=optind; i < argc; ++i) {
		inputs[n_inputs] = argv[i];
	}

	n_inputs = argc - optind;

	/* open the current directory */
	rc = open_cwd(&cwd_fd);
	if (__builtin_expect(rc != 0, 0))
		goto exit1;

	/* compile mode requires the list of inputs to be a single file and it must be a regular
	 * file, not a directory
	 */
	if (n_inputs == 1 && !is_directory_file(inputs[0])) {
		rc = compile_main(cwd_fd, inputs[0], output, include_dirs, n_include_dirs, is_verbose);
		goto exit2;
	}

	/* link mode requires all the inputs to be directory files */
	for (i=0; i < n_inputs; ++i) {
		if (!is_directory_file(inputs[i])) {
			fputs("non-directory input specified to the linker\n", stderr);
			rc = -1;
			goto exit2;
		}
	}

	rc = link_main(cwd_fd, inputs, output, n_inputs, is_verbose);

exit2:
	close(cwd_fd);
exit1:
	return rc;
}

static int open_cwd(int *restrict fd)
{
	int rc;
	*fd = open(".", 0);

	if (__builtin_expect(*fd == -1, 0)) {
		rc = errno;
		fprintf(stderr, "cannot open current working directory. error %d\n", rc);
		return rc;
	}

	return 0;
}

static int is_directory_file(const char *file)
{
	struct stat st = {0};

	if (stat(file, &st) == 0) {
		return S_ISDIR(st.st_mode);
	}

	return 0;
}


