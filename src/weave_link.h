/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * weave_link.h
 *
 * Copyright (C) 2022  Imran Haider
 */

#ifndef WEAVE_LINK_H
#define WEAVE_LINK_H

int link_main(
		int cwd_fd,
		const char *restrict *restrict input,
		const char *restrict output,
		unsigned char n_inputs,
		unsigned char verbose);

#endif


