// SPDX-License-Identifier: GPL-2.0-only
/*
 * html_parser.c
 *
 * Copyright (C) 2021  Imran Haider
 */

#include "html_parser.h"
#include "html_lexer.h"
#include <stdio.h>
#include <assert.h>

#include <string.h>
#include <stdlib.h>

#define EXPECT_TOKEN_1_1(tree,p,token) \
	do { \
		if (__builtin_expect(tree->tokens.id[p] != token, 0)) \
			return 0; \
		++p; \
	} while(0)

#define EXPECT_TOKEN_4_1(tree,p,token1,token2,token3,token4) \
	do { \
		if (__builtin_expect( \
				tree->tokens.id[p] != token1 && \
				tree->tokens.id[p] != token2 && \
				tree->tokens.id[p] != token3 && \
				tree->tokens.id[p] != token4, 0)) \
			return 0; \
		++p; \
	} while(0)

#define EXPECT_TOKEN_1_0N(tree,p,token) \
	do { \
		while (tree->tokens.id[p] == token) { \
			++p; \
		} \
	} while(0)


struct html_parser_t {
	html_token_idx_t current;

	/* parse tree */
	struct html_tree_t *restrict tree;

	/* node stack */
	html_token_idx_t node_stack[HTML_TREE_MAX_STACK_SIZE];
	size_t node_stack_size;

	/* parsing error handling */
	const char *restrict exception_msg;
	html_token_idx_t exception_location;

	/* debug-only: mapping between tree node to lexical token ranges */
	struct html_token_range_t node_tokens[HTML_TREE_MAX_NODES];

	/* flags */
	unsigned int exception_pending  :1;
	unsigned int collect_debug_info :1;
};

/* Lexical token analyzers */
static int read_node(struct html_parser_t *restrict parser);
static int read_node_open_tag_attributes(
		struct html_parser_t *restrict parser, html_token_idx_t node, html_token_idx_t p);
static int read_node_open_tag(struct html_parser_t *restrict parser);
static int read_node_close_tag(struct html_parser_t *restrict parser);
static int read_node_open_foreign_tag(struct html_parser_t *restrict parser);
static int read_node_variable(struct html_parser_t *restrict parser);
static int read_node_text(struct html_parser_t *restrict parser);
static int read_node_whitespace(struct html_parser_t *restrict parser);
static int read_node_comment(struct html_parser_t *restrict parser);
static int read_node_script(struct html_parser_t *restrict parser);
static int read_node_style(struct html_parser_t *restrict parser);

/* Parse tree operations */
static void push_node(struct html_parser_t *restrict parser, html_token_idx_t tag_name);
static void push_text(
		struct html_parser_t *restrict parser, html_token_idx_t begin, html_token_idx_t end);
static void pop_node(struct html_parser_t *restrict parser, html_token_idx_t tag_name);

/* Debugging */
static void trace_token(const struct html_tokens_t *restrict tokens, html_token_idx_t idx);

static_assert(
		1ull << sizeof(enum html_token_id_t) * 8 > HTML_TOKEN_END, "html_token_id_t is too small");
static_assert(
		1ull << sizeof(html_token_idx_t) * 8 > HTML_TREE_MAX_TOKENS,
		"html_token_idx_t is too small");
static_assert(
		1ull << sizeof(html_token_size_t) * 8 > HTML_TREE_MAX_TOKEN_SIZE,
		"html_token_size_t is too small");
static_assert(
		1ull << sizeof(html_token_off_t) * 8 > HTML_TREE_MAX_FILE_SIZE,
		"html_token_off_t is too small");

int html_trace_all_tokens(
		utf32_t *restrict in_data,
		size_t in_size,
		struct html_tree_t *restrict tree)
{
	struct html_parser_t parser = { 0 };

	int rc = html_lex(in_data, in_size, &tree->tokens);
	if (__builtin_expect(rc != 0, 0)) {
		return rc;
	}

	parser.tree = tree;

	/* setting the initial stack size to 1. index 0 is reserved so that we needn't check if
	 * node_stack_size-1 is negative in push_node()
	 */
	parser.node_stack_size = 1;

	/* process all tokens */
	while (parser.current < tree->tokens.count) {
		struct html_tokens_t *restrict tokens = &parser.tree->tokens;
		printf("\t");
		trace_token(tokens, parser.current);
		++parser.current;
	}

	return 0;
}

int html_parse(
		utf32_t *restrict in_data,
		size_t in_size,
		struct html_tree_t *restrict tree,
		unsigned char debug_mode)
{
	struct html_parser_t parser = { 0 };
	int processed;

	int rc = html_lex(in_data, in_size, &tree->tokens);
	if (__builtin_expect(rc != 0, 0)) {
		return rc;
	}

	parser.tree = tree;
	parser.collect_debug_info = !!debug_mode;

	/* setting the initial stack size to 1. index 0 is reserved so that we needn't check if
	 * node_stack_size-1 is negative in push_node()
	 */
	parser.node_stack_size = 1;

	/* process all tokens */
	while (parser.current < tree->tokens.count) {
		processed = read_node(&parser);

		if (processed) {
			if (__builtin_expect(parser.exception_pending, 0))
				break;
		}
		else {
			parser.exception_pending = 1;
			parser.exception_msg = "Invalid syntax";
			parser.exception_location = parser.current;
			break;
		}
	}

	if (__builtin_expect(parser.exception_pending, 0)) {
		const utf32_t *p;
		const utf32_t *begin = tree->tokens.base + tree->tokens.begin[parser.exception_location];
		int column_num = 0;
		int line_num = 0;

		for (p = in_data; p < begin; ++p) {
			if (*p != '\n') {
				++column_num;
			}
			else {
				++line_num;
				column_num = 0;
			}
		}

		fprintf(stderr, "html_parse: '%s' on line %d, column %d\n",
				parser.exception_msg, line_num+1, column_num+1);
		rc = -1;
	}

	if (debug_mode && (rc == 0 || processed == 0)) {
		html_dump_parse_table(parser.tree, parser.node_tokens);
	}

	return rc;
}

static int read_node(struct html_parser_t *restrict parser)
{
	int old_position = parser->current;

	int processed =
		   read_node_script(parser)
		|| read_node_style(parser)
		|| read_node_open_tag(parser)
		|| read_node_close_tag(parser)
		|| read_node_open_foreign_tag(parser)
		|| read_node_variable(parser)
		|| read_node_text(parser)
		|| read_node_whitespace(parser)
		|| read_node_comment(parser);

	if (parser->collect_debug_info) {
		size_t last_node_idx = parser->tree->node_count - 1;
		struct html_token_range_t *restrict range = parser->node_tokens + last_node_idx;

		if (range->begin == range->end) {
			range->begin = old_position;
		}
		range->end = parser->current;
	}

	return processed;
}

static int read_node_open_tag_attributes(
		struct html_parser_t *restrict parser, html_token_idx_t node, html_token_idx_t p)
{
	struct html_tree_t *restrict tree = parser->tree;
	const enum html_token_id_t *restrict id = tree->tokens.id;
	html_token_idx_t attrib_name_start = p;
	html_token_idx_t attrib_name_end = 0;
	html_token_idx_t attrib_value = 0;
	size_t attrib_idx = tree->attrib_count;

	while (
		id[p] == HTML_TOKEN_IDENTIFIER || id[p] == HTML_TOKEN_HTML || id[p] == HTML_TOKEN_STYLE ||
		id[p] == HTML_TOKEN_DATA || html_tree_token_is_self_closing(id[p])) {
		++p;

		while (
			id[p] == HTML_TOKEN_IDENTIFIER || id[p] == HTML_TOKEN_HTML ||
			id[p] == HTML_TOKEN_STYLE || id[p] == HTML_TOKEN_HYPHEN || id[p] == HTML_TOKEN_DATA ||
			id[p] == HTML_TOKEN_COLON || html_tree_token_is_self_closing(id[p])) {
			++p;
		}

		attrib_name_end = p;
		EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);

		if (id[p] == HTML_TOKEN_EQUAL) {
			++p;

			EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
			attrib_value = p;
			EXPECT_TOKEN_4_1(
				tree, p, HTML_TOKEN_SINGLEQUOTE_STRING, HTML_TOKEN_DOUBLEQUOTE_STRING,
				HTML_TOKEN_IDENTIFIER, HTML_TOKEN_TEXT);
			EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
		}
		else {
			attrib_value = 0;
		}

		tree->attrib_parent[attrib_idx] = node;
		tree->attrib_name[attrib_idx].begin = attrib_name_start;
		tree->attrib_name[attrib_idx].end = attrib_name_end;
		tree->attrib_value[attrib_idx] = attrib_value;

		if (attrib_idx < HTML_TREE_MAX_ATTRIBUTES) {
			++attrib_idx;
		}
		else {
			parser->exception_pending = 1;
			parser->exception_msg = "Not enough space for attributes";
			parser->exception_location = node;
			return 1;
		}

		attrib_name_start = p;
	}

	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);

	if (id[p] == HTML_TOKEN_GREATERTHAN) {
		++p;
		tree->attrib_count = attrib_idx;
		parser->current = p;
	}
	else {
		EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_SLASH);
		EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_GREATERTHAN);

		tree->attrib_count = attrib_idx;
		pop_node(parser, node);
		parser->current = p;
	}

	return 1;
}

static int read_node_open_tag(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;

	html_token_idx_t p = parser->current;
	html_token_idx_t tag_name = 0;

	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_LESSTHAN);
	tag_name = p;

	if (__builtin_expect(
		tree->tokens.id[p] != HTML_TOKEN_IDENTIFIER && tree->tokens.id[p] != HTML_TOKEN_HTML &&
		!html_tree_token_is_self_closing(tree->tokens.id[p]) , 0)) {
		return 0;
	}
	++p;

	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);

	/* By this time, we are pretty certain that we are parsing a tag. If we are wrong, we'll
	 * reset the state to undo the push_node operation
	 */
	push_node(parser, tag_name);

	if (read_node_open_tag_attributes(parser, tag_name, p)) {
		return 1;
	}

	--parser->tree->node_count;
	--parser->node_stack_size;

	return 0;
}

static int read_node_close_tag(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;

	html_token_idx_t p = parser->current;
	html_token_idx_t tag_name = 0;

	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_LESSTHAN);
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_SLASH);
	tag_name = p;

	if (__builtin_expect(
		tree->tokens.id[p] != HTML_TOKEN_IDENTIFIER && tree->tokens.id[p] != HTML_TOKEN_HTML &&
		!html_tree_token_is_self_closing(tree->tokens.id[p]), 0)) {
		return 0;
	}
	++p;

	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_GREATERTHAN);

	pop_node(parser, tag_name);

	parser->current = p;
	return 1;
}

static int read_node_open_foreign_tag(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;

	html_token_idx_t p = parser->current;
	html_token_idx_t tag_name = 0;

	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_LESSTHAN);
	tag_name = p;
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_QUESTIONMARK);
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_IDENTIFIER);
	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);

	/* By this time, we are pretty certain that we are parsing a tag. If we are wrong, we'll
	 * reset the state to undo the push_node operation
	 */
	push_node(parser, tag_name);

	do {
		while (tree->tokens.id[p] != HTML_TOKEN_QUESTIONMARK) {
			++p;
		}
		++p;
	} while (tree->tokens.id[p] != HTML_TOKEN_GREATERTHAN);
	++p;
	
	tag_name = p;
	pop_node(parser, tag_name);

	parser->current = p;
	return 1;
}

static int read_node_script(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;

	html_token_idx_t p = parser->current;
	html_token_idx_t tag_name = 0;

	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_LESSTHAN);
	tag_name = p;
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_SCRIPT);
	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);

	/* By this time, we are pretty certain that we are parsing a script tag. If we are wrong, we'll
	 * reset the state to undo the push_node operation
	 */
	push_node(parser, tag_name);

	if (read_node_open_tag_attributes(parser, tag_name, p)) {
		do {
			while (tree->tokens.id[p] != HTML_TOKEN_LESSTHAN) {
				++p;
			}
			++p;
		} while (tree->tokens.id[p] != HTML_TOKEN_SLASH);
		++p;
		
		tag_name = p;
		EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_SCRIPT);
		EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
		EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_GREATERTHAN);

		push_text(parser, parser->current, tag_name-2);
		pop_node(parser, tag_name);

		parser->current = p;
		return 1;
	}

	--parser->tree->node_count;
	--parser->node_stack_size;

	return 0;
}

static int read_node_style(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;

	html_token_idx_t p = parser->current;
	html_token_idx_t tag_name = 0;

	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_LESSTHAN);
	tag_name = p;
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_STYLE);
	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);

	/* By this time, we are pretty certain that we are parsing a tag. If we are wrong, we'll
	 * reset the state to undo the push_node operation
	 */
	push_node(parser, tag_name);

	if (read_node_open_tag_attributes(parser, tag_name, p)) {
		do {
			while (tree->tokens.id[p] != HTML_TOKEN_LESSTHAN) {
				++p;
			}
			++p;
		} while (tree->tokens.id[p] != HTML_TOKEN_SLASH);
		++p;
		
		tag_name = p;
		EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_STYLE);
		EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
		EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_GREATERTHAN);

		push_text(parser, parser->current, tag_name-2);
		pop_node(parser, tag_name);

		parser->current = p;
		return 1;
	}

	--parser->tree->node_count;
	--parser->node_stack_size;

	return 0;
}

static int read_node_text(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;
	html_token_idx_t p = parser->current;
	unsigned char all_whitespace = 1;

	while (1) {
		switch (tree->tokens.id[p]) {
		case HTML_TOKEN_TEXT:
		case HTML_TOKEN_IDENTIFIER:
		case HTML_TOKEN_AMPERSAND:
		case HTML_TOKEN_SEMICOLON:
		case HTML_TOKEN_HYPHEN:
		case HTML_TOKEN_COMMA:
		case HTML_TOKEN_COLON:
		case HTML_TOKEN_SINGLEQUOTE:
		case HTML_TOKEN_DOUBLEQUOTE:
		case HTML_TOKEN_EXCLAMATIONMARK:
		case HTML_TOKEN_QUESTIONMARK:
		case HTML_TOKEN_STYLE:
		case HTML_TOKEN_OPENPAREN:
		case HTML_TOKEN_CLOSEPAREN:
		case HTML_TOKEN_INCLUDE:
			all_whitespace = 0;
		case HTML_TOKEN_WHITESPACE:
			++p;
			continue;
		default:
			break;
		}
		break;
	}

	if (parser->current != p) {
		/* Drop text nodes that are just whitespace. I'm not sure if this adheres to the standard
		 * but whitespace text nodes account for a lot of noise and we can save a lot of space
		 * by eliminating them. */
		if (!all_whitespace) {
			push_text(parser, parser->current, p);
		}
		parser->current = p;
		return 1;
	}

	return 0;
}

static int read_node_whitespace(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;
	html_token_idx_t p = parser->current;
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_WHITESPACE);
	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
	parser->current = p;
	return 1;
}

static int read_node_comment(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;
	html_token_idx_t p = parser->current;
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_COMMENT);
	parser->current = p;
	return 1;
}

static int read_node_variable(struct html_parser_t *restrict parser)
{
	struct html_tree_t *restrict tree = parser->tree;

	html_token_idx_t p = parser->current;
	html_token_idx_t var_name = 0;

	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_OPENBRACE);
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_OPENBRACE);
	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
	var_name = p;
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_IDENTIFIER);
	EXPECT_TOKEN_1_0N(tree, p, HTML_TOKEN_WHITESPACE);
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_CLOSEBRACE);
	EXPECT_TOKEN_1_1(tree, p, HTML_TOKEN_CLOSEBRACE);

	push_node(parser, var_name);
	pop_node(parser, var_name);

	parser->current = p;
	return 1;
}

/* Parse tree operations */
static void push_node(struct html_parser_t *restrict parser, html_token_idx_t tag_name)
{
	struct html_tree_t *restrict tree = parser->tree;
	size_t i = tree->node_count;

	if (i < HTML_TREE_MAX_NODES) {
		/* Check if last tag was self-closing */
		html_token_idx_t last = parser->node_stack[parser->node_stack_size-1];

		if (html_tree_token_is_self_closing(tree->tokens.id[last])) {
			pop_node(parser, last);
			last = parser->node_stack[parser->node_stack_size-1];
		}

		tree->node_parent[i] = last;
		tree->node_tag_name[i] = tag_name;
		tree->node_count = i+1;

		parser->node_stack[parser->node_stack_size] = tag_name;
		++parser->node_stack_size;

		if (__builtin_expect(parser->node_stack_size >= HTML_TREE_MAX_STACK_SIZE, 0)) {
			parser->exception_pending = 1;
			parser->exception_msg =
				"Not enough space for node stack. Increase HTML_TREE_MAX_STACK_SIZE";
			parser->exception_location = tag_name;
		}
	}
	else {
		parser->exception_pending = 1;
		parser->exception_msg = "Not enough space for tree. Increase HTML_TREE_MAX_NODES";
		parser->exception_location = tag_name;
	}
}

static void push_text(
		struct html_parser_t *restrict parser, html_token_idx_t begin, html_token_idx_t end)
{
	struct html_tree_t *restrict tree = parser->tree;
	size_t i = tree->text_count;
	html_token_idx_t node = parser->node_stack[parser->node_stack_size-1];

	if (i < HTML_TREE_MAX_TEXTS) {
		html_token_idx_t prev;

		/* Check if last tag was self-closing */
		if (html_tree_token_is_self_closing(tree->tokens.id[node])) {
			prev = node;
			pop_node(parser, node);
			node = parser->node_stack[parser->node_stack_size-1];
		}
		else {
			prev = tree->node_tag_name[tree->node_count-1];
		}

		struct html_token_range_t range;
		range.begin = begin;
		range.end = end;

		tree->text_parent[i] = node;
		tree->text_prev[i] = prev;
		tree->text[i] = range;
		tree->text_count = i+1;
	}
	else {
		parser->exception_pending = 1;
		parser->exception_msg = "Not enough space for tree. Increase HTML_TREE_MAX_TEXTS";
		parser->exception_location = node;
	}
}

static void pop_node(struct html_parser_t *restrict parser, html_token_idx_t tag_name)
{
	struct html_tree_t *restrict tree = parser->tree;
	enum html_token_id_t *restrict id = tree->tokens.id;
	int32_t i;

	for (i=parser->node_stack_size-1; i>0; --i) {
		if (id[parser->node_stack[i]] == id[tag_name]) {
			parser->node_stack_size = i;
			return;
		}
	}
}

static char *get_token_string(const struct html_tree_t *restrict tree, html_token_idx_t idx)
{
	char *str;
	size_t size = 1;
	const utf32_t *begin = tree->tokens.base + tree->tokens.begin[idx];
	html_token_size_t token_size = tree->tokens.size[idx];

	unicode_write_utf8_string(begin, token_size, &str, &size);
	str[size] = 0;

	return str;
}

void html_dump_parse_table(
		const struct html_tree_t *restrict tree,
		const struct html_token_range_t *restrict node_tokens)
{
	size_t i;
	const struct html_tokens_t *restrict tokens = &tree->tokens;

	printf("tokens:\n");
	for (i=0; i < tokens->count; ++i) {
		enum html_token_id_t id = tokens->id[i];
		char *token_str = get_token_string(tree, i);

		printf("\t[%03ld]  --  id[%d]\ttext[%s]\n", i, id, token_str);

		unicode_utf8_string_free(&token_str, 1);
	}


	printf("\nnodes:\n");
	for (i=0; i < tree->node_count; ++i) {
		char *parent = get_token_string(tree, tree->node_parent[i]);
		char *tag_name = get_token_string(tree, tree->node_tag_name[i]);

		printf("\t[%03ld]  --  tag[%s]\tparent[%s]\n", i, tag_name, tree->node_parent[i]? parent : "");

		unicode_utf8_string_free(&parent, 1);
		unicode_utf8_string_free(&tag_name, 1);

		if (node_tokens) {
			size_t j;
			const struct html_token_range_t *restrict range = node_tokens + i;

			for (j=range->begin; j < range->end; ++j) {
				printf("\t\t");
				trace_token(tokens, j);
			}
		}
	}

	printf("\nattributes:\n");
	for (i=0; i < tree->attrib_count; ++i) {
		char *parent = get_token_string(tree, tree->attrib_parent[i]);
		char *value = get_token_string(tree, tree->attrib_value[i]);
		struct html_token_range_t attrib_name = tree->attrib_name[i];

		printf("\tname[");
		for (int j=attrib_name.begin; j < attrib_name.end; ++j) {
			char *name = get_token_string(tree, j);
			fputs(name, stdout);
			unicode_utf8_string_free(&name, 1);
		}

		printf("]\tvalue[%s]\tparent[%s]\n", tree->attrib_value[i] ? value : "true", parent);

		unicode_utf8_string_free(&parent, 1);
		unicode_utf8_string_free(&value, 1);
	}

	printf("\ntext nodes:\n");
	for (i=0; i < tree->text_count; ++i) {
		char *parent = get_token_string(tree, tree->text_parent[i]);
		char *prev = get_token_string(tree, tree->text_prev[i]);
		printf("\tparent[%s]\tprev[%s]\tvalue[", parent, prev);

		struct html_token_range_t range = tree->text[i];
		for (html_token_idx_t j = range.begin; j < range.end; ++j) {
			char *text = get_token_string(tree, j);
			printf("%s", text);
			unicode_utf8_string_free(&text, 1);
		}

		printf("]\n");
		unicode_utf8_string_free(&parent, 1);
	}
}

static void trace_token(const struct html_tokens_t *restrict tokens, html_token_idx_t idx)
{
	enum html_token_id_t token_id = tokens->id[idx];
	const utf32_t *restrict in_data = tokens->base + tokens->begin[idx];
	html_token_size_t in_size = tokens->size[idx];

	char *str;
	size_t size = 1;

	unicode_write_utf8_string(in_data, in_size, &str, &size);

	/* guaranteed space because the inital value of 'size' is 1 */
	str[size] = 0;

	printf("[%03d]  --  ", idx);

	switch (token_id) {
	case HTML_TOKEN_GREATERTHAN:
		printf("[>] '%s'\n", str);
		break;
	case HTML_TOKEN_LESSTHAN:
		printf("[<]: '%s'\n", str);
		break;
	case HTML_TOKEN_INCLUDE:
		printf("[include]: 'include'\n");
		break;
	case HTML_TOKEN_IDENTIFIER:
		printf("[identifier]: '%s'\n", str);
		break;
	case HTML_TOKEN_WHITESPACE:
		printf("[space]: ' '\n");
		break;
	case HTML_TOKEN_EQUAL:
		printf("[=]: '%s'\n", str);
		break;
	case HTML_TOKEN_SINGLEQUOTE:
		printf("[']: '%s'\n", str);
		break;
	case HTML_TOKEN_DOUBLEQUOTE:
		printf("[\"]: '%s'\n", str);
		break;
	case HTML_TOKEN_AMPERSAND:
		printf("[&]: '%s'\n", str);
		break;
	case HTML_TOKEN_EXCLAMATIONMARK:
		printf("[!]: '%s'\n", str);
		break;
	case HTML_TOKEN_QUESTIONMARK:
		printf("[?]: '%s'\n", str);
		break;
	case HTML_TOKEN_HYPHEN:
		printf("[-]: '%s'\n", str);
		break;
	case HTML_TOKEN_COLON:
		printf("[:]: '%s'\n", str);
		break;
	case HTML_TOKEN_OPENBRACE:
		printf("[{]: '%s'\n", str);
		break;
	case HTML_TOKEN_CLOSEBRACE:
		printf("[}] '%s'\n", str);
		break;
	case HTML_TOKEN_OPENPAREN:
		printf("[(]: '%s'\n", str);
		break;
	case HTML_TOKEN_CLOSEPAREN:
		printf("[)]: '%s'\n", str);
		break;
	case HTML_TOKEN_SEMICOLON:
		printf("[;]: '%s'\n", str);
		break;
	case HTML_TOKEN_ASTERISK:
		printf("[*]: '%s'\n", str);
		break;
	case HTML_TOKEN_HASH:
		printf("[#]: '%s'\n", str);
		break;
	case HTML_TOKEN_COMMA:
		printf("[,]: '%s'\n", str);
		break;
	case HTML_TOKEN_SLASH:
		printf("[/]: '%s'\n", str);
		break;
	case HTML_TOKEN_DOCTYPE:
		printf("[doctype]: '%s'\n", str);
		break;
	case HTML_TOKEN_HTML:
		printf("[html]: '%s'\n", str);
		break;
	case HTML_TOKEN_META:
		printf("[meta]: '%s'\n", str);
		break;
	case HTML_TOKEN_LINK:
		printf("[link]: '%s'\n", str);
		break;
	case HTML_TOKEN_IMG:
		printf("[img]: '%s'\n", str);
		break;
	case HTML_TOKEN_BR:
		printf("[br]: '%s'\n", str);
		break;
	case HTML_TOKEN_INPUT:
		printf("[input]: '%s'\n", str);
		break;
	case HTML_TOKEN_DATA:
		printf("[data]: '%s'\n", str);
		break;
	case HTML_TOKEN_SCRIPT:
		printf("[script]: '%s'\n", str);
		break;
	case HTML_TOKEN_SINGLEQUOTE_STRING:
		printf("[single-quote string]: '%s'\n", str);
		break;
	case HTML_TOKEN_DOUBLEQUOTE_STRING:
		printf("[double-quote string]: '%s'\n", str);
		break;
	case HTML_TOKEN_TEXT:
		printf("[text]: '%s'\n", str);
		break;
	case HTML_TOKEN_COMMENT:
		printf("[comment]: '%s'\n", str);
		break;
	case HTML_TOKEN_STYLE:
		printf("[style]: '%s'\n", str);
		break;
	case HTML_TOKEN_SELFCLOSING_BEGIN:
	case HTML_TOKEN_SELFCLOSING_END:
	case HTML_TOKEN_END:
		/* intentionally left blank */
		break;
	}

	unicode_utf8_string_free(&str, 1);
}


