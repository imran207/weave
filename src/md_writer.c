// SPDX-License-Identifier: GPL-2.0-only
/*
 * md_writer.c
 *
 * Copyright (C) 2023  Imran Haider
 */

#include "md_writer.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct md_writer_t {
	const struct md_tokens_t *restrict tokens;
	utf32_t *restrict output;
	size_t current;
	size_t max_size;

	size_t link_idx;
	size_t tooltip_idx;

	/* parsing error handling */
	const char *restrict exception_msg;

	/* flags */
	uint8_t exception_pending :1;
	uint8_t list_mode         :1;
	uint8_t list_ordered      :1;
	uint8_t list_first_item   :1;
	uint8_t title_mode        :1;
};

/* Write operations */
static size_t get_column_size(struct md_writer_t *restrict writer);
static void append_token(struct md_writer_t *restrict writer, md_token_idx_t token_idx);
static void append_token_char(struct md_writer_t *restrict writer, utf32_t ch);
static void append_token_string(struct md_writer_t *restrict writer, utf32_t *restrict str, size_t size);
static size_t append_blockquote(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx);
static size_t append_codeblock(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx);
static size_t append_label(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx);
static size_t append_image(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx,
	md_node_idx_t begin);


int md_write(
		utf32_t *restrict *restrict out_data, size_t *restrict out_size,
		const struct md_tree_t *restrict tree)
{
	int rc = 0;

	const size_t keyword_count = 2;
	utf32_t *keyword_data[keyword_count];
	size_t keyword_size[keyword_count];

	memset(keyword_size, 0, keyword_count * sizeof(size_t));

#define REGISTER_KEYWORD(id,str) unicode_read_ascii_string(str, sizeof(str)-1, keyword_data+id, keyword_size+id)
	REGISTER_KEYWORD(0, "---\n");
	REGISTER_KEYWORD(1, "#");

	/* create a lookup table to locate an instance of a token given its id */
	md_token_idx_t token_idx_table[MD_TOKEN_END] = {0};
	const struct md_tokens_t *restrict tokens = &tree->tokens;
	const enum md_token_id_t *restrict id = tokens->id;
	md_token_idx_t idx;

	for (idx=0; idx < tokens->count; ++idx) {
		enum md_token_id_t token_id = id[idx];
		token_idx_table[token_id] = idx;
	}

	/* allocate memory for output data */
	*out_size = MD_TREE_MAX_SIZE;
	*out_data = malloc(MD_TREE_MAX_SIZE * sizeof(utf32_t));

	size_t node_idx;
	struct md_writer_t writer = {0};
	size_t column_sizes[MD_TREE_MAX_TABLE_COLUMNS];

	writer.tokens = tokens;
	writer.output = *out_data;
	writer.max_size = MD_TREE_MAX_SIZE;

	if (tree->node_count) {
		int table_row = 0;
		int table_column = 0;
		int table_column_count = 0;

		for (node_idx = 0; node_idx < tree->node_count; ++node_idx) {
			struct md_node_t node = tree->node[node_idx];
			enum md_tag_id_t tag = tree->tag[node_idx];

			/* the order of these if-checks must match the order of the enum */
			if (tag < MD_TAG_HEADING_END) {
				enum md_tag_id_t i;
				for (i=MD_TAG_HEADING_BEGIN; i < tag; ++i)
					append_token_string(&writer, keyword_data[1], keyword_size[1]);

				append_token(&writer, token_idx_table[MD_TOKEN_SPACE]);
			}
			else if (tag < MD_TAG_STANDARD_TEXT_END) {
				enum md_tag_id_t id;
				for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
					append_token(&writer, token_idx_table[MD_TOKEN_ASTERISK]);

				md_token_idx_t idx;
				for (idx = node.begin; idx < node.end; ++idx)
					append_token(&writer, idx);

				for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
					append_token(&writer, token_idx_table[MD_TOKEN_ASTERISK]);
			}
			else if (tag == MD_TAG_CODE_TEXT) {
				append_token(&writer, token_idx_table[MD_TOKEN_BACKTICK]);

				md_token_idx_t i;
				for (i = node.begin; i < node.end; ++i)
					append_token(&writer, i);

				append_token(&writer, token_idx_table[MD_TOKEN_BACKTICK]);
			}
			else if (tag == MD_TAG_LINEBREAK) {
				if (table_column_count > 0) {
					/* reset table mode */
					table_column_count = 0;

					append_token(&writer, token_idx_table[MD_TOKEN_VERTICALPIPE]);
				}
				else if (writer.list_mode) {
					writer.list_mode = 0;
					append_token(&writer, token_idx_table[MD_TOKEN_NEWLINE]);
				}

				append_token(&writer, token_idx_table[MD_TOKEN_NEWLINE]);
			}
			else if (tag == MD_TAG_ORDERED_LIST) {
				writer.list_mode = 1;
				writer.list_ordered = 1;
				writer.list_first_item = 1;
			}
			else if (tag == MD_TAG_UNORDERED_LIST) {
				writer.list_mode = 1;
				writer.list_ordered = 0;
				writer.list_first_item = 1;
			}
			else if (tag == MD_TAG_LIST_ITEM) {
				if (writer.list_first_item == 0)
					append_token(&writer, token_idx_table[MD_TOKEN_NEWLINE]);
				else
					writer.list_first_item = 0;

				if (writer.list_ordered == 0)
					append_token(&writer, token_idx_table[MD_TOKEN_HYPHEN]);
				else
					append_token(&writer, token_idx_table[MD_TOKEN_PERIOD]);

				append_token(&writer, token_idx_table[MD_TOKEN_SPACE]);
			}
			else if (tag == MD_TAG_BLOCKQUOTE) {
				node_idx = append_blockquote(&writer, tree, token_idx_table, node_idx);
			}
			else if (tag == MD_TAG_CODE_BLOCK) {
				node_idx = append_codeblock(&writer, tree, token_idx_table, node_idx);
			}
			else if (tag == MD_TAG_LABEL) {
				if (writer.link_idx < tree->link_count) {
					if (tree->link[writer.link_idx].begin != node.begin) {
						node_idx = append_label(&writer, tree, token_idx_table, node_idx);

						if (
								writer.tooltip_idx < tree->tooltip_count &&
								tree->tooltip_parent[writer.tooltip_idx] == node.begin) {

							const struct md_node_t *restrict tooltip_node =
								tree->tooltip + writer.tooltip_idx;

							++writer.tooltip_idx;

							append_token(&writer, token_idx_table[MD_TOKEN_SPACE]);
							append_token(&writer, token_idx_table[MD_TOKEN_SINGLEQUOTE]);

							md_token_idx_t tooltip_token_idx;
							for (
									tooltip_token_idx = tooltip_node->begin;
									tooltip_token_idx < tooltip_node->end;
									++tooltip_token_idx) {

								// TODO: may need to escape single quotes in the tooltip
								append_token(&writer, tooltip_token_idx);
							}

							append_token(&writer, token_idx_table[MD_TOKEN_SINGLEQUOTE]);
						}

						append_token_char(&writer, ')');
					}
					else {
						append_token_char(&writer, '<');

						const struct md_node_t *restrict link_node = tree->link + writer.link_idx;
						++writer.link_idx;

						md_token_idx_t link_token_idx;

						for (
							link_token_idx = link_node->begin;
							link_token_idx < link_node->end;
							++link_token_idx)
							append_token(&writer, link_token_idx);

						append_token_char(&writer, '>');

						/* For a quick link, the node table and the link table will contain
						 * references to the same tokens. Since we only need one reference, we
						 * used the references from the link table and we'll be skipping over the
						 * references in the node table */
						while (
								node_idx < tree->node_count &&
								tree->node[node_idx].begin < link_node->end)
							++node_idx;
					}
				}
			}
			else if (tag == MD_TAG_IMG_ALTTEXT) {
				node_idx = append_image(&writer, tree, token_idx_table, node_idx, node.begin);
			}
			else if (tag == MD_TAG_HORIZONTAL_RULE) {
				append_token_string(&writer, keyword_data[0], keyword_size[0]);
			}
			else if (tag == MD_TAG_TABLE) {
				append_token(&writer, token_idx_table[MD_TOKEN_NEWLINE]);

				table_row = 0;
				table_column = 0;
				table_column_count = 0;
			}
			else if (tag == MD_TAG_TABLE_ROW) {
				/* for anything under the header row (which includes the separator line and the
				 * data cells), we need to write additional tokens to prepare the new row
				 */
				if (table_row > 0) {
					if (table_row > 1) {
						/* for the data rows, we simply add a vertical pipe for the end of the
						 * previous row */
						append_token(&writer, token_idx_table[MD_TOKEN_VERTICALPIPE]);
					}
					else {
						int col;

						/* before we start writing the separator row, we have a chance to get
						 * the width of the last cell in the header row
						 */
						column_sizes[table_column_count-1] = get_column_size(&writer);

						/* add the vertical pipe at the end of the previous row and another one
						 * at the start of the separator row
						 */
						append_token(&writer, token_idx_table[MD_TOKEN_VERTICALPIPE]);
						append_token(&writer, token_idx_table[MD_TOKEN_NEWLINE]);
						append_token(&writer, token_idx_table[MD_TOKEN_VERTICALPIPE]);

						/* write the hyphens and vertical pipes for the remaining columns. the
						 * number of hyphens depend on the column width
						 */
						for (col=0; col<table_column_count; ++col) {
							size_t i, column_size = column_sizes[col];

							for (i=0; i<column_size; ++i)
								append_token(&writer, token_idx_table[MD_TOKEN_HYPHEN]);

							append_token(&writer, token_idx_table[MD_TOKEN_VERTICALPIPE]);
						}
					}
				}

				/* move onto the next row */
				append_token(&writer, token_idx_table[MD_TOKEN_NEWLINE]);

				table_column = 0;
				++table_row;
			}
			else if (tag == MD_TAG_TABLE_CELL) {
				/* at the start of the next cell, we know when the previous cell (or column)
				 * ended. this will give us the width of all but the last column. for the last
				 * column, we will check its size when we are about to start writing a new row
				 * of table cells
				 */
				if (table_column > 0)
					column_sizes[table_column-1] = get_column_size(&writer);

				append_token(&writer, token_idx_table[MD_TOKEN_VERTICALPIPE]);
				++table_column;

				if (table_column > table_column_count) {
					if (__builtin_expect(
							writer.exception_pending == 1 ||
							table_column > MD_TREE_MAX_TABLE_COLUMNS, 0)) {
						if (__builtin_expect(writer.exception_pending == 0, 0)) {
							writer.exception_pending = 1;
							writer.exception_msg =
								"Not enough space for table columns. Increase HTML_PARSER_MAX_TABLE_COLUMNS";
						}

						break;
					}

					table_column_count = table_column;
				}
			}
		}
	}

	if (__builtin_expect(writer.exception_pending == 1, 0)) {
		fprintf(stderr, "md_write: '%s'\n", writer.exception_msg);
		rc = -1;
	}

	unicode_utf32_string_free(keyword_data, keyword_count);
	*out_size = writer.current;

	return rc;
}

static size_t get_column_size(struct md_writer_t *restrict writer)
{
	int i;
	utf32_t *restrict output = writer->output;

	for (i = writer->current-1; i >= 0; --i) {
		uint32_t ch = output[i];

		if (ch == '\n' || ch == '\r' || ch == '|')
			break;
	}

	return writer->current-1 - i;
}

static void append_token(struct md_writer_t *restrict writer, md_token_idx_t token_idx)
{
	md_token_size_t size = writer->tokens->size[token_idx];

	if (__builtin_expect(writer->exception_pending == 1 || writer->max_size < size, 0)) {
		if (__builtin_expect(writer->exception_pending == 0, 0)) {
			writer->exception_msg = "Out of memory. Incrase MD_TREE_MAX_SIZE";
			writer->exception_pending = 1;
		}
		return;
	}

	const utf32_t *begin = writer->tokens->base + writer->tokens->begin[token_idx];
	memcpy(writer->output + writer->current, begin, size * sizeof(utf32_t));
	writer->current += size;
	writer->max_size -= size;
}

static void append_token_char(struct md_writer_t *restrict writer, utf32_t ch)
{
	if (__builtin_expect(writer->exception_pending == 1 || writer->max_size < 1, 0)) {
		if (__builtin_expect(writer->exception_pending == 0, 0)) {
			writer->exception_msg = "Out of memory. Incrase MD_TREE_MAX_SIZE";
			writer->exception_pending = 1;
		}
		return;
	}

	writer->output[writer->current] = ch;
	++writer->current;
	--writer->max_size;
}

static void append_token_string(struct md_writer_t *restrict writer, utf32_t *restrict str, size_t size)
{
	if (__builtin_expect(writer->exception_pending == 1 || writer->max_size < size, 0)) {
		if (__builtin_expect(writer->exception_pending == 0, 0)) {
			writer->exception_msg = "Out of memory. Incrase MD_TREE_MAX_SIZE";
			writer->exception_pending = 1;
		}
		return;
	}

	memcpy(writer->output + writer->current, str, size * sizeof(utf32_t));
	writer->current += size;
	writer->max_size -= size;
}

static size_t append_blockquote(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx)
{
	/* node stack */
	md_token_idx_t node_stack[MD_TREE_MAX_STACK_SIZE];
	size_t node_stack_size = 1;
	uint8_t start_of_line = 1;

	/* add the BLOCKQUOTE node as the root element in the node stack. once the stack is emptied,
	 * we'll know that all children nodes within the BLOCKQUOTE node has been processed.
	 */
	node_stack[0] = tree->node[node_idx].begin;
	++node_idx;

	while (node_idx < tree->node_count) {
		struct md_node_t node = tree->node[node_idx];
		enum md_tag_id_t tag = tree->tag[node_idx];
		md_token_idx_t parent = tree->parent[node_idx];

		/* pop from the stack until we find the current node's parent */
		while (node_stack_size > 0 && parent != node_stack[node_stack_size-1]) {
			--node_stack_size;
		}

		if (node_stack_size == 0)
			/* We're done with all the children nodes within the BLOCKQUOTE node */
			break;

		if (start_of_line) {
			start_of_line = 0;
			append_token(writer, token_idx_table[MD_TOKEN_GREATERTHAN]);
		}

		if (tag > MD_TAG_STANDARD_TEXT_BEGIN && tag < MD_TAG_STANDARD_TEXT_END) {
			enum md_tag_id_t id;
			for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
				append_token(writer, token_idx_table[MD_TOKEN_ASTERISK]);

			if (node.begin < node.end-1) {
				md_token_idx_t idx;
				for (idx = node.begin; idx < node.end; ++idx)
					append_token(writer, idx);
			}
			else {
				/* text nodes for new lines are always one token wide */
				if (tree->tokens.id[node.begin] == MD_TOKEN_NEWLINE)
					start_of_line = 1;

				append_token(writer, node.begin);
			}

			for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
				append_token(writer, token_idx_table[MD_TOKEN_ASTERISK]);
		}
		else if (tag == MD_TAG_CODE_TEXT) {
			append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);

			md_token_idx_t i;
			for (i = node.begin; i < node.end; ++i)
				append_token(writer, i);

			append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);
		}
		else if (tag == MD_TAG_LINEBREAK) {
			append_token(writer, token_idx_table[MD_TOKEN_NEWLINE]);
		}
		else if (tag == MD_TAG_LABEL) {
			if (writer->link_idx < tree->link_count) {
				if (tree->link[writer->link_idx].begin != node.begin) {
					node_idx = append_label(writer, tree, token_idx_table, node_idx);

					if (
							writer->tooltip_idx < tree->tooltip_count &&
							tree->tooltip_parent[writer->tooltip_idx] == node.begin) {

						const struct md_node_t *restrict tooltip_node =
							tree->tooltip + writer->tooltip_idx;

						++writer->tooltip_idx;

						append_token(writer, token_idx_table[MD_TOKEN_SPACE]);
						append_token(writer, token_idx_table[MD_TOKEN_SINGLEQUOTE]);

						md_token_idx_t tooltip_token_idx;
						for (
								tooltip_token_idx = tooltip_node->begin;
								tooltip_token_idx < tooltip_node->end;
								++tooltip_token_idx) {

							// TODO: may need to escape single quotes in the tooltip
							append_token(writer, tooltip_token_idx);
						}

						append_token(writer, token_idx_table[MD_TOKEN_SINGLEQUOTE]);
					}

					append_token_char(writer, ')');
				}
				else {
					append_token_char(writer, '<');

					const struct md_node_t *restrict link_node = tree->link + writer->link_idx;
					++writer->link_idx;

					md_token_idx_t link_token_idx;

					for (
						link_token_idx = link_node->begin;
						link_token_idx < link_node->end;
						++link_token_idx) {

						append_token(writer, link_token_idx);
					}

					append_token_char(writer, '>');

					/* For a quick link, the node table and the link table will contain
					 * references to the same tokens. Since we only need one reference, we
					 * used the references from the link table and we'll be skipping over the
					 * references in the node table */
					while (
							node_idx < tree->node_count &&
							tree->node[node_idx].begin < link_node->end)
						++node_idx;
				}
			}
		}
		else if (tag == MD_TAG_IMG_ALTTEXT) {
			node_idx = append_image(writer, tree, token_idx_table, node_idx, node.begin);
		}
		else {
			/* Something else that cannot be part of a blockquote */
			break;
		}

		node_stack[node_stack_size] = node.begin;
		if (node_stack_size < MD_TREE_MAX_STACK_SIZE) {
			++node_stack_size;
		}
		else {
			writer->exception_pending = 1;
			writer->exception_msg =
				"Not enough space for node stack. Increase MD_TREE_MAX_STACK_SIZE";
			break;
		}

		++node_idx;
	}

	/* Return the index of the last node processed, not the index of the next node to be processed.
	 * This is expected by the caller
	 */
	return node_idx - 1;
}

static size_t append_codeblock(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx)
{
	/* node stack */
	md_token_idx_t node_stack[MD_TREE_MAX_STACK_SIZE];
	size_t node_stack_size = 1;

	append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);
	append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);
	append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);

	/* add the CODE_BLOCK node as the root element in the node stack. once the stack is emptied,
	 * we'll know that all children nodes within the CODE_BLOCK node has been processed.
	 */
	node_stack[0] = tree->node[node_idx].begin;
	++node_idx;

	while (node_idx < tree->node_count) {
		struct md_node_t node = tree->node[node_idx];
		enum md_tag_id_t tag = tree->tag[node_idx];
		md_token_idx_t parent = tree->parent[node_idx];

		/* pop from the stack until we find the current node's parent */
		while (node_stack_size > 0 && parent != node_stack[node_stack_size-1]) {
			--node_stack_size;
		}

		if (node_stack_size == 0)
			/* We're done with all the children nodes within the CODE_BLOCK node */
			break;

		if (tag > MD_TAG_STANDARD_TEXT_BEGIN && tag < MD_TAG_STANDARD_TEXT_END) {
			enum md_tag_id_t id;
			for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
				append_token(writer, token_idx_table[MD_TOKEN_ASTERISK]);

			md_token_idx_t idx;
			for (idx = node.begin; idx < node.end; ++idx)
				append_token(writer, idx);

			for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
				append_token(writer, token_idx_table[MD_TOKEN_ASTERISK]);
		}
		else if (tag == MD_TAG_LINEBREAK) {
			append_token(writer, token_idx_table[MD_TOKEN_NEWLINE]);
		}
		else if (tag == MD_TAG_LABEL) {
			if (writer->link_idx < tree->link_count) {
				if (tree->link[writer->link_idx].begin != node.begin) {
					node_idx = append_label(writer, tree, token_idx_table, node_idx);

					if (
							writer->tooltip_idx < tree->tooltip_count &&
							tree->tooltip_parent[writer->tooltip_idx] == node.begin) {

						const struct md_node_t *restrict tooltip_node =
							tree->tooltip + writer->tooltip_idx;

						++writer->tooltip_idx;

						append_token(writer, token_idx_table[MD_TOKEN_SPACE]);
						append_token(writer, token_idx_table[MD_TOKEN_SINGLEQUOTE]);

						md_token_idx_t tooltip_token_idx;
						for (
								tooltip_token_idx = tooltip_node->begin;
								tooltip_token_idx < tooltip_node->end;
								++tooltip_token_idx) {

							// TODO: may need to escape single quotes in the tooltip
							append_token(writer, tooltip_token_idx);
						}

						append_token(writer, token_idx_table[MD_TOKEN_SINGLEQUOTE]);
					}

					append_token_char(writer, ')');
				}
				else {
					append_token_char(writer, '<');

					const struct md_node_t *restrict link_node = tree->link + writer->link_idx;
					++writer->link_idx;

					md_token_idx_t link_token_idx;

					for (
						link_token_idx = link_node->begin;
						link_token_idx < link_node->end;
						++link_token_idx) {

						append_token(writer, link_token_idx);
					}

					append_token_char(writer, '>');

					/* For a quick link, the node table and the link table will contain
					 * references to the same tokens. Since we only need one reference, we
					 * used the references from the link table and we'll be skipping over the
					 * references in the node table */
					while (
							node_idx < tree->node_count &&
							tree->node[node_idx].begin < link_node->end)
						++node_idx;
				}
			}
		}
		else if (tag == MD_TAG_IMG_ALTTEXT) {
			node_idx = append_image(writer, tree, token_idx_table, node_idx, node.begin);
		}
		else {
			/* Something else that cannot be part of a code block */
			break;
		}

		node_stack[node_stack_size] = node.begin;
		if (node_stack_size < MD_TREE_MAX_STACK_SIZE) {
			++node_stack_size;
		}
		else {
			writer->exception_pending = 1;
			writer->exception_msg =
				"Not enough space for node stack. Increase MD_TREE_MAX_STACK_SIZE";
			break;
		}

		++node_idx;
	}

	append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);
	append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);
	append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);

	/* Return the index of the last node processed, not the index of the next node to be processed.
	 * This is expected by the caller
	 */
	return node_idx - 1;
}

static size_t append_label(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx)
{
	append_token_char(writer, '[');

	/* node stack */
	md_token_idx_t node_stack[MD_TREE_MAX_STACK_SIZE];
	size_t node_stack_size = 1;

	/* add the LABEL node as the root element in the node stack. once the stack is emptied, we'll
	 * know that all children nodes within the LABEL node has been processed.
	 */
	node_stack[0] = tree->node[node_idx].begin;
	++node_idx;

	while (node_idx < tree->node_count) {
		struct md_node_t node = tree->node[node_idx];
		enum md_tag_id_t tag = tree->tag[node_idx];
		md_token_idx_t parent = tree->parent[node_idx];

		/* pop from the stack until we find the current node's parent */
		while (node_stack_size > 0 && parent != node_stack[node_stack_size-1]) {
			--node_stack_size;
		}

		if (node_stack_size == 0)
			/* We're done with all the children nodes within the LABEL node */
			break;

		if (tag > MD_TAG_STANDARD_TEXT_BEGIN && tag < MD_TAG_STANDARD_TEXT_END) {
			enum md_tag_id_t id;
			for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
				append_token(writer, token_idx_table[MD_TOKEN_ASTERISK]);

			md_token_idx_t idx;
			for (idx = node.begin; idx < node.end; ++idx)
				append_token(writer, idx);

			for (id = MD_TAG_STANDARD_TEXT_BEGIN+1; id < tag; ++id)
				append_token(writer, token_idx_table[MD_TOKEN_ASTERISK]);
		}
		else if (tag == MD_TAG_CODE_TEXT) {
			append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);

			md_token_idx_t i;
			for (i = node.begin; i < node.end; ++i)
				append_token(writer, i);

			append_token(writer, token_idx_table[MD_TOKEN_BACKTICK]);
		}
		else if (tag == MD_TAG_IMG_ALTTEXT) {
			node_idx = append_image(writer, tree, token_idx_table, node_idx, node.begin);
		}
		else {
			/* Something else that cannot be part of a label */
			break;
		}

		node_stack[node_stack_size] = node.begin;
		if (node_stack_size < MD_TREE_MAX_STACK_SIZE) {
			++node_stack_size;
		}
		else {
			writer->exception_pending = 1;
			writer->exception_msg =
				"Not enough space for node stack. Increase MD_TREE_MAX_STACK_SIZE";
			break;
		}

		++node_idx;
	}

	append_token_char(writer, ']');
	append_token_char(writer, '(');

	const struct md_node_t *restrict link_node = tree->link + writer->link_idx;
	if (writer->link_idx < tree->link_count && tree->link_parent[writer->link_idx] == node_stack[0]) {
		++writer->link_idx;

		md_token_idx_t link_token_idx;
		for (
			link_token_idx = link_node->begin;
			link_token_idx < link_node->end;
			++link_token_idx) {

			append_token(writer, link_token_idx);
		}
	}

	/* Return the index of the last node processed, not the index of the next node to be processed.
	 * This is expected by the caller
	 */
	return node_idx - 1;
}

static size_t append_image(
	struct md_writer_t *restrict writer,
	const struct md_tree_t *restrict tree,
	const md_token_idx_t *restrict token_idx_table,
	size_t node_idx,
	md_node_idx_t begin)
{
	append_token(writer, token_idx_table[MD_TOKEN_EXCLAMATIONMARK]);
	node_idx = append_label(writer, tree, token_idx_table, node_idx);

	if (writer->tooltip_idx < tree->tooltip_count && tree->tooltip_parent[writer->tooltip_idx] == begin) {
		const struct md_node_t *restrict tooltip_node = tree->tooltip + writer->tooltip_idx;
		++writer->tooltip_idx;

		append_token(writer, token_idx_table[MD_TOKEN_SPACE]);
		append_token_char(writer, '\'');

		md_token_idx_t tooltip_token_idx;
		for (
			tooltip_token_idx = tooltip_node->begin;
			tooltip_token_idx < tooltip_node->end;
			++tooltip_token_idx) {

			// TODO: may need to escape single quotes in the tooltip
			append_token(writer, tooltip_token_idx);
		}

		append_token_char(writer, '\'');
	}

	append_token_char(writer, ')');
	return node_idx;
}

