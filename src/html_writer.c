// SPDX-License-Identifier: GPL-2.0-only
/*
 * html_writer.c
 *
 * Copyright (C) 2023  Imran Haider
 */

#include "html_writer.h"
#include "html_lexer.h"
#include "html_tree.h"
#include <stdio.h>
#include <assert.h>

#include <string.h>
#include <stdlib.h>

struct html_writer_t {
	const struct html_tokens_t *restrict tokens;
	utf32_t *restrict output;
	size_t current;
	size_t max_size;

	/* parsing error handling */
	const char *restrict exception_msg;

	/* flags */
	unsigned int exception_pending :1;
};

/* Write operations */
static void append_token(struct html_writer_t *restrict writer, html_token_idx_t token_idx);
static void append_token_oneline(
		struct html_writer_t *restrict writer, html_token_idx_t token_idx);
static void append_token_char(struct html_writer_t *restrict writer, uint32_t ch);
static void append_token_string(
		struct html_writer_t *restrict writer, utf32_t *restrict str, size_t size);

/* Debugging */
static_assert(
		1ull << sizeof(enum html_token_id_t) * 8 > HTML_TOKEN_END, "html_token_id_t is too small");
static_assert(
		1ull << sizeof(html_token_idx_t) * 8 > HTML_TREE_MAX_TOKENS,
		"html_token_idx_t is too small");
static_assert(
		1ull << sizeof(html_token_size_t) * 8 > HTML_TREE_MAX_TOKEN_SIZE,
		"html_token_size_t is too small");
static_assert(
		1ull << sizeof(html_token_off_t) * 8 > HTML_TREE_MAX_FILE_SIZE,
		"html_token_off_t is too small");


int html_write(
		utf32_t *restrict *restrict out_data, size_t *restrict out_size,
		const struct html_tree_t *restrict tree)
{
	const size_t keyword_count = 1;
	utf32_t *keyword_data[keyword_count];
	size_t keyword_size[keyword_count];
	int rc = 0;

	memset(keyword_size, 0, keyword_count * sizeof(size_t));

#define REGISTER_KEYWORD(id,str) unicode_read_ascii_string(str, sizeof(str)-1, keyword_data+id, keyword_size+id)
	REGISTER_KEYWORD(0, "true");

	/* create a lookup table to locate an instance of a token given its id */
	html_token_idx_t token_idx[HTML_TOKEN_END] = { 0 };
	const struct html_tokens_t *restrict tokens = &tree->tokens;
	const enum html_token_id_t *restrict id = tokens->id;
	html_token_idx_t idx;

	for (idx=0; idx < tokens->count; ++idx) {
		enum html_token_id_t token_id = id[idx];

		if (token_id < HTML_TOKEN_END) {
			token_idx[token_id] = idx;
		}
		else {
			fprintf(
				stderr, "html_write: token id %d (on index %d) is out of bounds\n", token_id, idx);
			rc = -1;
			goto done;
		}
	}

	/* node stack */
	html_token_idx_t node_stack[HTML_TREE_MAX_STACK_SIZE];
	size_t node_stack_size = 0;

	/* allocate memory for output data */
	*out_size = HTML_TREE_MAX_SIZE;
	*out_data = malloc(HTML_TREE_MAX_SIZE * sizeof(utf32_t));

	size_t node_idx, j;
	struct html_writer_t writer = {0};

	size_t attrib_idx = 0;
	size_t text_node_idx = 0;

	writer.tokens = tokens;
	writer.output = *out_data;
	writer.max_size = HTML_TREE_MAX_SIZE;

	/* TODO: these two flags here are 1-bit wide but they are taking 8-bits each. consolidate
	 * them somehow */

	uint8_t first;

	/* `has_text_nodes` is used to indicate that there was text after open tag, and so the
	 * the '>' of the open tag has been committed. we use this later to know if we should
	 * append the '>' before we start a new node or close the current one.
	 *
	 * The reason we don't append the '>' immediately when we write the open tag is because we
	 * don't know if the tag is a self-closing tag. we defer the append of '>' until we are sure.
	 */
	int8_t has_text_nodes;

	if (tree->node_count) {
		node_idx = 0;
		while (1) {

			first = 1;
			has_text_nodes = 0;

			html_token_idx_t tag_name = tree->node_tag_name[node_idx];
			html_token_idx_t parent = tree->node_parent[node_idx];
			html_token_idx_t prev_node = node_stack_size > 0 ? node_stack[node_stack_size-1] : -1;

			/* write text node immediately after an open tag */
			while (
					tree->text_parent[text_node_idx] == prev_node &&
					tree->text_prev[text_node_idx] == prev_node) {

				if (!has_text_nodes)
					append_token(&writer, token_idx[HTML_TOKEN_GREATERTHAN]);

				struct html_token_range_t range = tree->text[text_node_idx];
				for (html_token_idx_t i = range.begin; i < range.end; ++i) {
					enum html_token_id_t token_id = id[i];
					if (token_id == HTML_TOKEN_SINGLEQUOTE_STRING) {
						append_token(&writer, token_idx[HTML_TOKEN_SINGLEQUOTE]);
						append_token(&writer, i);
						append_token(&writer, token_idx[HTML_TOKEN_SINGLEQUOTE]);
					}
					else if (token_id == HTML_TOKEN_DOUBLEQUOTE_STRING) {
						append_token(&writer, token_idx[HTML_TOKEN_DOUBLEQUOTE]);
						append_token(&writer, i);
						append_token(&writer, token_idx[HTML_TOKEN_DOUBLEQUOTE]);
					}
					else {
						append_token(&writer, i);
					}
				}

				++text_node_idx;
				has_text_nodes = 1;
			}

			/* write closing tags if we moved to a sibling node or a parent node */
			while (node_stack_size > 0 && parent != node_stack[node_stack_size-1]) {
				--node_stack_size;

				if (first == 0) {
					/* sample output:
					 *		>\n
					 *		....</tag
					 */

					/* closing tag (ending) */
					append_token(&writer, token_idx[HTML_TOKEN_GREATERTHAN]);

					/* write text node between </tag1><tag2> */
					while (tree->text_parent[text_node_idx] == node_stack[node_stack_size] &&
						tree->text_prev[text_node_idx] == prev_node) {
						struct html_token_range_t range = tree->text[text_node_idx];
						for (html_token_idx_t i = range.begin; i < range.end; ++i) {
							enum html_token_id_t token_id = id[i];
							if (token_id == HTML_TOKEN_SINGLEQUOTE_STRING) {
								append_token(&writer, token_idx[HTML_TOKEN_SINGLEQUOTE]);
								append_token(&writer, i);
								append_token(&writer, token_idx[HTML_TOKEN_SINGLEQUOTE]);
							}
							else if (token_id == HTML_TOKEN_DOUBLEQUOTE_STRING) {
								append_token(&writer, token_idx[HTML_TOKEN_DOUBLEQUOTE]);
								append_token(&writer, i);
								append_token(&writer, token_idx[HTML_TOKEN_DOUBLEQUOTE]);
							}
							else {
								append_token(&writer, i);
							}
						}

						++text_node_idx;
					}

					append_token_char(&writer, '\n');

					/* indentation */
					for (j=0; j<node_stack_size; ++j) {
						append_token_char(&writer, '\t');
					}

					/* long-hand close tag (beginning) */
					append_token(&writer, token_idx[HTML_TOKEN_LESSTHAN]);
					append_token(&writer, token_idx[HTML_TOKEN_SLASH]);
					append_token(&writer, node_stack[node_stack_size]);
				}
				else if (has_text_nodes) {
					/* sample output:
					 *		</tag
					 */

					has_text_nodes = 0;

					/* long-hand close tag (beginning) */
					append_token(&writer, token_idx[HTML_TOKEN_LESSTHAN]);
					append_token(&writer, token_idx[HTML_TOKEN_SLASH]);
					append_token(&writer, node_stack[node_stack_size]);
				}
				else {
					html_token_idx_t top = node_stack[node_stack_size];
					enum html_token_id_t token_id = id[top];

					if (__builtin_expect(token_id == HTML_TOKEN_DOCTYPE, 0)) {
						/* do nothing */
					}
					else if (html_tree_token_is_self_closing(token_id)) {
						/* sample output:
						 *      ./
						 */

						/* TODO: Find out why closing a self-closing tag changes its
						 * behavior
						 */

#if 0
						/* short-hand close tag (beginning) */
						append_token_char(&writer, ' ');
						append_token(&writer, token_idx[HTML_TOKEN_SLASH]);
#endif
					}
					else {
						/* sample output:
						 *		></tag
						 */

						/* long-hand close tag (beginning) */
						append_token(&writer, token_idx[HTML_TOKEN_GREATERTHAN]);
						append_token(&writer, token_idx[HTML_TOKEN_LESSTHAN]);
						append_token(&writer, token_idx[HTML_TOKEN_SLASH]);
						append_token(&writer, top);
					}
				}

				first = 0;
			}

			if (node_idx > 0) {
				/* sample output:
				 *		>\n
				 *
				 */

				/* closing tag (ending) */
				if (!has_text_nodes)
					append_token(&writer, token_idx[HTML_TOKEN_GREATERTHAN]);

				/* write text node between </tag1><tag2> */
				while (tree->text_parent[text_node_idx] == parent &&
					tree->text_prev[text_node_idx] == prev_node) {

					struct html_token_range_t range = tree->text[text_node_idx];
					for (html_token_idx_t i = range.begin; i < range.end; ++i) {
						enum html_token_id_t token_id = id[i];
						if (token_id == HTML_TOKEN_SINGLEQUOTE_STRING) {
							append_token(&writer, token_idx[HTML_TOKEN_SINGLEQUOTE]);
							append_token(&writer, i);
							append_token(&writer, token_idx[HTML_TOKEN_SINGLEQUOTE]);
						}
						else if (token_id == HTML_TOKEN_DOUBLEQUOTE_STRING) {
							append_token(&writer, token_idx[HTML_TOKEN_DOUBLEQUOTE]);
							append_token(&writer, i);
							append_token(&writer, token_idx[HTML_TOKEN_DOUBLEQUOTE]);
						}
						else {
							append_token(&writer, i);
						}
					}

					++text_node_idx;
				}

				append_token_char(&writer, '\n');
			}

			/* check if we have any more nodes to print */
			if (node_idx >= tree->node_count) {
				break;
			}

			/* indentation */
			for (j=0; j<node_stack_size; ++j) {
				append_token_char(&writer, '\t');
			}

			/* open tag */
			append_token(&writer, token_idx[HTML_TOKEN_LESSTHAN]);
			append_token(&writer, tag_name);

			for (; tree->attrib_parent[attrib_idx] == tag_name; ++attrib_idx) {
				append_token_char(&writer, ' ');

				struct html_token_range_t attrib_name = tree->attrib_name[attrib_idx];
				for (j=attrib_name.begin; j < attrib_name.end; ++j) {
					append_token(&writer, j);
				}

				if (id[tag_name] != HTML_TOKEN_DOCTYPE) {
					append_token_char(&writer, '=');
					append_token_char(&writer, '"');

					if (tree->attrib_value[attrib_idx])
						append_token_oneline(&writer, tree->attrib_value[attrib_idx]);
					else
						append_token_string(&writer, keyword_data[0], keyword_size[0]);

					append_token_char(&writer, '"');
				}
			}

			node_stack[node_stack_size] = tag_name;

			if (node_stack_size < HTML_TREE_MAX_STACK_SIZE) {
				++node_stack_size;
			}
			else {
				writer.exception_pending = 1;
				writer.exception_msg =
					"Not enough space for node stack. Increase HTML_TREE_MAX_STACK_SIZE";
				break;
			}

			++node_idx;
		}
	}

	if (__builtin_expect(writer.exception_pending == 1, 0)) {
		fprintf(stderr, "html_write: '%s'\n", writer.exception_msg);
		rc = -1;
	}

done:
	unicode_utf32_string_free(keyword_data, keyword_count);
	*out_size = writer.current;

	return rc;
}

static void append_token(struct html_writer_t *restrict writer, html_token_idx_t token_idx)
{
	html_token_size_t size = writer->tokens->size[token_idx];

	if (__builtin_expect(writer->exception_pending == 1 || writer->max_size < size, 0)) {
		if (__builtin_expect(writer->exception_pending == 0, 0)) {
			writer->exception_msg = "Out of memory. Incrase HTML_TREE_MAX_SIZE";
			writer->exception_pending = 1;
		}
		return;
	}

	const utf32_t *begin = writer->tokens->base + writer->tokens->begin[token_idx];
	memcpy(writer->output + writer->current, begin, size * sizeof(utf32_t));
	writer->current += size;
	writer->max_size -= size;
}

static void append_token_oneline(
		struct html_writer_t *restrict writer, html_token_idx_t token_idx)
{
	html_token_size_t size = writer->tokens->size[token_idx];

	if (__builtin_expect(writer->exception_pending == 1 || writer->max_size < size, 0)) {
		if (__builtin_expect(writer->exception_pending == 0, 0)) {
			writer->exception_msg = "Out of memory. Incrase HTML_TREE_MAX_SIZE";
			writer->exception_pending = 1;
		}
		return;
	}

	const utf32_t *begin = writer->tokens->base + writer->tokens->begin[token_idx];

	html_token_size_t i;
	for (i=0; i < size; ++i) {
		utf32_t ch = begin[i];
		if (ch != '\n' && ch != '\r')
			writer->output[writer->current + i] = ch;
		else
			writer->output[writer->current + i] = ' ';
	}

	writer->current += size;
	writer->max_size -= size;
}

static void append_token_char(struct html_writer_t *restrict writer, uint32_t ch)
{
	if (__builtin_expect(writer->exception_pending == 1 || writer->max_size < 1, 0)) {
		if (__builtin_expect(writer->exception_pending == 0, 0)) {
			writer->exception_msg = "Out of memory. Incrase HTML_TREE_MAX_SIZE";
			writer->exception_pending = 1;
		}
		return;
	}

	writer->output[writer->current] = ch;
	++writer->current;
	--writer->max_size;
}

static void append_token_string(
		struct html_writer_t *restrict writer, utf32_t *restrict str, size_t size)
{
	if (__builtin_expect(writer->exception_pending == 1 || writer->max_size < size, 0)) {
		if (__builtin_expect(writer->exception_pending == 0, 0)) {
			writer->exception_msg = "Out of memory. Incrase HTML_TREE_MAX_SIZE";
			writer->exception_pending = 1;
		}
		return;
	}

	memcpy(writer->output + writer->current, str, size * sizeof(utf32_t));
	writer->current += size;
	writer->max_size -= size;
}


