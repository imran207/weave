/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * html_builder.c
 *
 * Copyright (C) 2023  Imran Haider
 */

#include "html_builder.h"
#include "html_tree.h"
#include "unicode.h"
#include <string.h>
#include <stdio.h>

static html_token_idx_t add_node(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		enum html_token_id_t id,
		const char *tag_name,
		size_t tag_length);

static void add_attribute(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		const char *attrib,
		size_t attrib_length,
		html_token_idx_t value_token);

static html_token_idx_t insert_token_ascii(
		struct html_builder_t *restrict builder,
		enum html_token_id_t id,
		const char *str,
		html_token_size_t len);

#if 0
static html_token_idx_t insert_token_unicode(
		struct html_builder_t *restrict builder,
		enum html_token_id_t id,
		const utf32_t *str,
		html_token_size_t len);
#endif

static html_token_idx_t last_child_of_parent(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);


void html_builder_init(
		struct html_builder_t *restrict builder,
		struct html_tree_t *restrict tree,
		utf32_t *base,
		size_t buffer_size)
{
	memset(tree, 0, sizeof(struct html_tree_t));
	tree->tokens.base = base;
	tree->tokens.buffer_size = buffer_size;

	/* explanation on why this is set to `1` instead of `0` is mentioned
	 * in the `insert_token_*` functions.
	 */
	tree->tokens.count = 1;

	builder->tree = tree;

	/* we need at least one of every static token to appear in the token buffer. in case
	 * it doesn't appear because the source content simply doesn't have it, we'll insert
	 * a sample token for each type
	 */
	insert_token_ascii(builder, HTML_TOKEN_GREATERTHAN, ">", 1);
	insert_token_ascii(builder, HTML_TOKEN_LESSTHAN, "<", 1);
	insert_token_ascii(builder, HTML_TOKEN_WHITESPACE, " ", 1);
	insert_token_ascii(builder, HTML_TOKEN_EQUAL, "=", 1);
	insert_token_ascii(builder, HTML_TOKEN_SINGLEQUOTE, "'", 1);
	insert_token_ascii(builder, HTML_TOKEN_DOUBLEQUOTE, "\"", 1);
	insert_token_ascii(builder, HTML_TOKEN_AMPERSAND, "&", 1);
	insert_token_ascii(builder, HTML_TOKEN_EXCLAMATIONMARK, "!", 1);
	insert_token_ascii(builder, HTML_TOKEN_QUESTIONMARK, "?", 1);
	insert_token_ascii(builder, HTML_TOKEN_HYPHEN, "-", 1);
	insert_token_ascii(builder, HTML_TOKEN_COLON, ":", 1);
	insert_token_ascii(builder, HTML_TOKEN_OPENBRACE, "{", 1);
	insert_token_ascii(builder, HTML_TOKEN_CLOSEBRACE, "}", 1);
	insert_token_ascii(builder, HTML_TOKEN_OPENPAREN, "(", 1);
	insert_token_ascii(builder, HTML_TOKEN_CLOSEPAREN, ")", 1);
	insert_token_ascii(builder, HTML_TOKEN_SEMICOLON, ";", 1);
	insert_token_ascii(builder, HTML_TOKEN_ASTERISK, "*", 1);
	insert_token_ascii(builder, HTML_TOKEN_HASH, "#", 1);
	insert_token_ascii(builder, HTML_TOKEN_COMMA, ",", 1);
	insert_token_ascii(builder, HTML_TOKEN_SLASH, "/", 1);
	insert_token_ascii(builder, HTML_TOKEN_HTML, "html", 4);
	insert_token_ascii(builder, HTML_TOKEN_META, "meta", 4);
	insert_token_ascii(builder, HTML_TOKEN_LINK, "link", 4);
	insert_token_ascii(builder, HTML_TOKEN_IMG, "img", 3);
	insert_token_ascii(builder, HTML_TOKEN_BR, "br", 3);
	insert_token_ascii(builder, HTML_TOKEN_INPUT, "input", 5);
	insert_token_ascii(builder, HTML_TOKEN_DOCTYPE, "!DOCTYPE", 8);
	insert_token_ascii(builder, HTML_TOKEN_DATA, "data", 4);
	insert_token_ascii(builder, HTML_TOKEN_SCRIPT, "script", 6);
	insert_token_ascii(builder, HTML_TOKEN_STYLE, "style", 5);
	insert_token_ascii(builder, HTML_TOKEN_INCLUDE, "include", 7);
}

html_token_idx_t html_builder_allocate_token(struct html_builder_t *builder)
{
	struct html_tokens_t *restrict tokens = &(builder->tree->tokens);
	html_token_idx_t idx = tokens->count;
	++tokens->count;

	/* compute the offset in the buffer where we can append the new token.
	 * note that we don't check if `idx` is greater than 0 because the first
	 * array index starts at value `1`. this is set in the `html_builder_init`
	 * function.
	 */
	tokens->begin[idx] = tokens->begin[idx-1] + tokens->size[idx-1];
	tokens->size[idx] = 0;
	tokens->id[idx] = HTML_TOKEN_IDENTIFIER;

	return idx;
}

void html_builder_append_last_token(
		struct html_builder_t *builder,
		const utf32_t *src,
		size_t len)
{
	struct html_tokens_t *restrict tokens = &(builder->tree->tokens);
	html_token_idx_t idx = tokens->count-1;

	html_token_off_t offset = tokens->begin[idx] + tokens->size[idx];
	utf32_t *dest = tokens->base + offset;

	memcpy(dest, src, len * sizeof(utf32_t));
	tokens->size[idx] += len;
}

html_token_idx_t html_builder_add_header(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		int level)
{
	char tag_name[3] = "h-";
	tag_name[1] = level + '0';

	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, tag_name, 2);
}

void html_builder_add_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token)
{
	html_token_idx_t after = last_child_of_parent(builder, parent);

	struct html_token_range_t text_range_ref = {
		text_token, text_token + 1
	};

	struct html_tree_t *restrict tree = builder->tree;
	const size_t text_idx = tree->text_count;
	++tree->text_count;

	tree->text_parent[text_idx] = parent;
	tree->text_prev[text_idx] = after? after : parent;
	tree->text[text_idx] = text_range_ref;
}

html_token_idx_t html_builder_add_italic_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token)
{
	struct html_token_range_t text_range_ref = {
		text_token, text_token + 1
	};

	struct html_tree_t *restrict tree = builder->tree;
	const size_t text_idx = tree->text_count;
	++tree->text_count;

	html_token_idx_t node = add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "em", 2);
	tree->text_parent[text_idx] = node;
	tree->text_prev[text_idx] = node;
	tree->text[text_idx] = text_range_ref;

	return node;
}

html_token_idx_t html_builder_add_bold_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token)
{
	struct html_token_range_t text_range_ref = {
		text_token, text_token + 1
	};

	struct html_tree_t *restrict tree = builder->tree;
	const size_t text_idx = tree->text_count;
	++tree->text_count;

	html_token_idx_t node = add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "strong", 6);
	tree->text_parent[text_idx] = node;
	tree->text_prev[text_idx] = node;
	tree->text[text_idx] = text_range_ref;

	return node;
}

html_token_idx_t html_builder_add_bold_italic_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token)
{
	struct html_token_range_t text_range_ref = {
		text_token, text_token + 1
	};

	struct html_tree_t *restrict tree = builder->tree;
	const size_t text_idx = tree->text_count;
	++tree->text_count;

	html_token_idx_t bold_node = add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "strong", 6);
	html_token_idx_t node = add_node(builder, bold_node, HTML_TOKEN_IDENTIFIER, "em", 2);
	tree->text_parent[text_idx] = node;
	tree->text_prev[text_idx] = node;
	tree->text[text_idx] = text_range_ref;

	return node;
}

html_token_idx_t html_builder_add_code_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token)
{
	struct html_token_range_t text_range_ref = {
		text_token, text_token + 1
	};

	struct html_tree_t *restrict tree = builder->tree;
	const size_t text_idx = tree->text_count;
	++tree->text_count;

	html_token_idx_t node = add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "code", 4);
	tree->text_parent[text_idx] = node;
	tree->text_prev[text_idx] = node;
	tree->text[text_idx] = text_range_ref;

	return node;
}

html_token_idx_t html_builder_add_div(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "div", 3);
}

html_token_idx_t html_builder_add_linebreak(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_BR, "br", 2);
}

void html_builder_add_linebreak_escaped(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	html_token_idx_t text_token = insert_token_ascii(builder, HTML_TOKEN_TEXT, "&lt;br&gt;", 10);
	html_builder_add_text(builder, parent, text_token);
}

html_token_idx_t html_builder_add_ordered_list(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "ol", 2);
}

html_token_idx_t html_builder_add_unordered_list(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "ul", 2);
}

html_token_idx_t html_builder_add_list_item(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "li", 2);
}

html_token_idx_t html_builder_add_blockquote(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "blockquote", 10);
}

html_token_idx_t html_builder_add_codeblock(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	html_token_idx_t pre_node = add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "pre", 3);
	return add_node(builder, pre_node, HTML_TOKEN_IDENTIFIER, "code", 4);
}

html_token_idx_t html_builder_add_horizontalrule(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "hr", 2);
}

html_token_idx_t html_builder_add_table(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "table", 5);
}

html_token_idx_t html_builder_add_tablerow(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "tr", 2);
}

html_token_idx_t html_builder_add_tablecell(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	return add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "td", 2);
}

html_token_idx_t html_builder_add_image(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t img_token,
		html_token_idx_t alt_text_token)
{
	html_token_idx_t node = add_node(builder, parent, HTML_TOKEN_IMG, "img", 3);

	add_attribute(builder, node, "src", 3, img_token);
	add_attribute(builder, node, "alt", 3, alt_text_token);

	return node;
}

html_token_idx_t html_builder_add_image_with_tooltip(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t img_token,
		html_token_idx_t alt_text_token,
		html_token_idx_t tooltip_token)
{
	html_token_idx_t node = add_node(builder, parent, HTML_TOKEN_IMG, "img", 3);

	add_attribute(builder, node, "src", 3, img_token);
	add_attribute(builder, node, "alt", 3, alt_text_token);
	add_attribute(builder, node, "title", 5, tooltip_token);

	return node;
}

html_token_idx_t html_builder_add_link(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t url_token)
{
	html_token_idx_t node = add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "a", 1);

	add_attribute(builder, node, "href", 4, url_token);

	return node;
}

html_token_idx_t html_builder_add_link_with_tooltip(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t url_token,
		html_token_idx_t tooltip_token)
{
	html_token_idx_t node = add_node(builder, parent, HTML_TOKEN_IDENTIFIER, "a", 1);

	add_attribute(builder, node, "href", 4, url_token);
	add_attribute(builder, node, "title", 5, tooltip_token);

	return node;
}

static html_token_idx_t add_node(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		enum html_token_id_t id,
		const char *tag_name,
		size_t tag_length)
{
	html_token_idx_t tag_ref = insert_token_ascii(builder, id, tag_name, tag_length);

	struct html_tree_t *restrict tree = builder->tree;
	const size_t node_idx = tree->node_count;
	++tree->node_count;

	tree->node_parent[node_idx] = parent;
	tree->node_tag_name[node_idx] = tag_ref; 

	return tag_ref;
}

static void add_attribute(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		const char *attrib,
		size_t attrib_length,
		html_token_idx_t value_token)
{
    html_token_idx_t name_token =
	    insert_token_ascii(builder, HTML_TOKEN_IDENTIFIER, attrib, attrib_length);

	struct html_token_range_t name_range_ref = {
		name_token, name_token + 1
	};

	struct html_tree_t *restrict tree = builder->tree;
	const size_t attrib_idx = tree->attrib_count;
	++tree->attrib_count;

	tree->attrib_parent[attrib_idx] = parent;
	tree->attrib_name[attrib_idx] = name_range_ref; 
	tree->attrib_value[attrib_idx] = value_token; 
}

static html_token_idx_t insert_token_ascii(
		struct html_builder_t *restrict builder,
		enum html_token_id_t id,
		const char *str,
		html_token_size_t len)
{
	struct html_tokens_t *restrict tokens = &(builder->tree->tokens);
	html_token_idx_t idx = tokens->count;

	/* compute the offset in the buffer where we can append the new token.
	 * note that we don't check if `idx` is greater than 0 because the first
	 * array index starts at value `1`. this is set in the `html_builder_init`
	 * function.
	 */
	html_token_off_t offset = tokens->begin[idx-1] + tokens->size[idx-1];

	if (offset + len < tokens->buffer_size) {
		++tokens->count;
		utf32_t *dest = tokens->base + offset;

		/* write string into buffer */
		for (int i=0; i < len; ++i) {
			dest[i] = unicode_read_ascii_char(&str);
		}

		tokens->begin[idx] = offset;
		tokens->size[idx] = len;
		tokens->id[idx] = id;
	}
	else {
		/* using return value of `0` as an error status */
		idx = 0;
	}

	return idx;
}

#if 0
static html_token_idx_t insert_token_unicode(
		struct html_builder_t *restrict builder,
		enum html_token_id_t id,
		const utf32_t *str,
		html_token_size_t len)
{
	struct html_tokens_t *restrict tokens = &(builder->tree->tokens);
	html_token_idx_t idx = tokens->count;

	/* compute the offset in the buffer where we can append the new token.
	 * note that we don't check if `idx` is greater than 0 because the first
	 * array index starts at value `1`. this is set in the `html_builder_init`
	 * function.
	 */
	html_token_off_t offset = tokens->begin[idx-1] + tokens->size[idx-1];

	if (offset + len < tokens->buffer_size) {
		++tokens->count;
		utf32_t *dest = tokens->base + offset;

		/* write string into buffer */
		for (int i=0; i < len; ++i) {
			dest[i] = str[i];
		}

		tokens->begin[idx] = offset;
		tokens->size[idx] = len;
		tokens->id[idx] = id;
	}
	else {
		/* using return value of `0` as an error status */
		idx = 0;
	}

	return idx;
}
#endif

static html_token_idx_t last_child_of_parent(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent)
{
	struct html_tree_t *restrict tree = builder->tree;
	size_t i;
	html_token_idx_t idx = 0;

	for (i = tree->node_count - 1; i > 1; --i) {
		if (tree->node_parent[i] == parent) {
			idx = tree->node_tag_name[i];
			break;
		}
	}

	return idx;
}


