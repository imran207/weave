/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * html_builder.h
 *
 * Copyright (C) 2023  Imran Haider
 */

#ifndef HTML_BUILDER_H
#define HTML_BUILDER_H

#include "html_tree.h"
#include <wchar.h>

struct html_builder_t {
	/* parse tree */
	struct html_tree_t *restrict tree;
};

void html_builder_init(
		struct html_builder_t *builder,
		struct html_tree_t *restrict tree,
		utf32_t *base,
		size_t count);

html_token_idx_t html_builder_allocate_token(struct html_builder_t *builder);

void html_builder_append_last_token(
		struct html_builder_t *builder,
		const utf32_t *data,
		size_t len);

html_token_idx_t html_builder_add_header(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		int level);

void html_builder_add_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token);

void html_builder_add_text_escaped(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token);

html_token_idx_t html_builder_add_italic_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token);

html_token_idx_t html_builder_add_bold_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token);

html_token_idx_t html_builder_add_bold_italic_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token);

html_token_idx_t html_builder_add_code_text(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t text_token);

html_token_idx_t html_builder_add_div(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_linebreak(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

void html_builder_add_linebreak_escaped(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_ordered_list(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_unordered_list(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_list_item(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_blockquote(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_codeblock(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_horizontalrule(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_table(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_tablerow(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_tablecell(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent);

html_token_idx_t html_builder_add_image(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t img_token,
		html_token_idx_t alt_text_token);

html_token_idx_t html_builder_add_image_with_tooltip(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t img_token,
		html_token_idx_t alt_text_token,
		html_token_idx_t tooltip_token);

html_token_idx_t html_builder_add_link(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t url_token);

html_token_idx_t html_builder_add_link_with_tooltip(
		struct html_builder_t *restrict builder,
		html_token_idx_t parent,
		html_token_idx_t url_token,
		html_token_idx_t tooltip_token);

#endif

