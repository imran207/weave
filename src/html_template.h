/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * html_template.h
 *
 * Copyright (C) 2021  Imran Haider
 */

#ifndef HTML_TEMPLATE_H
#define HTML_TEMPLATE_H

#include "html_parser.h"

void html_template_render(int cwd_fd, struct html_tree_t *tree);

#endif

