/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * html_tree.h
 *
 * Copyright (C) 2023  Imran Haider
 */

#ifndef HTML_TREE_H
#define HTML_TREE_H

#include "unicode.h"
#include <stddef.h>
#include <stdint.h>


#define HTML_TREE_MAX_TOKENS       0x40000
#define HTML_TREE_MAX_NODES        0x800
#define HTML_TREE_MAX_ATTRIBUTES   0x2000
#define HTML_TREE_MAX_TEXTS        0x400
#define HTML_TREE_MAX_SIZE         0x200000       /* in characters */
#define HTML_TREE_MAX_STACK_SIZE   0x100
#define HTML_TREE_MAX_FILE_SIZE    0xffffffff
#define HTML_TREE_MAX_TOKEN_SIZE   0xffff

enum html_token_id_t {
	HTML_TOKEN_GREATERTHAN,         /* > */
	HTML_TOKEN_LESSTHAN,            /* < */
	HTML_TOKEN_IDENTIFIER,
	HTML_TOKEN_WHITESPACE,          /*   */
	HTML_TOKEN_EQUAL,               /* = */
	HTML_TOKEN_SINGLEQUOTE,         /* ' */
	HTML_TOKEN_DOUBLEQUOTE,         /* " */
	HTML_TOKEN_AMPERSAND,           /* & */
	HTML_TOKEN_EXCLAMATIONMARK,     /* ! */
	HTML_TOKEN_QUESTIONMARK,        /* ? */
	HTML_TOKEN_HYPHEN,              /* - */
	HTML_TOKEN_COLON,               /* : */
	HTML_TOKEN_OPENBRACE,           /* { */
	HTML_TOKEN_CLOSEBRACE,          /* } */
	HTML_TOKEN_OPENPAREN,           /* ( */
	HTML_TOKEN_CLOSEPAREN,          /* ) */
	HTML_TOKEN_SEMICOLON,           /* ; */
	HTML_TOKEN_ASTERISK,            /* * */
	HTML_TOKEN_HASH,                /* # */
	HTML_TOKEN_COMMA,               /* , */
	HTML_TOKEN_SLASH,               /* / */
	HTML_TOKEN_HTML,

	/* Tokens for self-closing tag names */
	HTML_TOKEN_SELFCLOSING_BEGIN,
	HTML_TOKEN_META,
	HTML_TOKEN_LINK,
	HTML_TOKEN_IMG,
	HTML_TOKEN_BR,
	HTML_TOKEN_INPUT,
	HTML_TOKEN_DOCTYPE,
	HTML_TOKEN_SELFCLOSING_END,

	HTML_TOKEN_DATA,
	HTML_TOKEN_SCRIPT,
	HTML_TOKEN_SINGLEQUOTE_STRING,
	HTML_TOKEN_DOUBLEQUOTE_STRING,
	HTML_TOKEN_TEXT,
	HTML_TOKEN_COMMENT,
	HTML_TOKEN_STYLE,
	HTML_TOKEN_INCLUDE,
	HTML_TOKEN_END
} __attribute__ ((__packed__));

/* Must be less than HTML_TREE_MAX_TOKENS */
typedef uint32_t html_token_idx_t;

/* Must be less than HTML_TREE_MAX_FILE_SIZE */
typedef uint32_t html_token_off_t;

/* Must be less than HTML_TREE_MAX_TOKEN_SIZE */
typedef uint16_t html_token_size_t;

/* Struct to hold all lexical tokens */
struct html_tokens_t {
	html_token_off_t begin[HTML_TREE_MAX_TOKENS];
	html_token_size_t size[HTML_TREE_MAX_TOKENS];
	enum html_token_id_t id[HTML_TREE_MAX_TOKENS];
	utf32_t *base;
	size_t buffer_size;     /* size of the token buffer */
	size_t count;           /* number of tokens */
};

/* Struct to represent a range of tokens for a single string */
struct html_token_range_t {
	html_token_idx_t begin;
	html_token_idx_t end;
};

/* Struct to hold the parse tree */
struct html_tree_t {
	struct html_tokens_t tokens;

	/* the node and a reference to its parent node */
	html_token_idx_t node_parent[HTML_TREE_MAX_NODES];
	html_token_idx_t node_tag_name[HTML_TREE_MAX_NODES];

	/* the attribute name:value and a reference to the owner node */
	html_token_idx_t attrib_parent[HTML_TREE_MAX_ATTRIBUTES];
	struct html_token_range_t attrib_name[HTML_TREE_MAX_ATTRIBUTES];
	html_token_idx_t attrib_value[HTML_TREE_MAX_ATTRIBUTES];

	/* the text and a reference to the owner node and the previous sibling node */
	html_token_idx_t text_parent[HTML_TREE_MAX_TEXTS];
	html_token_idx_t text_prev[HTML_TREE_MAX_TEXTS];
	struct html_token_range_t text[HTML_TREE_MAX_TEXTS];

	size_t attrib_count;
	size_t node_count;
	size_t text_count;
};

static inline int html_tree_token_is_self_closing(enum html_token_id_t id) {
	return HTML_TOKEN_SELFCLOSING_BEGIN < id && HTML_TOKEN_SELFCLOSING_END > id;
}

int html_tree_append(
		struct html_tree_t *restrict dest,
		struct html_tree_t *restrict src);

#endif

