/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * md_lexer.h
 *
 * Copyright (C) 2021  Imran Haider
 */

#ifndef MD_LEXER_H
#define MD_LEXER_H

#include "md_parser.h"
#include "unicode.h"

#include <stddef.h>
#include <stdint.h>

int md_lex(
		const utf32_t *restrict in_data, size_t in_size, struct md_tokens_t *restrict tokens);

#endif

