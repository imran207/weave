// SPDX-License-Identifier: GPL-2.0-only
/*
 * weave_convert.c
 *
 * Copyright (C) 2021  Imran Haider
 */

#include "unicode.h"
#include "weave_rewrite.h"
#include "md_parser.h"
#include "html_parser.h"
#include "html_writer.h"
#include "markup_convert.h"

#include <getopt.h>
#include <ctype.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h> 

static const char *get_extension(const char *restrict fn);
static int open_cwd(int *restrict fd);
static int convert_md_html(
		int cwd_fd,
		const char *restrict input_fn,
		const char *restrict output_fn,
		unsigned char verbose);

int main(int argc, char **argv)
{
	const char *output = 0;
	const char *input;
	unsigned char is_verbose = 0;
	unsigned char is_help_mode = 0;

	int c, cwd_fd, rc = 0;

	/* get command line options */
	static struct option long_options[] = {
		{ "output",  required_argument, NULL, 'o' },	/* output directory */
		{ "verbose", no_argument,       NULL, 'v' },	/* enable verbosity */
		{ "help",    no_argument,       NULL, 'h' },	/* help */
		{ NULL,      0,                 NULL, 0   }
	};

	int option_index = 0;
	while ((c = getopt_long(argc, argv, "vho:", long_options, &option_index)) != -1) {
		switch (c) {
		case 'o':
			output = optarg;
			break;
		case 'v':
			is_verbose = 1;
			break;
		case 'h':
			is_help_mode = 1;
			break;
		case '?':
			if (optopt == 'o')
				fputs("Option -o requires an argument.\n", stderr);

			else if (isprint(optopt))
				fprintf(stderr, "Unknown option '-%c'.\n", optopt);

			return -1;
		default:
			return -1;
		}
	}

	/* print the help information if --help is specified */
	if (is_help_mode) {
		fputs(
			"Usage: weave-convert [options] file...\n"
			"Options:\n"
			"  -o        The output file\n"
			"  -v        Enable verbose mode\n"
			"  -h        Show this help page\n"
			"\n", stdout);

		return 0;
	}

	/* make sure the correct number of input and output files were specified */
	if (__builtin_expect(argc - optind < 1, 0)) {
		fputs("no input file\n", stderr);
		return -1;
	}

	if (__builtin_expect(argc - optind > 1, 0)) {
		fputs("too many input files\n", stderr);
		return -1;
	}

	if (__builtin_expect(output == 0, 0)) {
		fputs("output file not specified\n", stderr);
		return -1;
	}

	/* collect the input filenames into `input` */
	input = argv[optind];

	/* open the current directory */
	rc = open_cwd(&cwd_fd);
	if (__builtin_expect(rc != 0, 0))
		goto exit1;

	const char *input_ext = get_extension(input);
	const char *output_ext = get_extension(output);

	if (__builtin_expect(input_ext == 0, 0)) {
		fputs("input file doesn't have an extension", stderr);
		rc = -EINVAL;
		goto exit1;
	}

	if (__builtin_expect(output_ext == 0, 0)) {
		fputs("output file doesn't have an extension", stderr);
		rc = -EINVAL;
		goto exit1;
	}

	if (!strcasecmp(input_ext, "md")) {
		if (!strcasecmp(output_ext, "html")) {
			rc = convert_md_html(cwd_fd, input, output, is_verbose);
		}
		else if (!strcasecmp(output_ext, "md")) {
			rc = rewrite_md(cwd_fd, input, output, is_verbose);
		}
		else {
			fputs("invalid output file extension", stderr);
			rc = -EINVAL;
		}
	}
	else if (!strcasecmp(input_ext, "html")) {
		if (!strcasecmp(output_ext, "html")) {
			rc = rewrite_html(cwd_fd, input, output, is_verbose);
		}
		else if (!strcasecmp(output_ext, "md")) {
			/* It's not always possible to transform a HTML file to a markdown
			 * file without information loss. For this reason, we simply do
			 * not support the operation.
			 */
			fputs("cannot convert an HTML file to a MD file", stderr);
			rc = -EINVAL;
		}
		else {
			fputs("invalid output file extension", stderr);
			rc = -EINVAL;
		}
	}
	else {
		fputs("invalid input file extension", stderr);
		rc = -EINVAL;
	}

	close(cwd_fd);

exit1:
	return rc;
}

static const char *get_extension(const char *restrict fn)
{
	const char *ext = strrchr(fn, '.');
	if (ext)
		++ext;

	return ext;
}

static int open_cwd(int *restrict fd)
{
	int rc;
	*fd = open(".", 0);

	if (__builtin_expect(*fd == -1, 0)) {
		rc = errno;
		fprintf(stderr, "cannot open current working directory. error %d\n", rc);
		return rc;
	}

	return 0;
}

static int convert_md_html(
		int cwd_fd,
		const char *restrict input_fn,
		const char *restrict output_fn,
		unsigned char verbose)
{
	int rc = 0;
	utf32_t *input_data;
	size_t input_size = 0;

	/* read input file as utf-8 and store the data in 'input_data' as utf-32 */
	rc = unicode_read_utf8_file(cwd_fd, input_fn, &input_data, &input_size);
	if (__builtin_expect(rc != 0, 0))
		return -1;

	/* allocate memory for the markdown and the HTML trees */
	struct md_tree_t *md_tree;
	struct html_tree_t *html_tree;

	if ((md_tree = malloc(sizeof(struct md_tree_t))) == 0) {
		fprintf(stderr, "not enough memory\n");
		rc = -1;
		goto exit1;
	}

	if ((html_tree = malloc(sizeof(struct html_tree_t))) == 0) {
		fprintf(stderr, "not enough memory\n");
		rc = -1;
		goto exit2;
	}

	if (verbose) {
		printf("\ntoken table:\n");
		md_trace_token_table(input_data, input_size);
	}

	/* parse the input data into `md_tree` */
	memset(md_tree, 0, sizeof(struct md_tree_t));
	md_parse(input_data, input_size, md_tree);

	if (verbose) {
		printf("\nmarkdown node table:\n");
		md_trace_node_table(md_tree);

		printf("\nmarkdown link table:\n");
		md_trace_link_table(md_tree);
	}

	/* convert the markdown to HTML */
	utf32_t *token_buffer;
	size_t token_buffer_size = 1024 * 1024;

	if ((token_buffer = malloc(sizeof(utf32_t) * token_buffer_size)) == 0) {
		fprintf(stderr, "not enough memory\n");
		rc = -1;
		goto exit3;
	}
	memset(html_tree, 0, sizeof(struct html_tree_t));
	memset(token_buffer, 0, token_buffer_size * sizeof(utf32_t));

	rc = markup_convert_md_html(html_tree, md_tree, token_buffer, token_buffer_size);
	if (__builtin_expect(rc != 0, 0))
		goto exit4;

	if (verbose) {
		printf("\t\t\t\t\t HTML\n");
		printf("\t\t\t\t\t======\n");

		html_dump_parse_table(html_tree, 0);
	}

	/* write the HTML tree to a string */
	utf32_t *output = 0;
	size_t output_size = 0;

	rc = html_write(&output, &output_size, html_tree);
	if (__builtin_expect(rc != 0, 0))
		goto exit4;

	/* save the HTML string into the destination file */
	rc = unicode_write_utf8_file(cwd_fd, output_fn, output, output_size);
	unicode_write_utf8_file(cwd_fd, "tokens.txt", token_buffer, token_buffer_size);
	free(output);

exit4:
	free(token_buffer);
exit3:
	free(html_tree);
exit2:
	free(md_tree);
exit1:

	return rc;
}

