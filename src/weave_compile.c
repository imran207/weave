/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * weave_compile.c
 *
 * Copyright (C) 2022  Imran Haider
 */

#include "weave_compile.h"
#include "html_parser.h"
#include "html_writer.h"
#include "html_template.h"
#include "unicode.h"
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>

static int prepare_output(int cwd_fd, const char *output, int *restrict out_fd);
static int compile_data(
		int cwd_fd,
		const int *restrict input,
		size_t size,
		int out_fd,
		unsigned char verbose);
static int write_data(int out_fd, unsigned char idx, const int *data, size_t size);

int compile_main(
		int cwd_fd,
		const char *restrict input,
		const char *restrict output,
		const char *restrict *restrict include_dirs,
		unsigned short n_include_dirs,
		unsigned char verbose)
{
	int rc, out_fd;
	utf32_t *input_data;
	size_t input_size = 0;

	/* read input file as utf-8 and store the data in 'input_data' as utf-32 */
	rc = unicode_read_utf8_file(cwd_fd, input, &input_data, &input_size);
	if (__builtin_expect(rc != 0, 0))
		goto exit1;

	/* prepare the output directory that will contain all generated files */
	rc = prepare_output(cwd_fd, output, &out_fd);
	if (__builtin_expect(rc != 0, 0))
		goto exit2;

	/* perform the actual compiling task */
	rc = compile_data(cwd_fd, input_data, input_size, out_fd, verbose);

	/* clean up */
	close(out_fd);
exit2:
	unicode_utf32_string_free(&input_data, 1);
exit1:
	return rc;
}

static int prepare_output(int cwd_fd, const char *output, int *restrict out_fd)
{
	int rc = 0;
	struct stat st = {0};

	if (stat(output, &st) == -1) {
		if (__builtin_expect(mkdir(output, 0755) == -1, 0)) {
			rc = errno;
			fprintf(stderr, "cannot create directory '%s'. error %d\n", output, rc);
			goto exit1;
		}
	}

	*out_fd = openat(cwd_fd, output, 0);
	if (__builtin_expect(*out_fd == -1, 0)) {
		rc = errno;
		fprintf(stderr, "cannot open '%s'. error %d\n", output, rc);
	}

exit1:
	return rc;
}

static int compile_data(
		int cwd_fd,
		const utf32_t *restrict input,
		size_t size,
		int out_fd,
		unsigned char verbose)
{
	struct html_tree_t *tree;

	if ((tree = malloc(sizeof(struct html_tree_t))) == 0) {
		fprintf(stderr, "not enough memory\n");
		return -1;
	}

	memset(tree, 0, sizeof(struct html_tree_t));
	html_parse(input, size, tree, verbose);

	html_template_render(cwd_fd, tree);

	utf32_t *output;
	size_t output_size = 0;
	html_write(&output, &output_size, tree);

	/* FIXME: for debugging only */
	write_data(out_fd, 0, output, output_size);

	free(output);
	free(tree);

	return 0;
}

static int write_data(int out_fd, unsigned char idx, const int *data, size_t size)
{
	char filename[16];
	snprintf(filename, sizeof(filename), "%d.html", (int) idx);
	return unicode_write_utf8_file(out_fd, filename, data, size);
}


