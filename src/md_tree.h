/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * md_tree.h
 *
 * Copyright (C) 2023  Imran Haider
 */

#ifndef MD_TREE_H
#define MD_TREE_H

#include "unicode.h"

#define MD_TREE_MAX_TOKENS            0x40000
#define MD_TREE_MAX_NODES             0x800
#define MD_TREE_MAX_SIZE              0x200000       /* in characters */
#define MD_TREE_MAX_LINKS             0x80
#define MD_TREE_MAX_TOOLTIPS          0x10
#define MD_TREE_MAX_STACK_SIZE        0x100
#define MD_TREE_MAX_FILE_SIZE         0xffffffff
#define MD_TREE_MAX_TOKEN_SIZE        0xffff
#define MD_TREE_MAX_TABLE_COLUMNS     0x2

enum md_token_id_t {
	MD_TOKEN_HASH,              /* # */
	MD_TOKEN_ASTERISK,          /* * */
	MD_TOKEN_PLUS,              /* + */
	MD_TOKEN_OPENBRACKET,       /* [ */
	MD_TOKEN_CLOSEBRACKET,      /* ] */
	MD_TOKEN_OPENPAREN,         /* ( */
	MD_TOKEN_CLOSEPAREN,        /* ) */
	MD_TOKEN_OPENBRACE,         /* { */
	MD_TOKEN_CLOSEBRACE,        /* } */
	MD_TOKEN_HYPHEN,            /* - */
	MD_TOKEN_SPACE,             /*   */
	MD_TOKEN_TAB,
	MD_TOKEN_NEWLINE,
	MD_TOKEN_LESSTHAN,          /* < */
	MD_TOKEN_GREATERTHAN,       /* > */
	MD_TOKEN_EQUAL,             /* = */
	MD_TOKEN_TILDE,             /* ~ */
	MD_TOKEN_EXCLAMATIONMARK,   /* ! */
	MD_TOKEN_BACKTICK,          /* ` */
	MD_TOKEN_VERTICALPIPE,      /* | */
	MD_TOKEN_UNDERSCORE,        /* _ */
	MD_TOKEN_PERIOD,            /* . */
	MD_TOKEN_FORWARDSLASH,      /* / */
	MD_TOKEN_BACKSLASH,         /* \ */
	MD_TOKEN_COLON,             /* : */
	MD_TOKEN_COMMA,             /* , */
	MD_TOKEN_QUESTIONMARK,		/* ? */
	MD_TOKEN_AT,				/* @ */
	MD_TOKEN_DOLLAR,			/* $ */
	MD_TOKEN_SINGLEQUOTE,       /* ' */
	MD_TOKEN_DOUBLEQUOTE,       /* " */
	MD_TOKEN_NUMBER,
	MD_TOKEN_TEXT,
	MD_TOKEN_NONASCII,
	MD_TOKEN_EOF,
	MD_TOKEN_END
} __attribute__ ((__packed__));

enum md_tag_id_t {
	MD_TAG_HEADING_BEGIN,
	MD_TAG_HEADING1,
	MD_TAG_HEADING2,
	MD_TAG_HEADING3,
	MD_TAG_HEADING4,
	MD_TAG_HEADING5,
	MD_TAG_HEADING6,
	MD_TAG_HEADING_END,

	MD_TAG_STANDARD_TEXT_BEGIN,
	MD_TAG_NORMAL_TEXT,
	MD_TAG_ITALIC_TEXT,
	MD_TAG_BOLD_TEXT,
	MD_TAG_BOLD_ITALIC_TEXT,
	MD_TAG_STANDARD_TEXT_END,

	MD_TAG_CODE_TEXT,
	MD_TAG_LINEBREAK,
	MD_TAG_ORDERED_LIST,
	MD_TAG_LIST_ITEM,
	MD_TAG_UNORDERED_LIST,
	MD_TAG_BLOCKQUOTE,
	MD_TAG_CODE_BLOCK,
	MD_TAG_LABEL,
	MD_TAG_IMG_ALTTEXT,
	MD_TAG_HORIZONTAL_RULE,

	MD_TAG_TITLE,
	MD_TAG_TABLE,
	MD_TAG_TABLE_ROW,
	MD_TAG_TABLE_CELL,
	MD_TAG_END
} __attribute__ ((__packed__));


struct md_link_flag_t {
	uint8_t is_alias : 1;
};

/* Must be less than MD_TREE_MAX_NODES */
typedef uint16_t md_node_idx_t;

/* Must be less than MD_TREE_MAX_TOKENS */
typedef uint32_t md_token_idx_t;

/* Must be less than MD_TREE_MAX_FILE_SIZE */
typedef uint32_t md_token_off_t;

/* Must be less than MD_TREE_MAX_TOKEN_SIZE */
typedef uint16_t md_token_size_t;

/* Struct to hold all lexical tokens */
struct md_tokens_t {
	md_token_off_t begin[MD_TREE_MAX_TOKENS];
	md_token_size_t size[MD_TREE_MAX_TOKENS];
	enum md_token_id_t id[MD_TREE_MAX_TOKENS];
	const utf32_t *base;
	size_t count;
};

/* Struct to represent a range of tokens for a single string */
struct md_node_t {
	md_token_idx_t begin;
	md_token_idx_t end;
};

/* Struct to hold the parse tree */
struct md_tree_t {
	struct md_tokens_t tokens;

	/* the text and a reference to the owner node and the previous sibling node */
	struct md_node_t node[MD_TREE_MAX_NODES];
	enum md_tag_id_t tag[MD_TREE_MAX_NODES];
	md_token_idx_t parent[MD_TREE_MAX_NODES];

	struct md_node_t link[MD_TREE_MAX_LINKS];
	struct md_link_flag_t link_flags[MD_TREE_MAX_LINKS];
	md_token_idx_t link_parent[MD_TREE_MAX_LINKS];

	struct md_node_t tooltip[MD_TREE_MAX_TOOLTIPS];
	md_token_idx_t tooltip_parent[MD_TREE_MAX_TOOLTIPS];

	size_t node_count;
	size_t link_count;
	size_t tooltip_count;
};

#endif

