/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * md_parser.h
 *
 * Copyright (C) 2021  Imran Haider
 */

#ifndef MD_PARSER_H
#define MD_PARSER_H

#include "md_tree.h"
#include "unicode.h"

int md_parse(const utf32_t *restrict in_data, size_t in_size, struct md_tree_t *restrict tree);

int md_trace_token_table(const utf32_t *restrict in_data, size_t in_size);
void md_trace_node_table(const struct md_tree_t *restrict tree);
void md_trace_link_table(const struct md_tree_t *restrict tree);

#endif

