// SPDX-License-Identifier: GPL-2.0-only
/*
 * md_parser.c
 *
 * Copyright (C) 2021  Imran Haider
 */

#include "md_parser.h"
#include "md_lexer.h"
#include "md_tree.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#define EXPECT_TOKEN_1_1(tree,p,token) \
	do {\
		if (__builtin_expect(tree->tokens.id[p] != token, 0)) \
			return 0; \
		++p; \
	} while(0)

#define EXPECT_TOKEN_2_1(tree,p,token1,token2) \
	do { \
		if (__builtin_expect(tree->tokens.id[p] != token1 && tree->tokens.id[p] != token2, 0)) \
			return 0; \
		++p; \
	} while(0)

#define EXPECT_TOKEN_3_1(tree,p,token1,token2,token3) \
	do { \
		if (__builtin_expect( \
				tree->tokens.id[p] != token1 && tree->tokens.id[p] != token2 && \
				tree->tokens.id[p] != token3, 0)) \
			return 0; \
		++p; \
	} while(0)

#define EXPECT_TOKEN_1_01(tree,p,token) \
	do { \
		if (tree->tokens.id[p] == token) \
			++p; \
	} while(0)

#define EXPECT_TOKEN_1_1N(tree,p,token) \
	do { \
		if (__builtin_expect(tree->tokens.id[p] != token, 0)) \
			return 0; \
		do { \
			++p; \
		} while (tree->tokens.id[p] == token); \
	} while(0)

#define EXPECT_TOKEN_1_0N(tree,p,token) \
	do { \
		while (tree->tokens.id[p] == token) { \
			++p; \
		} \
	} while(0)

#define EXPECT_TOKEN_2_0N(tree,p,token1,token2) \
	do { \
		while (tree->tokens.id[p] == token1 || tree->tokens.id[p] == token2) { \
			++p; \
		} \
	} while(0)

#define EXPECT_TOKEN_2_1N(tree,p,token1,token2) \
	do { \
		if (__builtin_expect(tree->tokens.id[p] != token1 && tree->tokens.id[p] != token2, 0)) \
			return 0; \
		do { \
			++p; \
		} while (tree->tokens.id[p] == token1 || tree->tokens.id[p] == token2); \
	} while(0)

#define EXPECT_TEXT(tree,p) \
	do { \
		if (__builtin_expect(expect_text(tree, &p) == 0, 0)) \
			return 0; \
	} while(0)

#define EXPECT_RICHTEXT(tree,p) \
	do { \
		if (__builtin_expect(expect_richtext(tree, &p) == 0, 0)) \
			return 0; \
	} while(0)

#define EXPECT_CODE(tree,p) \
	do { \
		if (__builtin_expect(expect_code(tree, &p) == 0, 0)) \
			return 0; \
	} while(0)

#define EXPECT_LINK(tree,p) \
	do { \
		if (__builtin_expect(expect_link(tree, &p) == 0, 0)) \
			return 0; \
	} while(0)

#define EXPECT_TOOLTIP(tree,p) \
	do { \
		if (__builtin_expect(expect_tooltip(tree, &p) == 0, 0)) \
			return 0; \
	} while(0)

struct md_parser_t {
	md_token_idx_t current;

	/* parse tree */
	struct md_tree_t *restrict tree;

	/* header positions */
	md_token_idx_t headings[6];

	/* node stack */
	size_t node_stack[MD_TREE_MAX_STACK_SIZE];
	size_t node_stack_position;
	size_t node_stack_size;

	/* table */
	unsigned char table_column_count;

	/* parsing error handling */
	const char *restrict exception_msg;
	md_token_idx_t exception_location;

	/* flags */
	uint16_t exception_pending            :1;
	uint16_t header_depth                 :3;
	uint16_t line_has_text                :1;
	uint16_t table_mode                   :1;
	uint16_t list_mode                    :2;
	uint16_t alt_header_mode              :2;
	uint16_t title_mode                   :1;
	uint16_t in_backtick_fenced_codeblock :1;
	uint16_t in_tilde_fenced_codeblock    :1;

};

/* Lexical token analyzers */
static int read_node(struct md_parser_t *restrict parser);
static int read_h1_alt(struct md_parser_t *restrict parser);
static int read_h2_alt(struct md_parser_t *restrict parser);
static int read_h1(struct md_parser_t *restrict parser);
static int read_h2(struct md_parser_t *restrict parser);
static int read_h3(struct md_parser_t *restrict parser);
static int read_h4(struct md_parser_t *restrict parser);
static int read_h5(struct md_parser_t *restrict parser);
static int read_h6(struct md_parser_t *restrict parser);
static int read_linebreak(struct md_parser_t *restrict parser);
static int read_trailing_newline(struct md_parser_t *restrict parser);
static int read_rich_text(struct md_parser_t *restrict parser);
static int read_normal_text(struct md_parser_t *restrict parser);
static int read_code(struct md_parser_t *restrict parser);
static int read_italic_text(struct md_parser_t *restrict parser);
static int read_bold_text(struct md_parser_t *restrict parser);
static int read_bold_italic_text(struct md_parser_t *restrict parser);
static int read_horizontal_rule_asterisk(struct md_parser_t *restrict parser);
static int read_horizontal_rule_hyphen(struct md_parser_t *restrict parser);
static int read_horizontal_rule_underscore(struct md_parser_t *restrict parser);
static int read_blockquote(struct md_parser_t *restrict parser);
static int read_codeblock(struct md_parser_t *restrict parser);
static int read_backtick_fenced_codeblock(struct md_parser_t *restrict parser);
static int read_tilde_fenced_codeblock(struct md_parser_t *restrict parser);
static int read_ordered_list(struct md_parser_t *restrict parser);
static int read_unordered_list(struct md_parser_t *restrict parser);
static int read_link_begin(struct md_parser_t *restrict parser);
static int read_link_end(struct md_parser_t *restrict parser);
static int read_quick_link(struct md_parser_t *restrict parser);
static int read_image(struct md_parser_t *restrict parser);
static int read_table(struct md_parser_t *restrict parser);
static int read_table_header_separator(struct md_parser_t *restrict parser);
static int read_table_column_separator(struct md_parser_t *restrict parser);
static int read_eof(struct md_parser_t *restrict parser);
static int expect_text(struct md_tree_t *restrict tree, md_token_idx_t *restrict p);
static int expect_richtext(struct md_tree_t *restrict tree, md_token_idx_t *restrict p);
static int expect_code(struct md_tree_t *restrict tree, md_token_idx_t *restrict p);
static int expect_link(struct md_tree_t *restrict tree, md_token_idx_t *restrict p);
static int expect_tooltip(struct md_tree_t *restrict tree, md_token_idx_t *restrict p);

/* Parse tree operations */
static md_token_idx_t push_text(
		struct md_parser_t *parser, md_token_idx_t text_begin, md_token_idx_t text_end,
		md_token_idx_t node_end, enum md_tag_id_t tag_id);
static void push_node(
		struct md_parser_t *restrict parser, md_token_idx_t token_idx, enum md_tag_id_t tag_id);
static void pop_node(struct md_parser_t *restrict parser, enum md_tag_id_t tag_id);
static void enter_header(
		struct md_parser_t *restrict parser, md_token_idx_t begin, md_token_idx_t end,
		unsigned char level);
static void push_link(
		struct md_parser_t *restrict parser, md_token_idx_t link_begin, md_token_idx_t link_end,
		md_token_idx_t text_begin, uint8_t is_alias);
static void push_tooltip(
		struct md_parser_t *restrict parser, md_token_idx_t tooltip_begin,
		md_token_idx_t tooltip_end, md_token_idx_t text_begin);


/* Debugging */
static void trace_token(
		enum md_token_id_t token_id, const utf32_t *restrict in_data, md_token_size_t in_size);
static const char* get_tag_name(enum md_tag_id_t tag_id);

int md_trace_token_table(const utf32_t *restrict in_data, size_t in_size)
{
	struct md_tokens_t tokens = { 0 };

	int rc = md_lex(in_data, in_size, &tokens);
	if (__builtin_expect(rc != 0, 0)) {
		return rc;
	}

	md_token_idx_t i;
	for (i=0; i < tokens.count; ++i) {
		printf("\tindex: %-3d  ", i);
		trace_token(tokens.id[i], tokens.base + tokens.begin[i], tokens.size[i]);
	}

	return 0;
}

int md_parse(const utf32_t *restrict in_data, size_t in_size, struct md_tree_t *restrict tree)
{
	struct md_parser_t parser = { 0 };
	int processed;

	int rc = md_lex(in_data, in_size, &tree->tokens);
	if (__builtin_expect(rc != 0, 0)) {
		return rc;
	}

	parser.tree = tree;
	parser.header_depth = 0;

	/* setting the initial stack size to 1. index 0 is reserved so that we needn't check if
	 * node_stack_size-1 is negative in push_node()
	 */
	parser.node_stack_position = 0;
	parser.node_stack_size = 1;
	parser.line_has_text = 0;
	parser.table_mode = 0;
	parser.list_mode = 0;
	parser.title_mode = 0;

	/* process all tokens */
	while (parser.current < tree->tokens.count) {
		processed = read_node(&parser);

		if (processed) {
			if (__builtin_expect(parser.exception_pending, 0))
				break;
		}
		else {
			parser.exception_pending = 1;
			parser.exception_msg = "Invalid syntax";
			parser.exception_location = parser.current;
			break;
		}
	}

	if (__builtin_expect(parser.exception_pending, 0)) {
		const utf32_t *p;
		const utf32_t *begin = tree->tokens.base + tree->tokens.begin[parser.exception_location];
		int column_num = 0;
		int line_num = 0;

		for (p = in_data; p < begin; ++p) {
			if (*p != '\n') {
				++column_num;
			}
			else {
				++line_num;
				column_num = 0;
			}
		}

		fprintf(stderr, "md_parse: '%s' on line %d, column %d\n",
				parser.exception_msg, line_num+1, column_num+1);
		rc = -1;
	}

	return rc;
}

static int read_node(struct md_parser_t *restrict parser)
{
	int processed = 0;

	if (__builtin_expect(parser->in_backtick_fenced_codeblock, 0)) {
		if (!parser->line_has_text) {
			processed =
			   read_linebreak(parser)
			|| read_backtick_fenced_codeblock(parser)
			|| read_tilde_fenced_codeblock(parser);

		}
		else {
			processed = read_trailing_newline(parser);
		}

		processed =
			   processed
			|| read_rich_text(parser)
			|| read_eof(parser);
	}
	else {
		if (!parser->line_has_text) {
			processed =
			   read_linebreak(parser)
			|| read_horizontal_rule_asterisk(parser)
			|| read_horizontal_rule_hyphen(parser)
			|| read_horizontal_rule_underscore(parser)
			|| read_blockquote(parser)
			|| read_codeblock(parser)
			|| read_backtick_fenced_codeblock(parser)
			|| read_tilde_fenced_codeblock(parser)
			|| read_ordered_list(parser)
			|| read_unordered_list(parser)
			|| read_h1(parser)
			|| read_h2(parser)
			|| read_h3(parser)
			|| read_h4(parser)
			|| read_h5(parser)
			|| read_h6(parser)
			|| read_h1_alt(parser)
			|| read_h2_alt(parser)
			|| read_table(parser)
			|| read_table_header_separator(parser);

			/* The 'alternate' headers must be parsed after the standard headers. This is to prevent
			 * the hash tokens from being absorbed as part of the header text.
			 */
		}
		else {
			processed = read_trailing_newline(parser);
		}

		processed =
			   processed
			|| read_table_column_separator(parser)
			|| read_image(parser)
			|| read_code(parser)
			|| read_italic_text(parser)
			|| read_bold_text(parser)
			|| read_bold_italic_text(parser)
			|| read_normal_text(parser)
			|| read_link_begin(parser)
			|| read_link_end(parser)
			|| read_quick_link(parser)
			|| read_eof(parser);
	}

	return processed;
}

static int read_h1_alt(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_RICHTEXT(tree, p);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_NEWLINE);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_EQUAL);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_EQUAL);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_EQUAL);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_NEWLINE, MD_TOKEN_EOF);
	md_token_idx_t end = p;

	parser->alt_header_mode = 1;
	parser->title_mode = 1;
	enter_header(parser, begin, end, 1);
	push_node(parser, begin, MD_TAG_TITLE);
	
	return 1;
}

static int read_h2_alt(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_RICHTEXT(tree, p);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_NEWLINE);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_NEWLINE, MD_TOKEN_EOF);
	md_token_idx_t end = p;

	parser->alt_header_mode = 2;
	parser->title_mode = 1;
	enter_header(parser, begin, end, 2);
	push_node(parser, begin, MD_TAG_TITLE);
	
	return 1;
}

static int read_h1(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	enter_header(parser, begin, p, 1);
	push_node(parser, begin, MD_TAG_TITLE);

	parser->current = p;
	parser->title_mode = 1;

	return 1;
}

static int read_h2(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	enter_header(parser, begin, p, 2);
	push_node(parser, begin, MD_TAG_TITLE);

	parser->current = p;
	parser->title_mode = 1;

	return 1;
}

static int read_h3(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	enter_header(parser, begin, p, 3);
	push_node(parser, begin, MD_TAG_TITLE);

	parser->current = p;
	parser->title_mode = 1;

	return 1;
}

static int read_h4(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	enter_header(parser, begin, p, 4);
	push_node(parser, begin, MD_TAG_TITLE);

	parser->current = p;
	parser->title_mode = 1;

	return 1;
}

static int read_h5(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	enter_header(parser, begin, p, 5);
	push_node(parser, begin, MD_TAG_TITLE);

	parser->current = p;
	parser->title_mode = 1;

	return 1;
}

static int read_h6(struct md_parser_t *restrict parser)
{
	if (__builtin_expect(parser->title_mode != 0, 0))
		return 0;

	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HASH);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	enter_header(parser, begin, p, 6);
	push_node(parser, begin, MD_TAG_TITLE);

	parser->current = p;
	parser->title_mode = 1;

	return 1;
}

static int read_linebreak(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_NEWLINE);

	/* escape table and list modes */
	if (parser->table_mode) {
		parser->table_mode = 0;
		pop_node(parser, MD_TAG_TABLE);
	}

	if (parser->list_mode == 1) {
		parser->list_mode = 0;
		pop_node(parser, MD_TAG_UNORDERED_LIST);
	}
	else if (parser->list_mode == 2) {
		parser->list_mode = 0;
		pop_node(parser, MD_TAG_ORDERED_LIST);
	}

	parser->current = push_text(parser, text_begin, p, p, MD_TAG_LINEBREAK);

	if (!parser->in_backtick_fenced_codeblock && !parser->in_tilde_fenced_codeblock) {
		/* hard-reset the node stack. we never continue a tree-like structure across a line break */
		parser->node_stack_position = 0;
		parser->node_stack_size = 1;
	}

	return 1;
}

static int read_trailing_newline(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_NEWLINE);

	/* expect title mode */
	if (parser->title_mode) {
		pop_node(parser, MD_TAG_TITLE);

		if (parser->alt_header_mode == 1) {
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_EQUAL);
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_EQUAL);
			EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_EQUAL);

			text_begin = p;
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_NEWLINE);
		}
		else if (parser->alt_header_mode == 2) {
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HYPHEN);
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HYPHEN);
			EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_HYPHEN);

			text_begin = p;
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_NEWLINE);
		}

		parser->title_mode = 0;
		parser->alt_header_mode = 0;
	}

	parser->current = push_text(parser, text_begin, p, p, MD_TAG_NORMAL_TEXT);

	return 1;
}

static int read_rich_text(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_RICHTEXT(tree, p);

	md_token_idx_t text_end = p;

	if (parser->list_mode) {
		md_token_idx_t prev = p;
		EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_NEWLINE);

		/* if we encountered a new line, skip over spaces following it as well */
		if (p > prev) {
			prev = p;
			EXPECT_TOKEN_1_0N(tree, p, MD_TOKEN_SPACE);

			/* if we also encountered spaces, backtrack one step to keep a single space.
			 * we are effectively collapsing one or more spaces into a single space
			 */
			if (p > prev)
				--p;
		}
	}

	parser->current = push_text(parser, text_begin, text_end, p, MD_TAG_NORMAL_TEXT);
	return 1;
}

static int read_normal_text(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TEXT(tree, p);

	md_token_idx_t text_end = p;

	if (parser->list_mode) {
		md_token_idx_t prev = p;
		EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_NEWLINE);

		/* if we encountered a new line, skip over spaces following it as well */
		if (p > prev) {
			prev = p;
			EXPECT_TOKEN_1_0N(tree, p, MD_TOKEN_SPACE);

			/* if we also encountered spaces, backtrack one step to keep a single space.
			 * we are effectively collapsing one or more spaces into a single space
			 */
			if (p > prev)
				--p;
		}
	}

	parser->current = push_text(parser, text_begin, text_end, p, MD_TAG_NORMAL_TEXT);
	return 1;
}

static int read_code(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_BACKTICK);
	md_token_idx_t text_begin = p;

	EXPECT_CODE(tree, p);

	md_token_idx_t text_end = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_BACKTICK);
	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_NEWLINE);

	parser->current = push_text(parser, text_begin, text_end, p, MD_TAG_CODE_TEXT);
	return 1;
}

static int read_italic_text(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	md_token_idx_t text_begin = p;

	EXPECT_TEXT(tree, p);

	md_token_idx_t text_end = p;
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_NEWLINE);

	parser->current = push_text(parser, text_begin, text_end, p, MD_TAG_ITALIC_TEXT);
	return 1;
}

static int read_bold_text(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	md_token_idx_t text_begin = p;

	EXPECT_TEXT(tree, p);

	md_token_idx_t text_end = p;
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_NEWLINE);

	parser->current = push_text(parser, text_begin, text_end, p, MD_TAG_BOLD_TEXT);
	return 1;
}

static int read_bold_italic_text(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	md_token_idx_t text_begin = p;

	EXPECT_TEXT(tree, p);

	md_token_idx_t text_end = p;
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_ASTERISK, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_NEWLINE);

	parser->current = push_text(parser, text_begin, text_end, p, MD_TAG_BOLD_ITALIC_TEXT);
	return 1;
}

static int read_horizontal_rule_asterisk(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_ASTERISK);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_ASTERISK);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_ASTERISK);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_NEWLINE, MD_TOKEN_EOF);

	parser->current = push_text(parser, text_begin, p, p, MD_TAG_HORIZONTAL_RULE);
	return 1;
}

static int read_horizontal_rule_hyphen(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_NEWLINE, MD_TOKEN_EOF);

	parser->current = push_text(parser, text_begin, p, p, MD_TAG_HORIZONTAL_RULE);
	return 1;
}

static int read_horizontal_rule_underscore(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_UNDERSCORE);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_NEWLINE, MD_TOKEN_EOF);

	parser->current = push_text(parser, text_begin, p, p, MD_TAG_HORIZONTAL_RULE);
	return 1;
}

static int read_blockquote(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_GREATERTHAN);

	push_node(parser, text_begin, MD_TAG_BLOCKQUOTE);
	parser->current = p;

	return 1;
}

static int read_codeblock(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;

	if (tree->tokens.id[p] == MD_TOKEN_TAB) {
		++p;
		goto found;
	}
	else {
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_SPACE);
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_SPACE);
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_SPACE);
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_SPACE);

		goto found;
	}

	return 0;

found:
	push_node(parser, text_begin, MD_TAG_CODE_BLOCK);
	parser->current = p;
	return 1;
}

static int read_backtick_fenced_codeblock(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_BACKTICK);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_BACKTICK);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_BACKTICK);

	if (!parser->in_backtick_fenced_codeblock) {
		parser->in_backtick_fenced_codeblock = 1;
		push_node(parser, text_begin, MD_TAG_CODE_BLOCK);
	}
	else {
		parser->in_backtick_fenced_codeblock = 0;
		pop_node(parser, MD_TAG_CODE_BLOCK);
	}

	parser->current = p;
	return 1;
}

static int read_tilde_fenced_codeblock(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_TILDE);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_TILDE);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_TILDE);

	if (!parser->in_tilde_fenced_codeblock) {
		parser->in_tilde_fenced_codeblock = 1;
		push_node(parser, text_begin, MD_TAG_CODE_BLOCK);
	}
	else {
		parser->in_tilde_fenced_codeblock = 0;
		pop_node(parser, MD_TAG_CODE_BLOCK);
	}

	parser->current = p;
	return 1;
}

static int read_ordered_list(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_0N(tree, p, MD_TOKEN_SPACE);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_NUMBER);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_PERIOD);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	if (!parser->list_mode) {
		push_node(parser, text_begin, MD_TAG_ORDERED_LIST);
	}
	else {
		pop_node(parser, MD_TAG_LIST_ITEM);
	}

	push_node(parser, text_begin, MD_TAG_LIST_ITEM);

	parser->current = p;
	parser->list_mode = 2;

	return 1;
}

static int read_unordered_list(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t text_begin = p;
	EXPECT_TOKEN_1_0N(tree, p, MD_TOKEN_SPACE);
	EXPECT_TOKEN_3_1(tree, p, MD_TOKEN_HYPHEN, MD_TOKEN_ASTERISK, MD_TOKEN_PLUS);
	EXPECT_TOKEN_2_1N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	if (!parser->list_mode) {
		push_node(parser, text_begin, MD_TAG_UNORDERED_LIST);
	}
	else {
		pop_node(parser, MD_TAG_LIST_ITEM);
	}

	push_node(parser, text_begin, MD_TAG_LIST_ITEM);

	parser->current = p;
	parser->list_mode = 1;

	return 1;
}

static int read_link_begin(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	md_token_idx_t link_begin = p;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_OPENBRACKET);

	push_node(parser, link_begin, MD_TAG_LABEL);
	parser->current = p;

	return 1;
}

static int read_link_end(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	const enum md_token_id_t *restrict id = tree->tokens.id;
	md_token_idx_t p = parser->current;
	uint8_t is_alias = 0;
	uint8_t is_quick_alias = 0;

	/* make sure the top of the node stack is a 'link' node. in read_link_begin(), the call to
	 * 'push_node' takes the token MD_TOKEN_OPENBRACKET as the second argument, which associates
	 * the node stack element with this token.
	 */
	md_token_idx_t top = tree->node[parser->node_stack[parser->node_stack_position]].begin;
	if (__builtin_expect(id[top] != MD_TOKEN_OPENBRACKET, 0)) {
		return 0;
	}

	md_token_idx_t close_bracket = parser->current;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_CLOSEBRACKET);

	if (id[p] == MD_TOKEN_OPENPAREN) {
		is_alias = 0;
		++p;
	}
	else {
		if (id[p] == MD_TOKEN_OPENBRACKET)
			++p;
		else
			is_quick_alias = 1;

		is_alias = 1;
	}

	md_token_idx_t link_begin;
	md_token_idx_t link_end;
	md_token_idx_t tooltip_begin = 0;
	md_token_idx_t tooltip_end = 0;

	if (!is_quick_alias) {
		link_begin = p;
		EXPECT_LINK(tree, p);
		link_end = p;

		if (id[p] == MD_TOKEN_SPACE || id[p] == MD_TOKEN_TAB) {
			++p;
			EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

			enum md_token_id_t quote_token = id[p];
			EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_SINGLEQUOTE, MD_TOKEN_DOUBLEQUOTE);

			tooltip_begin = p;
			EXPECT_TOOLTIP(tree, p);

			tooltip_end = p;
			EXPECT_TOKEN_1_1(tree, p, quote_token);
		}

		if (!is_alias)
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_CLOSEPAREN);
		else
			EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_CLOSEBRACKET);
	}
	else {
		link_begin = top + 1;
		link_end = close_bracket;
	}

	/* pop the 'link' node, which should be the top of the node stack */
	parser->node_stack_size = parser->node_stack_position;
	--parser->node_stack_position;

	push_link(parser, link_begin, link_end, top, is_alias);

	if (tooltip_begin != 0) {
		push_tooltip(parser, tooltip_begin, tooltip_end, top);
	}

	parser->current = p;
	return 1;
}

static int read_quick_link(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_LESSTHAN);

	md_token_idx_t link_begin = p;
	EXPECT_LINK(tree, p);
	md_token_idx_t link_end = p;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_GREATERTHAN);

	push_node(parser, link_begin, MD_TAG_LABEL);
	push_link(parser, link_begin, link_end, link_begin, 0);
	parser->current = push_text(parser, link_begin, link_end, p, MD_TAG_NORMAL_TEXT);
	pop_node(parser, MD_TAG_LABEL);

	return 1;
}

static int read_image(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	const enum md_token_id_t *restrict id = tree->tokens.id;

	md_token_idx_t p = parser->current;
	uint8_t is_alias = 0;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_EXCLAMATIONMARK);
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_OPENBRACKET);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
	md_token_idx_t alt_text_begin = p;

	EXPECT_TEXT(tree, p);
	md_token_idx_t alt_text_end = p;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_CLOSEBRACKET);

	if (id[p] == MD_TOKEN_OPENPAREN) {
		is_alias = 0;
		++p;
	}
	else if (id[p] == MD_TOKEN_OPENBRACKET) {
		is_alias = 1;
		++p;
	}
	else {
		return 0;
	}

	md_token_idx_t link_begin = p;

	EXPECT_LINK(tree, p);
	md_token_idx_t link_end = p;
	md_token_idx_t tooltip_begin = 0;
	md_token_idx_t tooltip_end = 0;

	if (id[p] == MD_TOKEN_SPACE || id[p] == MD_TOKEN_TAB) {
		++p;
		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

		enum md_token_id_t quote_token = id[p];
		EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_SINGLEQUOTE, MD_TOKEN_DOUBLEQUOTE);

		tooltip_begin = p;
		EXPECT_TOOLTIP(tree, p);

		tooltip_end = p;
		EXPECT_TOKEN_1_1(tree, p, quote_token);
	}

	if (!is_alias)
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_CLOSEPAREN);
	else
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_CLOSEBRACKET);

	push_node(parser, alt_text_begin, MD_TAG_IMG_ALTTEXT);
	parser->current = push_text(parser, alt_text_begin, alt_text_end, p, MD_TAG_NORMAL_TEXT);
	push_link(parser, link_begin, link_end, alt_text_begin, is_alias);

	if (tooltip_begin != 0) {
		push_tooltip(parser, tooltip_begin, tooltip_end, alt_text_begin);
	}

	pop_node(parser, MD_TAG_IMG_ALTTEXT);
	return 1;
}

static int read_table(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	const enum md_token_id_t *restrict id = tree->tokens.id;

	if (__builtin_expect(parser->table_mode == 1 || parser->list_mode || parser->title_mode, 0)) {
		return 0;
	}

	md_token_idx_t p = parser->current;
	unsigned char n_columns = 0, i, found_separator = 0;

	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_VERTICALPIPE);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	do {
		EXPECT_TEXT(tree, p);
		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

		found_separator = id[p] == MD_TOKEN_VERTICALPIPE;
		if (found_separator)
			++p;

		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
		++n_columns;
	} while (found_separator && id[p] != MD_TOKEN_NEWLINE);

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_NEWLINE);
	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_VERTICALPIPE);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	for (i=1; i<n_columns; ++i) {
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_VERTICALPIPE);
		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
		EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_HYPHEN);
		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
	}

	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_VERTICALPIPE);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_NEWLINE, MD_TOKEN_EOF);

	push_node(parser, parser->current, MD_TAG_TABLE);
	parser->table_mode = 1;
	parser->table_column_count = n_columns;

	return 1;
}

static int read_table_header_separator(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;

	if (__builtin_expect(parser->table_mode == 0, 0)) {
		return 0;
	}

	md_token_idx_t p = parser->current;
	unsigned char n_columns = parser->table_column_count, i;

	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_VERTICALPIPE);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
	EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_HYPHEN);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

	for (i=1; i<n_columns; ++i) {
		EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_VERTICALPIPE);
		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
		EXPECT_TOKEN_1_1N(tree, p, MD_TOKEN_HYPHEN);
		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
	}

	EXPECT_TOKEN_1_01(tree, p, MD_TOKEN_VERTICALPIPE);
	EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);
	EXPECT_TOKEN_2_1(tree, p, MD_TOKEN_NEWLINE, MD_TOKEN_EOF);

	parser->current = p;
	return 1;
}

static int read_table_column_separator(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	const enum md_token_id_t *restrict id = tree->tokens.id;

	if (__builtin_expect(parser->table_mode == 0, 0)) {
		return 0;
	}

	md_token_idx_t p = parser->current;
	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_VERTICALPIPE);

	md_token_idx_t q = p;

	/* If the line begins with the |, it's not really a separator; it's merely
	 * an optional | character at the beginning of the row. We should still
	 * return 1 so it's marked as a processed token but we don't need to
	 * push/pop a cell node
	 */
	if (parser->line_has_text) {
		EXPECT_TOKEN_2_0N(tree, p, MD_TOKEN_SPACE, MD_TOKEN_TAB);

		if (!(id[p] == MD_TOKEN_NEWLINE || id[p] == MD_TOKEN_EOF)) {
			pop_node(parser, MD_TAG_TABLE_CELL);
			push_node(parser, q, MD_TAG_TABLE_CELL);
		}
	}

	parser->current = q;
	return 1;
}

static int read_eof(struct md_parser_t *restrict parser)
{
	struct md_tree_t *restrict tree = parser->tree;
	md_token_idx_t p = parser->current;

	EXPECT_TOKEN_1_1(tree, p, MD_TOKEN_EOF);
	parser->current = p;

	return 1;
}

static int expect_text(struct md_tree_t *restrict tree, md_token_idx_t *restrict p)
{
	const enum md_token_id_t *restrict id = tree->tokens.id;
	int found = 0;

	while (1) {
		switch (id[*p]) {
		case MD_TOKEN_BACKSLASH:
			*p += 2;
			continue;

		case MD_TOKEN_TEXT:
		case MD_TOKEN_NONASCII:
		case MD_TOKEN_NUMBER:
		case MD_TOKEN_PLUS:
		case MD_TOKEN_HYPHEN:
		case MD_TOKEN_PERIOD:
		case MD_TOKEN_SPACE:
		case MD_TOKEN_TAB:
		case MD_TOKEN_HASH:
		case MD_TOKEN_OPENPAREN:
		case MD_TOKEN_CLOSEPAREN:
		case MD_TOKEN_OPENBRACE:
		case MD_TOKEN_CLOSEBRACE:
		case MD_TOKEN_VERTICALPIPE:
		case MD_TOKEN_UNDERSCORE:
		case MD_TOKEN_EQUAL:
		case MD_TOKEN_EXCLAMATIONMARK:
		case MD_TOKEN_SINGLEQUOTE:
		case MD_TOKEN_DOUBLEQUOTE:
		case MD_TOKEN_FORWARDSLASH:
		case MD_TOKEN_COLON:
		case MD_TOKEN_COMMA:
		case MD_TOKEN_QUESTIONMARK:
		case MD_TOKEN_AT:
		case MD_TOKEN_DOLLAR:
			++*p;
			found = 1;
			continue;
		default:
			break;
		}
		break;
	}

	return found;
}

static int expect_richtext(struct md_tree_t *restrict tree, md_token_idx_t *restrict p)
{
	const enum md_token_id_t *restrict id = tree->tokens.id;
	int found = 0;

	while (1) {
		switch (id[*p]) {
		case MD_TOKEN_BACKSLASH:
			*p += 2;
			continue;

		case MD_TOKEN_TEXT:
		case MD_TOKEN_NONASCII:
		case MD_TOKEN_NUMBER:
		case MD_TOKEN_PLUS:
		case MD_TOKEN_HYPHEN:
		case MD_TOKEN_PERIOD:
		case MD_TOKEN_SPACE:
		case MD_TOKEN_TAB:
		case MD_TOKEN_HASH:
		case MD_TOKEN_OPENPAREN:
		case MD_TOKEN_CLOSEPAREN:
		case MD_TOKEN_OPENBRACE:
		case MD_TOKEN_CLOSEBRACE:
		case MD_TOKEN_VERTICALPIPE:
		case MD_TOKEN_UNDERSCORE:
		case MD_TOKEN_EQUAL:
		case MD_TOKEN_EXCLAMATIONMARK:
		case MD_TOKEN_SINGLEQUOTE:
		case MD_TOKEN_DOUBLEQUOTE:
		case MD_TOKEN_FORWARDSLASH:
		case MD_TOKEN_COLON:
		case MD_TOKEN_COMMA:
		case MD_TOKEN_QUESTIONMARK:
		case MD_TOKEN_AT:
		case MD_TOKEN_DOLLAR:
		case MD_TOKEN_BACKTICK:
		case MD_TOKEN_ASTERISK:
		case MD_TOKEN_OPENBRACKET:
		case MD_TOKEN_CLOSEBRACKET:
		case MD_TOKEN_LESSTHAN:
		case MD_TOKEN_GREATERTHAN:
			++*p;
			found = 1;
			continue;
		default:
			break;
		}
		break;
	}

	return found;
}

static int expect_code(struct md_tree_t *restrict tree, md_token_idx_t *restrict p)
{
	const enum md_token_id_t *restrict id = tree->tokens.id;
	int found = 0;

	while (1) {
		switch (id[*p]) {
		case MD_TOKEN_BACKSLASH:
			*p += 2;
			continue;

		case MD_TOKEN_TEXT:
		case MD_TOKEN_NONASCII:
		case MD_TOKEN_NUMBER:
		case MD_TOKEN_PLUS:
		case MD_TOKEN_HYPHEN:
		case MD_TOKEN_PERIOD:
		case MD_TOKEN_SPACE:
		case MD_TOKEN_TAB:
		case MD_TOKEN_HASH:
		case MD_TOKEN_OPENPAREN:
		case MD_TOKEN_CLOSEPAREN:
		case MD_TOKEN_OPENBRACE:
		case MD_TOKEN_CLOSEBRACE:
		case MD_TOKEN_VERTICALPIPE:
		case MD_TOKEN_UNDERSCORE:
		case MD_TOKEN_EQUAL:
		case MD_TOKEN_EXCLAMATIONMARK:
		case MD_TOKEN_SINGLEQUOTE:
		case MD_TOKEN_DOUBLEQUOTE:
		case MD_TOKEN_FORWARDSLASH:
		case MD_TOKEN_COLON:
		case MD_TOKEN_COMMA:
		case MD_TOKEN_QUESTIONMARK:
		case MD_TOKEN_AT:
		case MD_TOKEN_DOLLAR:
		case MD_TOKEN_ASTERISK:
		case MD_TOKEN_OPENBRACKET:
		case MD_TOKEN_CLOSEBRACKET:
		case MD_TOKEN_LESSTHAN:
		case MD_TOKEN_GREATERTHAN:
			++*p;
			found = 1;
			continue;
		default:
			break;
		}
		break;
	}

	return found;
}

static int expect_link(struct md_tree_t *restrict tree, md_token_idx_t *restrict p)
{
	const enum md_token_id_t *restrict id = tree->tokens.id;
	int found = 0;

	while (1) {
		switch (id[*p]) {
		case MD_TOKEN_BACKSLASH:
			*p += 2;
			continue;

		case MD_TOKEN_TEXT:
		case MD_TOKEN_NUMBER:
		case MD_TOKEN_PLUS:
		case MD_TOKEN_HYPHEN:
		case MD_TOKEN_FORWARDSLASH:
		case MD_TOKEN_COLON:
		case MD_TOKEN_PERIOD:
		case MD_TOKEN_HASH:
		case MD_TOKEN_EQUAL:
		case MD_TOKEN_EXCLAMATIONMARK:
		case MD_TOKEN_QUESTIONMARK:
			++*p;
			found = 1;
			continue;
		default:
			break;
		}
		break;
	}

	return found;
}

static int expect_tooltip(struct md_tree_t *restrict tree, md_token_idx_t *restrict p)
{
	const enum md_token_id_t *restrict id = tree->tokens.id;
	int found = 0;

	while (1) {
		switch (id[*p]) {
		case MD_TOKEN_BACKSLASH:
			*p += 2;
			continue;

		case MD_TOKEN_TEXT:
		case MD_TOKEN_NONASCII:
		case MD_TOKEN_NUMBER:
		case MD_TOKEN_PLUS:
		case MD_TOKEN_HYPHEN:
		case MD_TOKEN_PERIOD:
		case MD_TOKEN_SPACE:
		case MD_TOKEN_TAB:
		case MD_TOKEN_HASH:
		case MD_TOKEN_OPENPAREN:
		case MD_TOKEN_CLOSEPAREN:
		case MD_TOKEN_LESSTHAN:
		case MD_TOKEN_GREATERTHAN:
		case MD_TOKEN_EQUAL:
		case MD_TOKEN_EXCLAMATIONMARK:
			++*p;
			found = 1;
			continue;
		default:
			break;
		}
		break;
	}

	return found;
}

static md_token_idx_t push_text(
		struct md_parser_t *parser, md_token_idx_t text_begin, md_token_idx_t text_end,
		md_token_idx_t node_end, enum md_tag_id_t tag_id)
{
	struct md_tree_t *tree = parser->tree;
	const enum md_token_id_t *restrict id = tree->tokens.id;

	int last_token_is_newline = id[node_end-1] == MD_TOKEN_NEWLINE;
	if (!last_token_is_newline) {
		/* the most common case: the last token wasn't a new line */

		if (__builtin_expect(!parser->line_has_text, 0)) {
			/* if this token is the start of a new line */

			parser->line_has_text = 1;

			if (parser->table_mode) {
				/* if we are starting a new line while we are in table mode, we need
				 * to create a new table row and the first table cell within it
				 */
				push_node(parser, text_begin, MD_TAG_TABLE_ROW);
				push_node(parser, text_begin, MD_TAG_TABLE_CELL);
			}
		}
	}

	size_t i = tree->node_count;
	if (i < MD_TREE_MAX_NODES) {
		struct md_node_t range;
		range.begin = text_begin;
		range.end = text_end;

		unsigned char depth = parser->header_depth;
		md_token_idx_t parent;

		if (parser->node_stack_position > 0)
			parent = tree->node[parser->node_stack[parser->node_stack_position]].begin;
		else if (depth)
			parent = parser->headings[depth-1];
		else
			parent = MD_TREE_MAX_NODES;

		tree->parent[i] = parent;
		tree->node[i] = range;
		tree->tag[i] = tag_id;
		tree->node_count = i+1;

		if (__builtin_expect(last_token_is_newline, 0)) {
			/* the last token was a new line. we reset the line_has_text flag so we can later
			 * detect when we start the next line
			 */
			parser->line_has_text = 0;

			if (parser->table_mode) {
				/* we are in table mode and we've encountered a new line. this pop_node call will
				 * pop both the table-cell and the table-row from the stack
				 */
				pop_node(parser, MD_TAG_TABLE_ROW);

				/* undo the last text insertion, which had to be a newline. it doesn't really
				 * help us when the last cell of every row is coupled with an additinal newline
				 * token
				 */
				--tree->node_count;
			}
			else if (parser->list_mode) {
				pop_node(parser, MD_TAG_LIST_ITEM);
			}
			else if (parser->title_mode) {
				pop_node(parser, MD_TAG_TITLE);
			}
			else if (!parser->in_backtick_fenced_codeblock && !parser->in_tilde_fenced_codeblock) {
				/* if we are not in table or list mode, and we are not in a fenced code block, we
				 * soft-reset the node stack position. this will allow us to walk through the nodes
				 * again in the new line (based on the newly encountered tokens) and use an
				 * existing node as a parent if applicable.
				 *
				 * we don't perform this soft-reset in table or list mode because we need to
				 * remember in the next line what table we are still associated with.
				 */
				parser->node_stack_position = 0;
			}
		}
	}
	else {
		parser->exception_pending = 1;
		parser->exception_msg = "Not enough space for tree. Increase MD_TREE_MAX_NODES";
		parser->exception_location = text_begin;
	}

	return node_end;
}

static void push_node(
		struct md_parser_t *restrict parser, md_token_idx_t token_idx, enum md_tag_id_t tag_id)
{
	struct md_tree_t *restrict tree = parser->tree;
	const enum md_token_id_t *restrict id = tree->tokens.id;
	size_t i = tree->node_count;

	if (i < MD_TREE_MAX_NODES) {
		if (parser->node_stack_position+1 < parser->node_stack_size) {
			md_token_idx_t idx = tree->node[parser->node_stack[parser->node_stack_position+1]].begin;
			if (id[idx] == id[token_idx]) {
				/* If the node we are about to push is already in the next position on the
				 * stack, we use the existing node.
				 */
				++parser->node_stack_position;
				return;
			}
			else {
				/* If the node we are about to push is different than the one that is on the
				 * stack, we reset the stack size to eliminate everything after the current
				 * position and carry on adding the new node as we would normally do.
				 */
				parser->node_stack_size = parser->node_stack_position+1;
			}
		}

		unsigned char depth = parser->header_depth;
		md_token_idx_t parent;

		if (parser->node_stack_position > 0)
			parent = tree->node[parser->node_stack[parser->node_stack_position]].begin;
		else if (depth)
			parent = parser->headings[depth-1];
		else
			parent = MD_TREE_MAX_NODES;

		struct md_node_t range = { token_idx, token_idx+1 };
		tree->parent[i] = parent;
		tree->node[i] = range;
		tree->tag[i] = tag_id;
		tree->node_count = i+1;

		parser->node_stack[parser->node_stack_size] = i;
		parser->node_stack_position = parser->node_stack_size;
		++parser->node_stack_size;

		if (__builtin_expect(parser->node_stack_size >= MD_TREE_MAX_STACK_SIZE, 0)) {
			parser->exception_pending = 1;
			parser->exception_msg =
				"Not enough space for node stack. Increase MD_TREE_MAX_STACK_SIZE";
			parser->exception_location = token_idx;
		}
	}
	else {
		parser->exception_pending = 1;
		parser->exception_msg = "Not enough space for tree. Increase MD_TREE_MAX_NODES";
		parser->exception_location = token_idx;
	}
}

static void pop_node(struct md_parser_t *restrict parser, enum md_tag_id_t tag_id)
{
	struct md_tree_t *restrict tree = parser->tree;
	enum md_tag_id_t *restrict tag = tree->tag;

	int32_t i;

	for (i=parser->node_stack_position; i>0; --i) {
		size_t node_idx = parser->node_stack[i];
		if (tag[node_idx] == tag_id) {
			parser->node_stack_position = i-1;
			parser->node_stack_size = i;
			return;
		}
	}
}

static void enter_header(
		struct md_parser_t *restrict parser, md_token_idx_t begin, md_token_idx_t end,
		unsigned char level)
{
	struct md_tree_t *restrict tree = parser->tree;
	size_t node_idx = tree->node_count;

	if (node_idx < MD_TREE_MAX_NODES) {
		struct md_node_t range;
		range.begin = begin;
		range.end = end;

		tree->node[node_idx] = range;
		tree->tag[node_idx] = MD_TAG_HEADING1 + level - 1;
		tree->node_count = node_idx+1;

		unsigned char depth = parser->header_depth;
		md_node_idx_t parent;

		if (depth >= level)
			/* Remaining in the same header level or exiting the current scope to use
			 * a bigger header level
			 */
			parent = parser->headings[level-1];

		else if (depth)
			/* Entering a smaller header scope. We'll copy the current header token index
			 * to the smaller header scopes, all the way down to the level right before the
			 * requested level.
			 */
			parent = parser->headings[depth-1];

		else
			/* Entering the H1 header. This is a special case because H1 headers do not have
			 * a parent header scope so we will use MD_TREE_MAX_NODES as a sentinel value
			 */
			parent = MD_TREE_MAX_NODES;

		tree->parent[node_idx] = parent;

		for (unsigned char heading_idx = depth; heading_idx < level-1; ++heading_idx) {
			parser->headings[heading_idx] = parent;
		}

		parser->headings[level-1] = begin;
		parser->header_depth = level;

		/* hard-reset the node stack. we never continue a tree-like structure across different
		 * headings
		 */
		parser->node_stack_position = 0;
		parser->node_stack_size = 1;
	}
	else {
		parser->exception_pending = 1;
		parser->exception_msg = "Not enough space for tree. Increase MD_TREE_MAX_NODES";
		parser->exception_location = begin;
	}
}

static void push_link(
		struct md_parser_t *restrict parser, md_token_idx_t link_begin, md_token_idx_t link_end,
		md_token_idx_t text_begin, uint8_t is_alias)
{
	struct md_tree_t *restrict tree = parser->tree;
	size_t i = tree->link_count;

	if (i < MD_TREE_MAX_LINKS) {
		struct md_node_t range;
		range.begin = link_begin;
		range.end = link_end;

		tree->link_parent[i] = text_begin;
		tree->link[i] = range;
		tree->link_flags[i].is_alias = is_alias;
		tree->link_count = i+1;
	}
	else {
		parser->exception_pending = 1;
		parser->exception_msg = "Not enough space for tree. Increase MD_TREE_MAX_LINKS";
		parser->exception_location = link_begin;
	}
}

static void push_tooltip(
		struct md_parser_t *restrict parser, md_token_idx_t tooltip_begin,
		md_token_idx_t tooltip_end, md_token_idx_t text_begin)
{
	struct md_tree_t *restrict tree = parser->tree;
	size_t i = tree->tooltip_count;

	if (i < MD_TREE_MAX_TOOLTIPS) {
		struct md_node_t range;
		range.begin = tooltip_begin;
		range.end = tooltip_end;

		tree->tooltip_parent[i] = text_begin;
		tree->tooltip[i] = range;
		tree->tooltip_count = i+1;
	}
	else {
		parser->exception_pending = 1;
		parser->exception_msg = "Not enough space for tree. Increase MD_TREE_MAX_TOOLTIPS";
		parser->exception_location = tooltip_begin;
	}
}

static void trace_token(
		enum md_token_id_t token_id, const utf32_t *restrict in_data, md_token_size_t in_size)
{
	char *str;
	size_t size = 1;

	unicode_write_utf8_string(in_data, in_size, &str, &size);

	/* guaranteed space because the inital value of 'size' is 1 */
	str[size] = 0;

	switch (token_id) {
	case MD_TOKEN_OPENBRACE:
		printf("type: {             value: '%s'\n", str);
		break;
	case MD_TOKEN_CLOSEBRACE:
		printf("type: }             value: '%s'\n", str);
		break;
	case MD_TOKEN_UNDERSCORE:
		printf("type: _             value: '%s'\n", str);
		break;
	case MD_TOKEN_HASH:
		printf("type: #             value: '%s'\n", str);
		break;
	case MD_TOKEN_ASTERISK:
		printf("type: *             value: '%s'\n", str);
		break;
	case MD_TOKEN_PLUS:
		printf("type: +             value: '%s'\n", str);
		break;
	case MD_TOKEN_OPENBRACKET:
		printf("type: '['           value: '%s'\n", str);
		break;
	case MD_TOKEN_CLOSEBRACKET:
		printf("type: ']'           value: '%s'\n", str);
		break;
	case MD_TOKEN_OPENPAREN:
		printf("type: (             value: '%s'\n", str);
		break;
	case MD_TOKEN_CLOSEPAREN:
		printf("type: )             value: '%s'\n", str);
		break;
	case MD_TOKEN_HYPHEN:
		printf("type: -             value: '%s'\n", str);
		break;
	case MD_TOKEN_SPACE:
		printf("type: space         value: ' '\n");
		break;
	case MD_TOKEN_TAB:
		printf("type: tab           value: ' '\n");
		break;
	case MD_TOKEN_NEWLINE:
		printf("type: newline       value: ' '\n");
		break;
	case MD_TOKEN_LESSTHAN:
		printf("type: <             value: '%s'\n", str);
		break;
	case MD_TOKEN_GREATERTHAN:
		printf("type: >             value: '%s'\n", str);
		break;
	case MD_TOKEN_EQUAL:
		printf("type: equal         value: '%s'\n", str);
		break;
	case MD_TOKEN_TILDE:
		printf("type: tilde         value: '%s'\n", str);
		break;
	case MD_TOKEN_EXCLAMATIONMARK:
		printf("type: !             value: '%s'\n", str);
		break;
	case MD_TOKEN_BACKTICK:
		printf("type: `             value: '%s'\n", str);
		break;
	case MD_TOKEN_VERTICALPIPE:
		printf("type: |             value: '%s'\n", str);
		break;
	case MD_TOKEN_SINGLEQUOTE:
		printf("type: single_quote  value: '%s'\n", str);
		break;
	case MD_TOKEN_DOUBLEQUOTE:
		printf("type: double_quote  value: '%s'\n", str);
		break;
	case MD_TOKEN_TEXT:
		printf("type: text          value: '%s'\n", str);
		break;
	case MD_TOKEN_NONASCII:
		printf("type: non-ascii     value: '%s'\n", str);
		break;
	case MD_TOKEN_NUMBER:
		printf("type: number        value: '%s'\n", str);
		break;
	case MD_TOKEN_PERIOD:
		printf("type: .             value: '%s'\n", str);
		break;
	case MD_TOKEN_FORWARDSLASH:
		printf("type: /             value: '%s'\n", str);
		break;
	case MD_TOKEN_BACKSLASH:
		printf("type: \\            value: '%s'\n", str);
		break;
	case MD_TOKEN_COLON:
		printf("type: colon         value: '%s'\n", str);
		break;
	case MD_TOKEN_COMMA:
		printf("type: comma         value: '%s'\n", str);
		break;
	case MD_TOKEN_AT:
		printf("type: @             value: '%s'\n", str);
		break;
	case MD_TOKEN_DOLLAR:
		printf("type: $             value: '%s'\n", str);
		break;
	case MD_TOKEN_QUESTIONMARK:
		printf("type: ?             value: '%s'\n", str);
		break;
	case MD_TOKEN_EOF:
		printf("type: eof           value: ''\n");
		break;
	case MD_TOKEN_END:
		/* intentionally left blank */
		break;
	}

	unicode_utf8_string_free(&str, 1);
}

static const char* get_tag_name(enum md_tag_id_t tag_id)
{
	switch (tag_id) {
	case MD_TAG_HEADING1:
		return "heading1";
	case MD_TAG_HEADING2:
		return "heading2";
	case MD_TAG_HEADING3:
		return "heading3";
	case MD_TAG_HEADING4:
		return "heading4";
	case MD_TAG_HEADING5:
		return "heading5";
	case MD_TAG_HEADING6:
		return "heading6";
	case MD_TAG_NORMAL_TEXT:
		return "text";
	case MD_TAG_TITLE:
		return "title";
	case MD_TAG_CODE_TEXT:
		return "code";
	case MD_TAG_ITALIC_TEXT:
		return "italic-text";
	case MD_TAG_BOLD_TEXT:
		return "bold-text";
	case MD_TAG_BOLD_ITALIC_TEXT:
		return "bold-italic-text";
	case MD_TAG_LINEBREAK:
		return "line-break";
	case MD_TAG_ORDERED_LIST:
		return "ordered-list";
	case MD_TAG_UNORDERED_LIST:
		return "unordered-list";
	case MD_TAG_BLOCKQUOTE:
		return "blockquote";
	case MD_TAG_CODE_BLOCK:
		return "code-block";
	case MD_TAG_LABEL:
		return "label";
	case MD_TAG_IMG_ALTTEXT:
		return "image-alt-text";
	case MD_TAG_HORIZONTAL_RULE:
		return "horizontal-rule";
	case MD_TAG_TABLE:
		return "table";
	case MD_TAG_TABLE_ROW:
		return "table-row";
	case MD_TAG_TABLE_CELL:
		return "table-cell";
	case MD_TAG_LIST_ITEM:
		return "list-item";
	default:
		return 0;
	}
}

void md_trace_node_table(const struct md_tree_t *restrict tree)
{
	size_t i;

	for (i=0; i < tree->node_count; ++i) {
		md_token_idx_t parent = tree->parent[i];
		struct md_node_t node = tree->node[i];

		printf("\tindex: %-3ld  token_begin: %-3d  token_end: %-3d  ", i,  node.begin, node.end);

		if (parent < MD_TREE_MAX_NODES)
			printf("token_parent: %-6d  ", parent);
		else
			printf("token_parent: (root)  ");

		const char* tag_name = get_tag_name(tree->tag[i]);
		if (tag_name) {
			printf("tag: %s\n", tag_name);
		}
		else {
			printf("tag: unknown (%d)\n", tree->tag[i]);
		}
	}
}

void md_trace_link_table(const struct md_tree_t *restrict tree)
{
	int i;

	for (i=0; i < tree->link_count; ++i) {
		md_token_idx_t parent = tree->link_parent[i];
		struct md_node_t range = tree->link[i];

		printf("\tindex: %-3d  token_begin: %-3d  token_end: %-3d  ", i, range.begin, range.end);

		if (parent < MD_TREE_MAX_NODES)
			printf("token_parent: %-6d  ", parent);
		else
			printf("token_parent: (root)");

		printf("\n");
	}
}


